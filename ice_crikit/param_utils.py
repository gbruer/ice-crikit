from pyadjoint_utils import Control


class Param:
    def __init__(self, val, h, trainable, name, bounds=(None, None)):
        self.val = val
        self.h = h
        self.trainable = trainable
        self.bounds = bounds
        self.name = name

    def val_str(self):
        return str(self.val)


class ConstantParam(Param):
    def __init__(self, val, h, trainable, bounds=(None, None)):
        super().__init__(val, h, trainable, val.name(), bounds)

    def val_str(self):
        return str(self.val.values())


class ParamBag:
    def __init__(self, params):
        self.set_all_params(params)

    def set_all_params(self, params):
        self.params = params
        self.fixed_params = tuple(p for p in self.params if not p.trainable)
        self.trainable_params = tuple(p for p in self.params if p.trainable)
        self.trainable_params_idx = tuple(
            i for i, p in enumerate(self.params) if p.trainable
        )

        self.bounds = self.combine_bounds(self.trainable_params)
        self.controls = tuple(Control(p.val) for p in self.trainable_params)
        self.num_trainable_params = sum(
            [p.val._ad_dim() for p in self.trainable_params]
        )
        self.num_fixed_params = sum([p.val._ad_dim() for p in self.fixed_params])

    def get_vals(self, kind="all"):
        if kind == "all":
            params = self.params
        elif kind == "trainable":
            params = self.trainable_params
        elif kind == "fixed":
            params = self.fixed_params
        else:
            raise ValueError(f"Invalid kind: f{kind}")
        return tuple(p.val for p in params)

    def set_trainable_param_vals(self, trainable_param_vals):
        for i, val in zip(self.trainable_params_idx, trainable_param_vals):
            self.params[i].val = val
        self.controls = tuple(Control(p.val) for p in self.trainable_params)
        self.num_trainable_params = sum(
            [p.val._ad_dim() for p in self.trainable_params]
        )

    @staticmethod
    def combine_bounds(params):
        lower_bounds = [p.bounds[0] for p in params]
        upper_bounds = [p.bounds[1] for p in params]
        return [lower_bounds, upper_bounds]

    def __str__(self):
        return self.to_string()

    def to_string(self, kind="all"):
        lines = []
        max_length = max(len(p.name) for p in self.params)

        if kind not in ("all", "fixed", "trainable"):
            raise ValueError(f"Invalid kind: f{kind}")

        if kind in ("all", "fixed"):
            if len(self.fixed_params) > 0:
                lines.append(f"Fixed params: {self.num_fixed_params}")
                for p in self.fixed_params:
                    lines.append(f"    {p.name:{max_length}s} : {p.val_str()}")
            else:
                lines.append("No fixed params.")

        if kind in ("all", "trainable"):
            if len(self.trainable_params) > 0:
                lines.append("")
                lines.append(f"Trainable params: {self.num_trainable_params}")
                for p in self.trainable_params:
                    lines.append(f"    {p.name:{max_length}s} : {p.val_str()}")
            else:
                lines.append("No trainable params.")

        return "\n".join(lines)

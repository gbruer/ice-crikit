# Ice

This repo is for testing out CRIKit on an ice simulation.


## Requirements

This repository contains a submodule pointing to a specific commit of [CRIKit].
To clone this repo, use `git clone --recurse-submodules` to ensure the submodule is initialized.

Then run `python setup.py develop` to install ice-crikit as a module. This helps
with clean imports for the scripts in this repo.

## Running

There is a `.petscrc` file in the root directory, so it is recommended to run the
code from the root directory.

`python scripts/damage.py` runs a simulation with a damage factor field. Run with
`--help` to see more info.



[CRIKit]: https://gitlab.com/crikit/crikit




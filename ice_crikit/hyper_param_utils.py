import ast
import pathlib
import pprint
import warnings

from dataclasses import asdict, dataclass
from typing import Any, Optional, Tuple


@dataclass(unsafe_hash=True, order=True)
class HyperParams:

    # Network parameters.
    layer_sizes: Tuple[int]
    activation: str
    optimizer: str
    seed: int = None

    directory: str = ""

    # Observer parameters.
    noise: Optional[Tuple[float]] = None
    surface: bool = False
    observer: str = "full"
    borehole: bool = False
    borehole_observer: str = "full"
    borehole_noise: Optional[Tuple[float]] = None
    borehole_x: float = 0.6
    noise_seed: Optional[int] = None

    # CR parameters.
    dim: int = 2
    p_init: float = 1.5
    p_true: float = 1.5
    eps2: float = 1e-10
    gamma_f_true: float = 0.05
    esf_true: float = 2
    gamma_h_true: float = 0.01
    eh_true: float = 1.0

    # Experiment parameters.
    xi: float = 5e-1
    quad: int = 6
    phi_init: float = 0

    # Mesh parameters.
    grid: int = 16
    fine_grid: int = 32
    L: float = 2
    H: float = 1
    l: float = 0.5
    h: float = 0.5

    @staticmethod
    def init_from_args(args):
        # Most of the parameters can be copied directly from args.
        direct_arg_keys = [
            "activation",
            "optimizer",
            "seed",
            "surface",
            "observer",
            "borehole",
            "borehole_observer",
            "noise_seed",
            "dim",
            "p_init",
            "eps2",
            "p_true",
            "gamma_f_true",
            "esf_true",
            "gamma_h_true",
            "eh_true",
            "xi",
            "grid",
            "fine_grid",
            "phi_init",
            "L",
            "H",
            "l",
            "h",
            "quad",
        ]
        params = {k: getattr(args, k) for k in direct_arg_keys if hasattr(args, k)}

        # Some of the parameters need to be processed a little.
        directory = (
            args.directory
            if hasattr(args, "directory")
            else str(pathlib.Path().resolve())
        )
        layer_sizes = tuple(args.layer_sizes)
        noise = tuple(args.noise) if args.noise is not None else None
        borehole_noise = (
            tuple(args.borehole_noise) if args.borehole_noise is not None else None
        )
        return HyperParams(
            directory=directory,
            layer_sizes=layer_sizes,
            noise=noise,
            borehole_noise=borehole_noise,
            **params,
        )

    def __post_init__(self):
        self.eps2 = 1e-10
        if self.noise is None:
            self.noise = ()
        if self.borehole_noise is None:
            self.borehole_noise = ()
        self.phi_init = 0

    @property
    def keys(self):
        return list(self.asdict().keys())

    @property
    def quad_params(self):
        return dict(quadrature_degree=self.quad)

    def __str__(self):
        return pprint.pformat(self.asdict())

    def asdict(self):
        return asdict(self)

    @staticmethod
    def read_from_file(path_hyper_params, READ_MAX=10000):
        # path_hyper_params = pathlib.Path(path_hyper_params)
        if not path_hyper_params.exists():
            warnings.warn(f"Couldn't find this file: {path_hyper_params}")
            return None
        with path_hyper_params.open("r") as f:
            s = f.read(READ_MAX)
            if len(s) >= READ_MAX:
                raise ValueError(f"This file is too big: {path_hyper_params}")
        hyper_params_dict = ast.literal_eval(s)
        hyper_params = HyperParams(**hyper_params_dict)
        return hyper_params

    def save_to_file(self, filepath):
        with open(filepath, "w") as f:
            pprint.pprint(hyper_params_data.asdict(), stream=f)


@dataclass(frozen=False, unsafe_hash=True)
class HyperParamsData:
    optimizer_iterations: int

    loss_alg_initial: float
    loss_alg_initial_gradient: float
    loss_alg_final: float
    loss_alg_final_gradient: float

    num_trainable_params: int
    initially_solvable: bool
    finally_solvable: bool

    loss_initial: float = None
    loss_initial_gradient: float = None
    loss_final: float = None
    loss_final_gradient: float = None

    loss_initial_exp: Tuple[Tuple[float]] = None
    loss_initial_exp_gradient: Tuple[Tuple[float]] = None
    loss_final_exp: Tuple[Tuple[float]] = None
    loss_final_exp_gradient: Tuple[Tuple[float]] = None

    taylor_data: Any = None
    invt_losses: Any = None
    invt_losses_initial: Any = None
    invt_losses_final: Any = None
    cr_error_large: Any = None
    cr_error_small: Any = None

    def __post_init__(self):
        if self.invt_losses is not None:
            self.invt_losses_initial = tuple(self.invt_losses)
            self.invt_losses = None

    @staticmethod
    def init_from_opt_info(opt_info, param_bag):
        loss_total = opt_info[0]
        loss_gradient_total = opt_info[1]
        return HyperParamsData(
            optimizer_iterations=len(opt_info[0]),
            loss_alg_initial=loss_total[0],
            loss_alg_final=loss_total[-1],
            loss_alg_initial_gradient=loss_gradient_total[0],
            loss_alg_final_gradient=loss_gradient_total[-1],
            num_trainable_params=param_bag.num_trainable_params,
            initially_solvable=loss_total[0] < 1e5,
            finally_solvable=loss_total[-1] < 1e5,
        )

    def init_from_incomplete(
        self,
        J,
        J_initial_gradient,
        J_final,
        J_final_gradient,
        exp_losses_initial,
        exp_losses_initial_gradients,
        exp_losses_final,
        exp_losses_final_gradients,
        taylor_data,
    ):
        return HyperParamsData(
            optimizer_iterations=self.optimizer_iterations,
            loss_initial=J,
            loss_initial_gradient=J_initial_gradient,
            loss_final=J_final,
            loss_final_gradient=J_final_gradient,
            loss_initial_exp=tuple(exp_losses_initial),
            loss_initial_exp_gradient=tuple(exp_losses_initial_gradients),
            loss_final_exp=tuple(exp_losses_final),
            loss_final_exp_gradient=tuple(exp_losses_final_gradients),
            loss_alg_initial=self.loss_alg_initial,
            loss_alg_final=self.loss_alg_final,
            loss_alg_initial_gradient=self.loss_alg_initial_gradient,
            loss_alg_final_gradient=self.loss_alg_final_gradient,
            num_trainable_params=self.num_trainable_params,
            initially_solvable=J < 1e5,
            finally_solvable=J_final < 1e5,
            taylor_data=taylor_data,
        )

    @staticmethod
    def init_from_info(
        opt_info,
        param_bag,
        J,
        J_initial_gradient,
        J_final,
        J_final_gradient,
        exp_losses_initial,
        exp_losses_initial_gradients,
        exp_losses_final,
        exp_losses_final_gradients,
    ):
        loss_total = opt_info[0]
        loss_gradient_total = opt_info[1]
        return HyperParamsData(
            optimizer_iterations=len(opt_info[0]),
            loss_initial=J,
            loss_initial_gradient=J_initial_gradient,
            loss_final=J_final,
            loss_final_gradient=J_final_gradient,
            loss_initial_exp=exp_losses_initial,
            loss_initial_exp_gradient=exp_losses_initial_gradients,
            loss_final_exp=exp_losses_final,
            loss_final_exp_gradient=exp_losses_final_gradients,
            loss_alg_initial=loss_total[0],
            loss_alg_final=loss_total[-1],
            loss_alg_initial_gradient=loss_gradient_total[0],
            loss_alg_final_gradient=loss_gradient_total[-1],
            num_trainable_params=param_bag.num_trainable_params,
            initially_solvable=J < 1e5,
            finally_solvable=J_final < 1e5,
        )

    @property
    def keys(self):
        return list(self.asdict().keys())

    def __str__(self):
        return pprint.pformat(self.asdict())

    def asdict(self):
        return asdict(self)

    @property
    def complete(self):
        return not (
            self.loss_initial is None
            or self.loss_initial_gradient is None
            or self.loss_final is None
            or self.loss_final_gradient is None
            or self.loss_initial_exp is None
            or self.loss_initial_exp_gradient is None
            or self.loss_final_exp is None
            or self.loss_final_exp_gradient is None
            or self.taylor_data is None
            or self.invt_losses_initial is None
            or self.invt_losses_final is None
        )

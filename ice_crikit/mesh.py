import numpy as onp


def stretch_mesh(x, y, L, H, h=0.5, R=None):
    if R is None == h is None:
        raise ValueError("Must specify either h or R but not both")

    if R is None:
        if h == 0:
            return x, y
        R = h / 2 + L ** 2 / (2 * h)

    cy = H - onp.sqrt(R ** 2 - L ** 2)

    def circle_stretch(x, y):
        h = cy + onp.sqrt(R ** 2 - x ** 2)
        y = y / H * h
        return [x, y]

    return circle_stretch(x, y)


def build_mesh(L, H, nx, ny, h=0.5, R=None, eps=1e-2):
    from crikit import RectangleMesh, Point, near

    if R is None == h is None:
        raise ValueError("Must specify either h or R but not both")
    mesh = RectangleMesh(Point(0, 0), Point(L, H), nx, ny)
    if R is None:
        if h == 0:
            # Special handling for infinite radius circle.
            def top_boundary(x, on_boundary):
                return on_boundary and near(x[1], H, eps)

            def right_boundary(x, on_boundary):
                return on_boundary and near(x[0], L, eps)

            return mesh, top_boundary, right_boundary

        R = h / 2 + L ** 2 / (2 * h)

    C = mesh.coordinates()
    x, y = stretch_mesh(C[:, 0], C[:, 1], L, H, R=R)

    C_new = onp.array((x, y)).T
    mesh.coordinates()[:] = C_new

    cy = H - onp.sqrt(R ** 2 - L ** 2)

    def top_boundary(x, on_boundary):
        h = cy + onp.sqrt(R ** 2 - x[0] ** 2)
        return on_boundary and near(x[1], h, eps)

    def right_boundary(x, on_boundary):
        return on_boundary and near(x[0], L, eps)

    return mesh, top_boundary, right_boundary

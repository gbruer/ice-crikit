import tensorflow

import jax
import jax.numpy as jnp
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

from crikit import *
from matplotlib.lines import Line2D

from mesh import stretch_mesh
from plotting import plot_translucent_point_group

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)

MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"
GREY = "#CCCCCC"
GOLD = "gold"
AQUA = "aqua"
VIOLET = "violet"

point_kwargs_all = dict(facecolor=WHITE)
point_kwargs_left = dict(facecolor=MOSS_GREEN)
point_kwargs_right = dict(facecolor=VIOLET)
point_kwargs_right_large = dict(facecolor=AQUA)
point_kwargs_borehole = dict(facecolor=GOLD)
point_kwargs_surface = dict(facecolor=MINT_BLUE)


def read_file(filestem):
    filestem_xy = f"{filestem}_xy.xdmf"
    filestem = f"{filestem}.xdmf"
    # with XDMFFile(MPI.comm_world, filestem_xy) as f:
    #     mesh = Mesh()
    #     f.read(mesh)

    #     V = FunctionSpace(mesh, "P", 1)

    #     scalar_invts_quad_xy = Function(V)
    #     f.read_checkpoint(scalar_invts_quad_xy, "scalar_invts_quad_xy_1", 0)

    #     scalar_invts_quad_xy = Function(V)
    #     f.read_checkpoint(scalar_invts_quad_xy, "scalar_invts_quad_xy_2", 0)

    with XDMFFile(MPI.comm_world, filestem) as f:
        mesh = Mesh()
        f.read(mesh)

        V = FunctionSpace(mesh, "P", 1)
        coeff_func_quad = Function(V)
        f.read_checkpoint(coeff_func_quad, "coeff_func_quad", 0)

    return mesh, coeff_func_quad


@jax.vmap
def cr_func_DphiDt(
    scalar_invts, p=1.5, eps2=1e-10, gamma_f=0.05, esf=2, gamma_h=0.01, eh=1
):
    e = scalar_invts[0]
    phi = scalar_invts[1]

    def fracturing_term(_):
        return gamma_f * e * (1 - phi)

    def healing_term(_):
        return gamma_h * (e - eh)

    def zero(_):
        return 0.0

    # Fracturing term.
    DphiDt = jax.lax.cond(
        e >= ((1 - phi) ** (1 / (1 - p))) * esf,
        fracturing_term,
        zero,
        operand=None,
    )

    # Healing term.
    DphiDt += jax.lax.cond(
        jnp.logical_and(phi >= 0, e <= eh), healing_term, zero, operand=None
    )

    return DphiDt


def main():
    filename_small = f"data_xy_small.npz"
    data_small = np.load(filename_small)
    invts_small_xy = data_small["invts"]
    mask_xy_small = data_small["mask_xy"]
    xy_coords_small = data_small["xy_coords"]

    filename_large = f"data_xy_large.npz"
    data_large = np.load(filename_large)
    invts_large_xy = data_large["invts"]
    mask_xy_large = data_large["mask_xy"]
    xy_coords_large = data_large["xy_coords"]

    L = 2
    H = 1
    h = 0.5
    nx, ny = 16, 16

    borehole_x = 0.6
    borehole_delta = 0.05

    x_vals = xy_coords_small[:, 0]
    xy_coords_top_small = np.array(
        stretch_mesh(x_vals, np.ones_like(x_vals), L, H, h=h)
    ).T

    mask_left = x_vals < borehole_x - borehole_delta
    mask_right = x_vals > borehole_x + borehole_delta
    mask_borehole = np.abs(x_vals - borehole_x) <= borehole_delta
    mask_surface = np.abs(xy_coords_small[:, 1] - xy_coords_top_small[:, 1]) < 0.01

    xy_coords_left = xy_coords_small[mask_left]
    xy_coords_right = xy_coords_small[mask_right]
    xy_coords_borehole = xy_coords_small[mask_borehole]
    xy_coords_surface = xy_coords_small[mask_surface]

    x_vals = xy_coords_large[:, 0]
    mask_right_large = x_vals > borehole_x + borehole_delta
    xy_coords_right_large = xy_coords_large[mask_right_large]

    invts_left = invts_small_xy[mask_xy_small & mask_left]
    invts_right = invts_small_xy[mask_xy_small & mask_right]
    invts_borehole = invts_small_xy[mask_xy_small & mask_borehole]
    invts_surface = invts_small_xy[mask_xy_small & mask_surface]
    invts_right_large = invts_large_xy[mask_xy_large & mask_right_large]

    filestem = "data_invt_large_hull"
    mesh_large, coeff_func_quad_large = read_file(filestem)

    filestem = "data_invt_small_hull"
    mesh_small, coeff_func_quad_small = read_file(filestem)

    x_label = r"$\sqrt{\text{tr }\dot \epsilon^2}$"
    y_label = r"$\phi$"

    invts_small = coeff_func_quad_small.function_space().tabulate_dof_coordinates()
    coeff_func_quad_small.vector().set_local(cr_func_DphiDt(invts_small))

    invts_large = coeff_func_quad_large.function_space().tabulate_dof_coordinates()
    coeff_func_quad_large.vector().set_local(cr_func_DphiDt(invts_large))

    # def scale_mesh(mesh):
    #     C = mesh.coordinates()
    #     C[:, 0] = np.sqrt(C[:, 0])
    #     mesh.coordinates()[:] = C

    # scale_mesh(coeff_func_quad_small.function_space().mesh())
    # scale_mesh(coeff_func_quad_large.function_space().mesh())

    do_mesh_plot = False
    if do_mesh_plot:
        group_kwargs = dict(
            zoomable=True,
            lw=0,
            size=2,
            alpha=1,
            # color=''
        )

        plt.figure()

        plot(
            coeff_func_quad_large.function_space().mesh(),
            zorder=-1,
            alpha=0.8,
            linewidth=1,
        )

        plt.xlabel(x_label)
        plt.ylabel(y_label)

        pt = plot_translucent_point_group(
            invts_right_large[:, 0],
            invts_right_large[:, 1],
            label=r"$\text{large strain}$",
            **group_kwargs,
            # **point_kwargs_right_large,
        )

        ax = plt.gca()
        ax.set_aspect("auto")
        plt.savefig("invariant_mesh_large")

        plt.figure()

        plot(
            coeff_func_quad_small.function_space().mesh(),
            zorder=-1,
            alpha=0.8,
            linewidth=1,
        )

        plt.xlabel(x_label)
        plt.ylabel(y_label)

        plot_translucent_point_group(
            invts_left[:, 0],
            invts_left[:, 1],
            label=r"$\text{left}$",
            **group_kwargs,
            # **point_kwargs_left,
        )

        plot_translucent_point_group(
            invts_borehole[:, 0],
            invts_borehole[:, 1],
            label=r"$\text{borehole}$",
            **group_kwargs,
            # **point_kwargs_borehole,
        )

        plot_translucent_point_group(
            invts_right[:, 0],
            invts_right[:, 1],
            label=r"$\text{right}$",
            **group_kwargs,
            # **point_kwargs_right,
        )

        plot_translucent_point_group(
            invts_surface[:, 0],
            invts_surface[:, 1],
            label=r"$\text{surface}$",
            **group_kwargs,
        )

        # plt.xlim(left=0)
        ax = plt.gca()
        ax.set_aspect("auto")

        # Add a tick at 1 (for healing threshold).
        # plt.xticks(list(plt.xticks()[0]) + [1])

        # plt.xlim(left=0, right=invts_small[:, 0].max())

        # plt.legend(bbox_to_anchor=(-0.3, 0.5), loc="center right", borderaxespad=0.0)

        plt.savefig("invariant_mesh_small")

    # plt.show()

    # sys.exit(1)

    group_kwargs = dict(
        zoomable=True,
        lw=0,
        size=2,
        alpha=0.6,
    )

    plt.figure()
    cbs = []

    levels = 40

    locator = matplotlib.ticker.MaxNLocator(nbins=levels, symmetric=True)

    divnorm = colors.TwoSlopeNorm(vcenter=0)
    colors_negative = plt.cm.seismic(np.linspace(0, 0.5, levels))
    colors_positive = plt.cm.seismic(np.linspace(0.5, 1, levels))
    all_colors = np.vstack((colors_negative, colors_positive))
    divmap = colors.LinearSegmentedColormap.from_list("divmap", all_colors)

    cb = plt.colorbar(
        plot(
            coeff_func_quad_large,
            cmap=divmap,
            norm=divnorm,
            levels=levels,
            locator=locator,
        )
    )
    cbs.append(cb)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    pt = plot_translucent_point_group(
        invts_right_large[:, 0],
        invts_right_large[:, 1],
        label=r"$\text{large strain}$",
        **group_kwargs,
        **point_kwargs_right_large,
    )

    ax = plt.gca()
    large_strain_lines = ax.get_lines()
    ax.set_aspect("auto")
    plt.savefig("true_cr_invariant_large_partitioned")

    plt.figure()

    divnorm = colors.TwoSlopeNorm(vcenter=0)
    cb = plt.colorbar(
        plot(
            coeff_func_quad_small,
            cmap=divmap,
            norm=divnorm,
            vmax=cb.vmax,
            levels=levels,
            locator=locator,
        )
    )
    cbs.append(cb)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plot_translucent_point_group(
        invts_left[:, 0],
        invts_left[:, 1],
        label=r"$\text{left}$",
        **group_kwargs,
        **point_kwargs_left,
    )

    plot_translucent_point_group(
        invts_borehole[:, 0],
        invts_borehole[:, 1],
        label=r"$\text{borehole}$",
        **group_kwargs,
        **point_kwargs_borehole,
    )

    plot_translucent_point_group(
        invts_right[:, 0],
        invts_right[:, 1],
        label=r"$\text{right}$",
        **group_kwargs,
        **point_kwargs_right,
    )

    surface_kwargs = dict(group_kwargs, **point_kwargs_surface, size=5)
    plot_translucent_point_group(
        invts_surface[:, 0],
        invts_surface[:, 1],
        label=r"$\text{surface}$",
        **surface_kwargs,
    )

    plt.xlim(left=0)

    # Add a tick at 1 (for healing threshold).
    plt.xticks(list(plt.xticks()[0]) + [1])

    plt.xlim(left=0, right=invts_small[:, 0].max())

    ax = plt.gca()
    ax.set_aspect("auto")
    for l in large_strain_lines:
        l2 = Line2D([], [])
        l2.update_from(l)
        ax.add_line(l2)

    # plt.legend(bbox_to_anchor=(-0.3, 0.5), loc="center right", borderaxespad=0.0)

    plt.savefig("true_cr_invariant_small_partitioned")

    # Now plot them again but with different colorbars.
    vmin = min(cb.vmin for cb in cbs)
    vmax = max(cb.vmax for cb in cbs)
    print("vmin:", vmin)
    print("vmax:", vmax)

    # d = coeff_func_quad_small.vector().get_local()
    # pos_min = d[d > 0].min()
    # print('pos_min:', pos_min)
    levels_min_neg = -0.01
    levels_min_pos = 0.1
    levels_max_pos = 1
    vmin_pos = 0.0
    levels_neg = np.linspace(levels_min_neg, 0, 6)
    levels_pos = np.linspace(levels_min_pos, levels_max_pos, 19)

    plt.figure()
    f = plot(
        coeff_func_quad_large, vmin=vmin_pos, vmax=vmax, levels=levels_pos, cmap="Reds"
    )
    plt.colorbar(f, location="left", pad=0.2)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    pt = plot_translucent_point_group(
        invts_right_large[:, 0],
        invts_right_large[:, 1],
        label=r"$\text{large strain}$",
        **group_kwargs,
        **point_kwargs_right_large,
    )

    ax = plt.gca()
    large_strain_lines = ax.get_lines()
    ax.set_aspect("auto")
    plt.savefig("true_cr_invariant_pos_neg_large_partitioned")

    plt.figure()

    f = plot(
        coeff_func_quad_small, vmin=vmin_pos, vmax=vmax, levels=levels_pos, cmap="Reds"
    )
    f = plot(
        coeff_func_quad_small,
        vmin=levels_min_neg,
        vmax=0,
        levels=levels_neg,
        cmap="Blues_r",
    )
    plt.colorbar(f, location="left", pad=0.22)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plot_translucent_point_group(
        invts_left[:, 0],
        invts_left[:, 1],
        label=r"$\text{left}$",
        **group_kwargs,
        **point_kwargs_left,
    )

    plot_translucent_point_group(
        invts_borehole[:, 0],
        invts_borehole[:, 1],
        label=r"$\text{borehole}$",
        **group_kwargs,
        **point_kwargs_borehole,
    )

    plot_translucent_point_group(
        invts_right[:, 0],
        invts_right[:, 1],
        label=r"$\text{right}$",
        **group_kwargs,
        **point_kwargs_right,
    )

    surface_kwargs = dict(group_kwargs, **point_kwargs_surface, size=5)
    plot_translucent_point_group(
        invts_surface[:, 0],
        invts_surface[:, 1],
        label=r"$\text{surface}$",
        **surface_kwargs,
    )

    plt.xlim(left=0)

    plt.xticks([0, 1, 2, 3, 4])

    plt.xlim(left=0, right=invts_small[:, 0].max())

    ax = plt.gca()
    ax.set_aspect("auto")
    for l in large_strain_lines:
        l2 = Line2D([], [])
        l2.update_from(l)
        ax.add_line(l2)

    # plt.legend(bbox_to_anchor=(-0.3, 0.5), loc="center right", borderaxespad=0.0)

    plt.savefig("true_cr_invariant_pos_neg_small_partitioned")

    plt.show()


if __name__ == "__main__":
    main()

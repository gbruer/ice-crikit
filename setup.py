from setuptools import setup, find_packages
from itertools import chain
import os
import pathlib

python_requires = ">=3.7"
install_requires = [
    "dolfin-adjoint >= 2019.1.1",
    "numpy",
    "matplotlib",
    "shapely",
    # "crikit",
    "alphashape",
    "jax",
    "flax",
    "jaxlib",
]
extras_requires = {
    "dev": ["black", "pre-commit"],
}

extras_requires["all"] = list(chain(*extras_requires.values()))

classifiers = [
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.7",
]

url = "https://gitlab.com/gbruer/ice-crikit"

version_file = os.path.join(os.path.dirname(__file__), "ice_crikit/_version.py")
with open(version_file, "r") as f:
    version = f.read().split("=", 1)[1].strip(" \n\"'")


HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()

setup(
    name="ice-crikit",
    version=version,
    description="Constitutive Relation Inference Toolkit",
    long_description=README,
    long_description_content_type="text/markdown",
    author="Grant Bruer, Tobin Isaac",
    classifiers=classifiers,
    url=url,
    packages=['ice_crikit'],
    python_requires=python_requires,
    install_requires=install_requires,
    extras_require=extras_requires,
)

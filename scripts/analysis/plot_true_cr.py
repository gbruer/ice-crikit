import tensorflow

import jax
import jax.numpy as jnp
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

from crikit import *

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)

MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"
GREY = "#CCCCCC"

point_kwargs_all = dict(facecolor=WHITE)
point_kwargs_left = dict(facecolor=MOSS_GREEN)
point_kwargs_right = dict(facecolor=PURPLE)
point_kwargs_right_large = dict(facecolor=ORANGE)
point_kwargs_close = dict(facecolor=GREY)
point_kwargs_surface = dict(facecolor=MINT_BLUE)


def read_file(filestem, do_xy=False):
    filestem_xy = f"{filestem}_xy.xdmf"
    filestem = f"{filestem}.xdmf"
    if do_xy:
        with XDMFFile(MPI.comm_world, filestem_xy) as f:
            mesh = Mesh()
            f.read(mesh)

            plt.figure()
            plot(mesh)
            plt.title("xy mesh")
            plt.show()

            V = FunctionSpace(mesh, "P", 1)
            coeff_func_xy_quad = Function(V)
            f.read_checkpoint(coeff_func_xy_quad, "coeff_func_xy_quad", 0)

            plt.figure()
            plt.colorbar(plot(coeff_func_xy_quad))
            plt.title("coeff func xy quad")
            plt.show()

            scalar_invts_quad_xy = Function(V)
            f.read_checkpoint(scalar_invts_quad_xy, "scalar_invts_quad_xy_1", 0)

            plt.figure()
            plt.colorbar(plot(scalar_invts_quad_xy))
            plt.title("invt xy 1 quad")
            plt.show()

            scalar_invts_quad_xy = Function(V)
            f.read_checkpoint(scalar_invts_quad_xy, "scalar_invts_quad_xy_2", 0)

            plt.figure()
            plt.colorbar(plot(scalar_invts_quad_xy))
            plt.title("invt xy 2 quad")
            plt.show()
        return

    with XDMFFile(MPI.comm_world, filestem) as f:
        mesh = Mesh()
        f.read(mesh)

        V = FunctionSpace(mesh, "P", 1)
        coeff_func_quad = Function(V)
        f.read_checkpoint(coeff_func_quad, "coeff_func_quad", 0)

    return mesh, coeff_func_quad


@jax.vmap
def cr_func_DphiDt(
    scalar_invts, p=1.5, eps2=1e-10, gamma_f=0.05, esf=2, gamma_h=0.01, eh=1
):
    e = scalar_invts[0]
    phi = scalar_invts[1]

    def fracturing_term(_):
        return gamma_f * e * (1 - phi)

    def healing_term(_):
        return gamma_h * (e - eh)

    def zero(_):
        return 0.0

    # Fracturing term.
    DphiDt = jax.lax.cond(
        e >= ((1 - phi) ** (1 / (1 - p))) * esf,
        fracturing_term,
        zero,
        operand=None,
    )

    # Healing term.
    DphiDt += jax.lax.cond(
        jnp.logical_and(phi >= 0, e <= eh), healing_term, zero, operand=None
    )

    return DphiDt


def main():
    filestem = "data_invt_large"
    mesh_large, coeff_func_quad_large = read_file(filestem)

    filestem = "data_invt_small"
    mesh_small, coeff_func_quad_small = read_file(filestem)

    x_label = r"$\sqrt{\text{tr }\dot \epsilon^2}$"
    y_label = r"$\phi$"

    invts_small = coeff_func_quad_small.function_space().tabulate_dof_coordinates()
    coeff_func_quad_small.vector().set_local(cr_func_DphiDt(invts_small))

    invts_large = coeff_func_quad_large.function_space().tabulate_dof_coordinates()
    coeff_func_quad_large.vector().set_local(cr_func_DphiDt(invts_large))

    # def scale_mesh(mesh):
    #     C = mesh.coordinates()
    #     C[:, 0] = np.sqrt(C[:, 0])
    #     mesh.coordinates()[:] = C

    # scale_mesh(coeff_func_quad_small.function_space().mesh())
    # scale_mesh(coeff_func_quad_large.function_space().mesh())

    cbs = []

    plt.figure()

    divnorm = colors.TwoSlopeNorm(vcenter=0)
    cb = plt.colorbar(
        plot(coeff_func_quad_small, cmap="seismic", norm=divnorm, levels=100)
    )
    cbs.append(cb)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.xlim(left=0)

    # Add a tick at 1 (for healing threshold).
    plt.xticks(list(plt.xticks()[0]) + [1])

    plt.xlim(left=0, right=invts_small[:, 0].max())
    # plot(coeff_func_quad_small.function_space().mesh(), alpha=0.3)

    ax = plt.gca()
    ax.set_aspect("auto")
    plt.savefig("true_cr_invariant_small")

    plt.figure()

    divnorm = colors.TwoSlopeNorm(vcenter=0)
    cb = plt.colorbar(
        plot(coeff_func_quad_large, cmap="seismic", norm=divnorm, levels=40)
    )
    cbs.append(cb)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    # plot(coeff_func_quad_large.function_space().mesh(), alpha=0.3)

    ax = plt.gca()
    ax.set_aspect("auto")
    plt.savefig("true_cr_invariant_large")

    # Now plot them again but with different colorbars.
    vmin = min(cb.vmin for cb in cbs)
    vmax = max(cb.vmax for cb in cbs)
    print("vmin:", vmin)
    print("vmax:", vmax)

    plt.figure()

    # d = coeff_func_quad_small.vector().get_local()
    # pos_min = d[d > 0].min()
    # print('pos_min:', pos_min)
    levels_min_pos = 0.1
    levels_max_pos = 1.1
    vmin_pos = 0.0
    levels_neg = np.linspace(vmin, 0, 6)
    levels_pos = np.linspace(levels_min_pos, levels_max_pos, 21)
    f = plot(
        coeff_func_quad_small, vmin=vmin_pos, vmax=vmax, levels=levels_pos, cmap="Reds"
    )
    f = plot(
        coeff_func_quad_small, vmin=vmin, vmax=0, levels=levels_neg, cmap="Blues_r"
    )
    plt.colorbar(f, location="left", pad=0.22)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.xlim(left=0)

    plt.xticks([0, 1, 2, 3, 4])

    plt.xlim(left=0, right=invts_small[:, 0].max())

    ax = plt.gca()
    ax.set_aspect("auto")
    plt.savefig("true_cr_invariant_pos_neg_small")

    plt.figure()

    f = plot(
        coeff_func_quad_large, vmin=vmin_pos, vmax=vmax, levels=levels_pos, cmap="Reds"
    )
    plt.colorbar(f, location="left", pad=0.2)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    ax = plt.gca()
    ax.set_aspect("auto")
    plt.savefig("true_cr_invariant_pos_neg_large")

    plt.show()


if __name__ == "__main__":
    main()

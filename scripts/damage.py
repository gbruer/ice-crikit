import tensorflow
from crikit.cr.quadrature import make_quadrature_spaces, make_quadrature_space
from crikit import *
from crikit.cr.space_builders import DirectSum
from crikit.cr.map_builders import FunnelOut

from pyadjoint.overloaded_type import create_overloaded_object as coo
from pyadjoint.reduced_functional_numpy import ReducedFunctionalNumPy
from pyadjoint_utils.jax_adjoint import to_adjfloat
from jax import numpy as jnp
from pprint import pprint

import argparse
import csv
import crikit.utils as utils
import dataclasses
import jax
import matplotlib.pyplot as plt
import numpy as onp
import pathlib

from ice_crikit.cache import PickleDictionaryCache
from ice_crikit.cr import make_jax_cr_tau, make_jax_network_cr, make_jax_cr, make_ufl_invariants_cr, make_ufl_cr
from ice_crikit.cr_plotter import InvariantCRPlotter
from ice_crikit.experiment import Experiment
from ice_crikit.hyper_param_utils import HyperParams, HyperParamsData
from ice_crikit.mesh import build_mesh
from ice_crikit.network_builders import BatchStats
from ice_crikit.param_utils import Param, ParamBag, ConstantParam

import logging

ffc_logger = logging.getLogger("FFC")
ffc_logger.setLevel(logging.WARNING)
from crikit import logging


SMALL_SIZE = 16
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "text.usetex": True,
        # "font.family": "serif",
        # "font.serif": ["Palatino"],
        # 'figure.figsize': [6, 4],
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        # 'axes.grid': True,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)


def get_quadrature_dx(el, dx):
    if el.family() == "Quadrature":
        quad_params = {
            "quadrature_rule": el.quadrature_scheme(),
            "quadrature_degree": el.degree(),
            "representation": "quadrature",
        }
        dx = dx(metadata=quad_params)
    return dx


@overload_jax
def direct_loss(true, pred):
    return ((true.flatten() - pred.flatten()) ** 2).mean()


def run_direct(inputs_ufl, cr_true, cr, controls):
    with stop_annotating():
        cr_true_np_out = get_composite_cr(
            cr_true,
            Ndarrays((-1,)),
            # domain=cr_mesh.ufl_domain(),
            # quad_params=quad_params,
        )
        true_np = cr_true_np_out(inputs_ufl)

    with push_tape():
        project_invts_to_np = get_composite_cr(
            cr_true.source,
            cr.source,
        )
        invts_np = project_invts_to_np(inputs_ufl)
        pred_np = cr(invts_np)
        J_jax = direct_loss(true_np, pred_np)
        J = to_adjfloat(J_jax)
        Jhat = ReducedFunctional(J, controls)
    onp.savez("training_data", x=invts_np, y=true_np)
    return Jhat



def save_experiment_output(w_initial, w_trained, name):
    filename = f"{name}.xdmf"
    comm = w_initial[0].function_space().mesh().mpi_comm()

    def write_mixed_element_function(f, w, name, *args, **kwargs):
        subspaces = [u.function_space().collapse() for u in w.split()]
        w_subs = [Function(V) for V in subspaces]
        fa = FunctionAssigner(subspaces, w.function_space())
        fa.assign(w_subs, w)
        for i, wi in enumerate(w_subs):
            f.write_checkpoint(wi, f"{name}_{i}", *args, **kwargs)

    # Write the functions to the file.
    with XDMFFile(comm, filename) as f:
        f.write(w_initial[0].function_space().mesh())
        for i, w in enumerate(w_initial):
            write_mixed_element_function(
                f, w, "w_initial", 0, XDMFFile.Encoding.HDF5, append=True
            )

        if w_trained is not None:
            for i, w in enumerate(w_trained):
                write_mixed_element_function(
                    f, w, "w_trained", 0, XDMFFile.Encoding.HDF5, append=True
                )


def ad_norm(grad_loss):
    return sum(onp.sqrt(coo(g)._ad_dot(g)) for g in grad_loss)


def convert_dict_to_dataclass(d, name="unnamed"):
    fields = []
    for k, v in d.items():
        if isinstance(v, dict):
            v = convert_dict_to_dataclass(v, k)
        elif isinstance(v, list):
            v = tuple(v)

        fields.append((k, "typing.Any", v))
    dict_class = dataclasses.make_dataclass(name, fields, frozen=True)
    return dict_class()


def get_true_invariant_data(
    hyper_params, cr_plotters, cr_inputs_ufl, get_cb=False, plots_dir=None
):
    p = Constant(hyper_params.p_true, name="p_true_invariants")
    eps2 = Constant(hyper_params.eps2, name="eps2_invariants")
    gamma_f = Constant(hyper_params.gamma_f_true, name="gamma_f_true_invariants")
    esf = Constant(hyper_params.esf_true, name="esf_true_invariants")
    gamma_h = Constant(hyper_params.gamma_h_true, name="gamma_h_true_invariants")
    eh = Constant(hyper_params.eh_true, name="eh_true_invariants")

    cr_true_inner_func, cr_true_scalar_invt = make_ufl_invariants_cr(
        p, hyper_params.dim, eps2, gamma_f, esf, gamma_h, eh, linear_invt=True
    )

    invariant_data_trues = []
    for cr_plotter in cr_plotters:
        invariant_data_true = cr_plotter.compute(
            cr_true_scalar_invt,
            cr_true_inner_func,
            cr_inputs_ufl,
        )

        if get_cb and plots_dir is not None:
            true_cb, true_cb_xy, _ = cr_plotter.plot_cr_all(
                data=invariant_data_true,
                prefix=f"Correct CR {cr_plotter.name}: ",
                skip_plot=True,
            )
        else:
            true_cb = None
            true_cb_xy = None
        invariant_data_trues.append((invariant_data_true, true_cb, true_cb_xy))
        invariant_data_true.record_to_file_functions(f"data_invt_{cr_plotter.name}")
    return invariant_data_trues


def run_network_cr_batch(
    cr_DphiDt,
    hyper_params,
    network=None,
    cr_DphiDt_trained=None,
    hyper_params_data=None,
    trained_params_val=None,
    run_training=None,
    run_testing=None,
    y_trues_train=None,
    y_trues_test=None,
    w_trues=None,
    observer=None,
    loss=None,
    verify=False,
    cr_plotters=None,
    cr_inputs_ufl=None,
    dirpath=None,
    plots_dir=None,
    get_true_data=False,
    invariant_data_trues=None,
    xy_use_true=False,
):
    p_np = array(jnp.array(hyper_params.p_init))
    eps2_np = array(jnp.array(hyper_params.eps2))

    if xy_use_true and w_trues is None:
        raise ValueError("w_trues must be given in order to use it as the XY data")

    if not (xy_use_true or xy_use_true is None) and run_training is None:
        raise ValueError(
            "To use experiment XY data (xy_use_true=False), you must run the experiment."
        )

    if cr_plotters is None:
        cr_plotters = []

    (
        scalar_invt_map,
        form_invt_map,
        inner_map,
        coeff_form_map,
    ) = cr_DphiDt.get_point_maps()

    if dirpath is None:
        dirpath = pathlib.Path()

    if plots_dir is not None:
        save_dir_previous = utils.FIGURE_SAVE_DIR
        utils.FIGURE_SAVE_DIR = plots_dir

    with push_tape():
        if run_training is not None:
            if network is not None:
                all_params = network.get_flat_params()
                param_bag = ParamBag(all_params)

            cr_tau = make_jax_cr_tau(p_np, eps2_np, hyper_params.dim, print_cr=False)

            funnel = FunnelOut(cr_tau.source, 2)
            parallel = ParallelPointMap(cr_tau, cr_DphiDt)
            cr = get_composite_cr(funnel, parallel)

            run_kwargs = dict(observer=observer, do_plots=False, ufl=False, disp=False)
            y_preds_train, w_preds_train = run_training(cr, **run_kwargs)

            if y_trues_train is not None:
                exp_losses = [
                    loss(y_true, y_pred)
                    for y_true, y_pred in zip(y_trues_train, y_preds_train)
                ]
                J = sum(sum(losses) for losses in exp_losses)

                if network is not None:

                    c = param_bag.controls
                    Jhat = ReducedFunctional(J, c)
                    exp_losses_rfs = [
                        tuple(ReducedFunctional(l, c) for l in losses)
                        for losses in exp_losses
                    ]

                    trainable_params_vals = list(param_bag.get_vals("trainable"))
                    J = Jhat(trainable_params_vals)
                    J_initial_gradient = Jhat.derivative()
                    J_initial_gradient_norm = ad_norm(J_initial_gradient)

                    exp_losses_initial = [
                        tuple(Control(l).tape_value() for l in losses)
                        for losses in exp_losses
                    ]
                    exp_losses_initial_gradients = [
                        tuple(rf.derivative() for rf in rfs) for rfs in exp_losses_rfs
                    ]
                    exp_losses_initial_gradients = [
                        tuple(ad_norm(g) for g in gs)
                        for gs in exp_losses_initial_gradients
                    ]
                    if trained_params_val is not None:
                        J_final = Jhat(trained_params_val)
                        J_final_gradient = Jhat.derivative()
                        J_final_gradient_norm = ad_norm(J_final_gradient)
                        exp_final_losses = [
                            tuple(Control(l).tape_value() for l in losses)
                            for losses in exp_losses
                        ]
                        exp_losses_final_gradients = [
                            tuple(rf.derivative() for rf in rfs)
                            for rfs in exp_losses_rfs
                        ]
                        exp_losses_final_gradients = [
                            tuple(ad_norm(g) for g in gs)
                            for gs in exp_losses_final_gradients
                        ]
                        w_preds_train_final = [
                            Control(w).tape_value() for w in w_preds_train
                        ]

                        if verify:
                            m = trained_params_val
                            h = J_final_gradient
                            epsilons = [1.0 / 2 ** i for i in range(12)]
                            taylor_data = taylor_to_dict(
                                Jhat, m, h, Hm=0, epsilons=epsilons
                            )
                        else:
                            taylor_data = None

                        hyper_params_data = hyper_params_data.init_from_incomplete(
                            J,
                            J_initial_gradient_norm,
                            J_final,
                            J_final_gradient_norm,
                            exp_losses,
                            exp_losses_initial_gradients,
                            exp_final_losses,
                            exp_losses_final_gradients,
                            taylor_data,
                        )

                        path_hyper_params = dirpath / "hyper_params_data.txt"
                        with path_hyper_params.open("w") as f:
                            pprint(hyper_params_data.asdict(), stream=f)
                        path_experiment_output = dirpath / "experiment_output"
                        save_experiment_output(
                            w_preds_train, w_preds_train_final, path_experiment_output
                        )

    refresh_cache = False
    with stop_annotating():
        if xy_use_true is not None:
            w = w_trues[0] if xy_use_true else w_preds_train[0]
            cr_inputs_ufl = Experiment.get_cr_inputs(w, ())

        if get_true_data:
            invariant_data_trues = get_true_invariant_data(
                hyper_params,
                cr_plotters,
                cr_inputs_ufl,
                get_cb=True,
                plots_dir=plots_dir,
            )

        invariant_data_preds = []
        invariant_data_losses = []
        for cr_plotter, (invariant_data_true, true_cb, true_cb_xy) in zip(
            cr_plotters, invariant_data_trues
        ):
            filepath = dirpath / f"cr_output_initial_{cr_plotter.name}.npz"
            if filepath.exists() and not refresh_cache:
                invariant_data_pred = cr_plotter.compute_from_file(
                    scalar_invt_map,
                    inner_map,
                    filepath,
                )
            else:
                invariant_data_pred = cr_plotter.compute(
                    scalar_invt_map,
                    inner_map,
                    cr_inputs_ufl,
                )
                invariant_data_pred.record_to_file(filepath)

            if plots_dir is not None:
                # Plot untrained CR output.
                cr_plotter.plot_cr_all(
                    data=invariant_data_pred,
                    true_data=invariant_data_true,
                    true_cb=true_cb,
                    true_cb_xy=true_cb_xy,
                    prefix=f"Untrained CR {cr_plotter.name}: ",
                )
            invariant_data_preds.append(invariant_data_pred)

            if hyper_params_data.invt_losses_initial is None:
                p = invariant_data_pred.coeff_func_quad
                t = invariant_data_true.coeff_func_quad
                quad_dx = get_quadrature_dx(p.ufl_element(), dx)
                misfit = assemble((p - t) ** 2 * quad_dx)
                invariant_data_losses.append(misfit)

        if hyper_params_data.invt_losses_initial is None:
            hyper_params_data = dataclasses.replace(
                hyper_params_data, invt_losses_initial=tuple(invariant_data_losses)
            )
            path_hyper_params = dirpath / "hyper_params_data.txt"
            with path_hyper_params.open("w") as f:
                pprint(hyper_params_data.asdict(), stream=f)

        if cr_DphiDt_trained is not None:

            (
                scalar_invt_map,
                form_invt_map,
                inner_map,
                coeff_form_map,
            ) = cr_DphiDt_trained.get_point_maps()

            invariant_data_preds = []
            invariant_data_losses = []
            for cr_plotter, (invariant_data_true, true_cb, true_cb_xy) in zip(
                cr_plotters, invariant_data_trues
            ):
                filepath = dirpath / f"cr_output_trained_{cr_plotter.name}.npz"
                if filepath.exists() and not refresh_cache:
                    invariant_data_pred = cr_plotter.compute_from_file(
                        scalar_invt_map,
                        inner_map,
                        filepath,
                    )
                else:
                    invariant_data_pred = cr_plotter.compute(
                        scalar_invt_map,
                        inner_map,
                        cr_inputs_ufl,
                    )
                    invariant_data_pred.record_to_file(filepath)

                if plots_dir is not None:
                    # Plot trained CR output in a large strain rate range.
                    cr_plotter.plot_cr_all(
                        data=invariant_data_pred,
                        true_data=invariant_data_true,
                        true_cb=true_cb,
                        true_cb_xy=true_cb_xy,
                        prefix=f"Trained CR {cr_plotter.name}: ",
                    )
                invariant_data_preds.append(invariant_data_pred)

                if hyper_params_data.invt_losses_final is None:
                    p = invariant_data_pred.coeff_func_quad
                    t = invariant_data_true.coeff_func_quad
                    quad_dx = get_quadrature_dx(p.ufl_element(), dx)
                    misfit = assemble((p - t) ** 2 * quad_dx)
                    invariant_data_losses.append(misfit)

            if hyper_params_data.invt_losses_final is None:
                hyper_params_data = dataclasses.replace(
                    hyper_params_data, invt_losses_final=tuple(invariant_data_losses)
                )
                path_hyper_params = dirpath / "hyper_params_data.txt"
                with path_hyper_params.open("w") as f:
                    pprint(hyper_params_data.asdict(), stream=f)

    if get_true_data:
        true_kwargs = dict(
            invariant_data_trues=invariant_data_trues,
        )
        rv = (
            invariant_data_trues,
            true_kwargs,
            invariant_data_preds,
        )
    else:
        rv = (invariant_data_preds,)

    if hyper_params_data is not None:
        rv += (hyper_params_data,)

    if plots_dir is not None:
        utils.FIGURE_SAVE_DIR = save_dir_previous

    return rv


def run_network_cr(
    args,
    hyper_params,
    cr_true,
    run_training,
    run_testing,
    y_trues_train,
    y_trues_test,
    w_trues,
    ex,
    loss,
):

    p_np = array(jnp.array(args.p_init))
    eps2_np = array(jnp.array(args.eps2))

    p = Constant(args.p_true, name="p_true_invariants")
    eps2 = Constant(args.eps2, name="eps2_invariants")
    gamma_f = Constant(args.gamma_f_true, name="gamma_f_true_invariants")
    esf = Constant(args.esf_true, name="esf_true_invariants")
    gamma_h = Constant(args.gamma_h_true, name="gamma_h_true_invariants")
    eh = Constant(args.eh_true, name="eh_true_invariants")

    cr_tau = make_jax_cr_tau(p_np, eps2_np, args.dim)

    cr_true_inner_func, cr_true_scalar_invt = make_ufl_invariants_cr(
        p, args.dim, eps2, gamma_f, esf, gamma_h, eh
    )
    cr_inputs_ufl = Experiment.get_cr_inputs(w_trues[0], ())

    cr_DphiDt, network = make_jax_network_cr(
        seed=args.seed,
        dim=args.dim,
        hidden_activation=args.activation,
        layer_sizes=args.layer_sizes,
        weights_file=args.weights_file,
    )

    (
        scalar_invt_map,
        form_invt_map,
        inner_map,
        coeff_form_map,
    ) = cr_DphiDt.get_point_maps()

    print("Computing batch stats")
    with stop_annotating():
        # Set input batch stats.
        cr_invariants_ufl = cr_true_scalar_invt(cr_inputs_ufl)
        project_invariants_np = get_composite_cr(
            cr_true_scalar_invt.target,
            Ndarrays((-1, 3)),
        )
        invariants_np = project_invariants_np(cr_invariants_ufl)
        invariants_network_np = jax.vmap(network.input_transform)(invariants_np)

        input_batch_stats = BatchStats()
        input_batch_stats.set_stats_from_data(invariants_network_np)
        print("Input:", input_batch_stats)
        if network.input_batch_stats is None:
            network.input_batch_stats = input_batch_stats
        print("Using:", network.input_batch_stats)
        print()

    print("Saving initial network to file")
    with stop_annotating():
        network.save_params_to_file("params_initial.npz")

    if not args.batch:
        # Plot correct CR output.
        print("Plotting initial CR")
        invariant_names = [
            r"tr $\dot\epsilon$",
            r"$\text{tr }\dot\epsilon^2$",
            r"$\phi$",
        ]
        coeff_names = [r"$\frac{D\phi}{Dt}$"]
        with stop_annotating():
            cr_plotter = InvariantCRPlotter(
                cr_DphiDt.cr_input_shape[0],
                cr_DphiDt.num_scalar_functions,
                w_trues[0].function_space().mesh(),
                quad_params=ex.quad_params,
                invariant_cmap=INVARIANT_CMAP,
                xy_cmap=XY_CMAP,
                invariant_names=invariant_names,
                coeff_names=coeff_names,
                invariant_idxs=(1, 2),
                x_range=(20, 4900),
                y_range=(0.08, 0.16),
                name="large",
            )

            true_cb, true_cb_xy, true_data = cr_plotter.plot_cr_all(
                cr_true_scalar_invt,
                cr_true_inner_func,
                cr_inputs_ufl,
                prefix="Correct CR: ",
                skip_plot=args.batch,
            )

            cr_plotter_small = InvariantCRPlotter(
                cr_DphiDt.cr_input_shape[0],
                cr_DphiDt.num_scalar_functions,
                w_trues[0].function_space().mesh(),
                quad_params=ex.quad_params,
                invariant_cmap=INVARIANT_CMAP,
                xy_cmap=XY_CMAP,
                invariant_names=invariant_names,
                coeff_names=coeff_names,
                invariant_idxs=(1, 2),
                x_range=(0, 20),
                y_range=(0, 0.15),
                name="small",
            )

            (
                true_cb_small,
                true_cb_xy_small,
                true_data_small,
            ) = cr_plotter_small.plot_cr_all(
                cr_true_scalar_invt,
                cr_true_inner_func,
                cr_inputs_ufl,
                prefix="Correct CR (small): ",
                skip_plot=args.batch,
            )

        with stop_annotating():
            # Plot untrained CR output in a large strain rate range.
            cr_plotter.plot_cr_all(
                scalar_invt_map,
                inner_map,
                cr_inputs_ufl,
                true_data=true_data,
                true_cb=true_cb,
                true_cb_xy=true_cb_xy,
                prefix="Untrained CR: ",
            )

            # Plot untrained CR output in the low strain rate range.
            cr_plotter_small.plot_cr_all(
                scalar_invt_map,
                inner_map,
                cr_inputs_ufl,
                true_data=true_data_small,
                true_cb=true_cb_small,
                true_cb_xy=true_cb_xy_small,
                prefix="Untrained CR (small): ",
            )

        print("Finished plotting")

    funnel = FunnelOut(cr_tau.source, 2)
    parallel = ParallelPointMap(cr_tau, cr_DphiDt)
    cr = get_composite_cr(funnel, parallel)

    all_params = network.get_flat_params()
    param_bag = ParamBag(all_params)

    print()
    print("----------------------------")
    print(param_bag)
    print("----------------------------")

    if args.bootstrap:
        Jhat = run_direct(
            cr_invariants_ufl, cr_true_inner_func, inner_map, param_bag.controls
        )
        trained_params_val, iter_info = optimize_params(
            Jhat,
            record_file="loss_network_cr_direct.csv",
            bounds=param_bag.bounds,
            display_params=args.opt_params,
            method=args.optimizer,
            disp=args.opt_output,
            options=dict(gtol=args.gtol, ftol=args.ftol),
        )

        # Set params of CR to the new trained params.
        with stop_annotating():
            param_bag.set_trainable_param_vals(trained_params_val)
            cr_DphiDt.set_params(param_bag.get_vals())

            network.save_params_to_file("params_direct.npz")

        if not args.batch:
            with stop_annotating():
                # Plot untrained CR output in a large strain rate range.
                cr_plotter.plot_cr_all(
                    scalar_invt_map,
                    inner_map,
                    cr_inputs_ufl,
                    true_data=true_data,
                    true_cb=true_cb,
                    true_cb_xy=true_cb_xy,
                    prefix="Bootstrapped CR: ",
                )

                # Plot untrained CR output in the low strain rate range.
                cr_plotter_small.plot_cr_all(
                    scalar_invt_map,
                    inner_map,
                    cr_inputs_ufl,
                    true_data=true_data_small,
                    true_cb=true_cb_small,
                    true_cb_xy=true_cb_xy_small,
                    prefix="Boostrapped CR (small): ",
                )
    y_preds_train, w_preds_train = run_training(
        cr, title="Untrained CR", ufl=False, disp=args.debug
    )
    y_preds_test, w_preds_test = run_testing(
        cr, title="Untrained CR", ufl=False, disp=args.debug
    )

    exp_losses = [
        loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_train, y_preds_train)
    ]
    print("exp_losses:", exp_losses)
    J = sum(sum(losses) for losses in exp_losses)
    print()
    print("   Init loss: %g" % J)
    if onp.isnan(J):
        raise ValueError("Error: loss is NaN")

    c = param_bag.controls
    Jhat = ReducedFunctional(J, c)
    exp_losses_rfs = [
        tuple(ReducedFunctional(l, c) for l in losses) for losses in exp_losses
    ]

    trainable_params_vals = list(param_bag.get_vals("trainable"))
    for k in range(20):
        if J < 1e10:
            print("The network worked!")
            break
        print(k, "Retrying with smaller network weights")
        alpha = 0.5 ** 0.5
        with stop_annotating():
            for i, v in enumerate(trainable_params_vals):
                trainable_params_vals[i] = v._ad_mul(alpha)

        print(trainable_params_vals)
        print("   Running experiment")
        J = Jhat(trainable_params_vals)
        print("   Loss: %g" % J)

    if k != 0:
        print("Saving line search weights to file")
        param_bag.set_trainable_param_vals(trainable_params_vals)
        cr_DphiDt.set_params(param_bag.get_vals())
        network.save_params_to_file("params_initial_linesearch.npz")

    if args.verify:
        m = param_bag.get_vals("trainable")
        h = tuple(param.h for param in param_bag.trainable_params)
        epsilons = [1.0 / 2 ** i for i in range(12)]
        d = taylor_to_dict(Jhat, m, h, Hm=0, epsilons=epsilons)
        pprint(d)

    if k == 0 and not args.verify:
        # Need to run ReducedFunctional to get gradients.
        J = Jhat(trainable_params_vals)

    J_initial_gradient = Jhat.derivative()
    exp_losses_initial = [
        tuple(Control(l).tape_value() for l in losses) for losses in exp_losses
    ]
    exp_losses_initial_gradients = [
        tuple(rf.derivative() for rf in rfs) for rfs in exp_losses_rfs
    ]

    def get_finite_difference_derivative(Jhat, m, h, epsilons):
        with stop_annotating():
            h_orig = Enlist(h)
            J = ReducedFunction(Jhat.functional, Jhat.controls)
            J_np = ReducedFunctionNumPy(J)
            h_orig_np = J_np.obj_to_array(h)
            h_np = onp.zeros_like(h_orig_np)

            all_of_it = []
            for i in range(h_np.size):
                print(f"=============== #{i+1} out of {h_np.size}===============")
                h_np[:] = 0
                h_np[i] = h_orig_np[i]
                h = J_np.get_rf_input(h_np)
                d = taylor_to_dict(Jhat, m, h, Hm=0, epsilons=epsilons)
                pprint(d)
                all_of_it.append(d)
                print(f"=============== all done ===============================")
        return all_of_it

    if args.verify_extra:
        """ TODO: Can I get rid of this? """
        m = tuple(param.val for param in trainable_params)
        h = tuple(param.h for param in trainable_params)
        epsilons = [1.0 / 2 ** i for i in range(12)]
        all_of_it = get_finite_difference_derivative(Jhat, m, h, epsilons=epsilons)

        J = ReducedFunction(Jhat.functional, Jhat.controls)
        J_np = ReducedFunctionNumPy(J)
        h_orig_np = J_np.obj_to_array(h)
        params_np = J_np.obj_to_array(m)

        findiff_orig_np = onp.array([d["estim_dJdm"][-1] for d in all_of_it])
        findiff_np = findiff_orig_np * h_orig_np
        adjoint_np = onp.array([d["dJdm"] for d in all_of_it])

        all_orig_np = onp.array([params_np, findiff_orig_np, adjoint_np]).T
        all_np = onp.array([params_np, findiff_np, adjoint_np]).T
        all_h_orig_np = onp.array([h_orig_np, findiff_orig_np, adjoint_np]).T
        all_h_np = onp.array([h_orig_np, findiff_np, adjoint_np]).T

        div = findiff_np / adjoint_np
        div_orig = findiff_orig_np / adjoint_np

        findiff_flat = J_np.get_rf_input(findiff_np)
        findiff_orig_flat = J_np.get_rf_input(findiff_orig_np)
        adjoint_flat = J_np.get_rf_input(adjoint_np)

        findiff = jax.tree_util.tree_unflatten(network.tree_structure, findiff_flat)
        adjoint = jax.tree_util.tree_unflatten(network.tree_structure, adjoint_flat)

        network_params = jax.tree_map(lambda p: p.val, network.params)
        trees = [network_params, findiff, adjoint]
        big_tree = jax.tree_transpose(
            outer_treedef=jax.tree_structure([0 for t in trees]),
            inner_treedef=network.tree_structure,
            pytree_to_transpose=trees,
        )

        ds = findiff_orig_flat
        dJdm = sum(hi._ad_dot(di) for hi, di in zip(h, ds))

        d3 = taylor_to_dict(Jhat, m, h, dJdm=dJdm, Hm=0, epsilons=epsilons)
        d["dJdm"]

    ############# Optimize ##################
    if args.opt:

        print()
        print("Optimizing...")
        trained_params_val, iter_info = optimize_params(
            Jhat,
            record_file="loss_network_cr.csv",
            bounds=param_bag.bounds,
            display_params=args.opt_params,
            method=args.optimizer,
            disp=args.opt_output,
            options=dict(gtol=args.gtol, ftol=args.ftol),
        )

        J_final = Jhat(trained_params_val)
        J_final_gradient = Jhat.derivative()
        J_final_gradient = ad_norm(J_final_gradient)
        exp_final_losses = [
            tuple(Control(l).tape_value() for l in losses) for losses in exp_losses
        ]
        exp_losses_final_gradients = [
            tuple(rf.derivative() for rf in rfs) for rfs in exp_losses_rfs
        ]
        exp_losses_final_gradients = [
            tuple(ad_norm(g) for g in gs) for gs in exp_losses_final_gradients
        ]
        w_preds_train_final = [Control(w).tape_value() for w in w_preds_train]
        print()
        print("----------------------------")
        print("Trained params")
        print(param_bag.to_string("trainable"))
        print("exp_losses:", exp_final_losses)
        print("exp_losses_gradients:", exp_losses_final_gradients)
        print("Final loss: %g" % J_final)
        print("----------------------------")

        # Set params of CR to the new trained params.
        with stop_annotating():
            param_bag.set_trainable_param_vals(trained_params_val)
            cr_DphiDt.set_params(param_bag.get_vals())
            network.save_params_to_file("params_trained.npz")

        # Write data.
        hyper_params_data = HyperParamsData.init_from_info(
            iter_info,
            param_bag,
            J,
            J_initial_gradient,
            J_final,
            J_final_gradient,
            exp_losses,
            exp_losses_initial_gradients,
            exp_final_losses,
            exp_losses_final_gradients,
        )
        batch_keys = hyper_params.keys + hyper_params_data.keys
        batch_data = dict(**hyper_params.asdict(), **hyper_params_data.asdict())
        pprint(batch_data)
        if args.batch:
            with open("hyper_params_data.txt", "w") as f:
                pprint(hyper_params_data.asdict(), stream=f)

            with open("batch_data.csv", "w") as csv_file:
                writer = csv.DictWriter(csv_file, fieldnames=batch_keys)
                writer.writeheader()
                writer.writerow(batch_data)

        if not args.batch:
            # Run the experiments with the trained params.
            # TODO: Make this more efficient.
            #   - I should just be able to access the tape value, but I'd also like to plot the solutions.
            with push_tape():
                y_preds_train, w_preds_train = run_training(
                    cr, title="Trained", ufl=False, disp=args.debug
                )
                y_preds_test, w_preds_test = run_testing(
                    cr, title="Trained CR", ufl=False, disp=args.debug
                )

            # Plot loss.
            plt.figure()
            plt.semilogy(iter_info[0])
            plt.xlabel("Iteration")
            plt.ylabel(r"L")
            plt.title("Loss during optimization")
            utils.saveplot()

            plt.figure()
            plt.semilogy(iter_info[1])
            plt.xlabel("Iteration")
            plt.ylabel(r"$|\nabla L|$")
            plt.title("Loss gradient during optimization")
            utils.saveplot()

            with stop_annotating():
                cr_plotter.plot_cr_all(
                    scalar_invt_map,
                    inner_map,
                    cr_inputs_ufl,
                    true_data=true_data,
                    true_cb=true_cb,
                    true_cb_xy=true_cb_xy,
                    prefix="Trained CR: ",
                )

                cr_plotter_small.plot_cr_all(
                    scalar_invt_map,
                    inner_map,
                    cr_inputs_ufl,
                    true_data=true_data_small,
                    true_cb=true_cb_small,
                    true_cb_xy=true_cb_xy_small,
                    prefix="Trained CR (small): ",
                )



def run_jax_cr(
    args,
    cr_true,
    run_training,
    run_testing,
    y_trues_train,
    y_trues_test,
    w_trues,
    ex,
    loss,
):
    p_np = array(jnp.array(args.p_init))
    eps2_np = array(jnp.array(args.eps2))
    gamma_f_np = array(jnp.array(args.gamma_f_init))
    esf_np = array(jnp.array(args.esf_init))
    gamma_h_np = array(jnp.array(args.gamma_h_init))
    eh_np = array(jnp.array(args.eh_init))

    all_params = (
        Param(p_np, array(jnp.array(0.0625)), False, "p", (1.3, 2)),
        Param(eps2_np, array(jnp.array(1e-12)), False, "eps2"),
        Param(gamma_f_np, array(jnp.array(1e-1)), True, "gamma_f", (0, onp.inf)),
        Param(esf_np, array(jnp.array(0.0625)), False, "esf", (0, onp.inf)),
        Param(gamma_h_np, array(jnp.array(1e-3)), True, "gamma_h", (0, onp.inf)),
        Param(eh_np, array(jnp.array(0.0625)), False, "eh", (0, onp.inf)),
    )
    param_bag = ParamBag(all_params)

    print()
    print("----------------------------")
    print(param_bag)
    print("----------------------------")

    cr, cr_tau, cr_DphiDt = make_jax_cr(
        p_np, args.dim, eps2_np, gamma_f_np, esf_np, gamma_h_np, eh_np
    )

    y_preds_train, w_preds_train = run_training(
        cr, title="Untrained CR", ufl=False, disp=args.debug
    )
    y_preds_test, w_preds_test = run_testing(
        cr, title="Untrained CR", ufl=False, disp=args.debug
    )

    exp_losses = [
        loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_train, y_preds_train)
    ]
    print("exp_losses:", exp_losses)
    J = sum(sum(losses) for losses in exp_losses)
    print()
    print("   Init loss: %g" % J)
    if onp.isnan(J):
        raise ValueError("Error: loss is NaN")

    p = Constant(args.p_true, name="p_true_invariants")
    eps2 = Constant(args.eps2, name="eps2_invariants")
    gamma_f = Constant(args.gamma_f_true, name="gamma_f_true_invariants")
    esf = Constant(args.esf_true, name="esf_true_invariants")
    gamma_h = Constant(args.gamma_h_true, name="gamma_h_true_invariants")
    eh = Constant(args.eh_true, name="eh_true_invariants")
    cr_ufl = make_ufl_cr(p, args.dim, eps2, gamma_f, esf, gamma_h, eh)

    cr_true_inner_func, cr_true_scalar_invt = make_ufl_invariants_cr(
        p, args.dim, eps2, gamma_f, esf, gamma_h, eh
    )
    cr_inputs_ufl = Experiment.get_cr_inputs(w_trues[0], ())

    if args.test_jac:
        compare_jax_ufl(ex, w_trues[0], cr, cr_ufl)

    Jhat = ReducedFunctional(J, param_bag.controls)
    if args.verify:
        m = param_bag.get_vals("trainable")
        h = tuple(param.h for param in param_bag.trainable_params)
        epsilons = [1.0 / 2 ** i for i in range(12)]
        d = taylor_to_dict(Jhat, m, h, Hm=0, epsilons=epsilons)

        if False:
            from pyadjoint_utils.verification import super_taylor_to_dict

            d = super_taylor_to_dict(Jhat, m, h, Hm=0, epsilons=epsilons)
        pprint(d)

    # Plot correct CR output.
    (
        scalar_invt_map,
        form_invt_map,
        inner_map,
        coeff_form_map,
    ) = cr_DphiDt.get_point_maps()

    invariant_names = [r"tr $\epsilon$", r"tr $\epsilon^2$", r"$\phi$"]
    coeff_names = [r"$\frac{D\phi}{Dt}$"]

    with stop_annotating():
        # Plot untrained CR output in a large strain rate range.
        cr_plotter = InvariantCRPlotter(
            cr_DphiDt.cr_input_shape[0],
            cr_DphiDt.num_scalar_functions,
            w_trues[0].function_space().mesh(),
            quad_params=ex.quad_params,
            invariant_cmap=INVARIANT_CMAP,
            xy_cmap=XY_CMAP,
            invariant_names=invariant_names,
            coeff_names=coeff_names,
            invariant_idxs=(1, 2),
            x_range=(0, 30),
            y_range=(0, 0.1),
            name="large",
        )

        true_cb, true_cb_xy, true_data = cr_plotter.plot_cr_all(
            cr_true_scalar_invt,
            cr_true_inner_func,
            cr_inputs_ufl,
            prefix="Correct CR: ",
            skip_plot=args.batch,
        )

        cr_plotter.plot_cr_all(
            scalar_invt_map,
            inner_map,
            cr_inputs_ufl,
            true_data=true_data,
            true_cb=true_cb,
            true_cb_xy=true_cb_xy,
            prefix="Untrained CR: ",
        )

        # Plot untrained CR output in the low strain rate range.
        cr_plotter_small = InvariantCRPlotter(
            cr_DphiDt.cr_input_shape[0],
            cr_DphiDt.num_scalar_functions,
            w_trues[0].function_space().mesh(),
            quad_params=ex.quad_params,
            invariant_cmap=INVARIANT_CMAP,
            xy_cmap=XY_CMAP,
            invariant_names=invariant_names,
            coeff_names=coeff_names,
            invariant_idxs=(1, 2),
            x_range=(0, args.eh_true + 0.1),
            y_range=(0, 0.1),
            name="small",
        )

        true_cb_small, true_cb_xy_small, true_data_small = cr_plotter_small.plot_cr_all(
            cr_true_scalar_invt,
            cr_true_inner_func,
            cr_inputs_ufl,
            prefix="Correct CR (small): ",
            skip_plot=args.batch,
        )

        cr_plotter_small.plot_cr_all(
            scalar_invt_map,
            inner_map,
            cr_inputs_ufl,
            true_data=true_data_small,
            true_cb=true_cb_small,
            true_cb_xy=true_cb_xy_small,
            prefix="Untrained CR (small): ",
        )

    ############# Optimize ##################
    if args.opt:

        print()
        print("Optimizing...")
        trained_params_val, iter_info = optimize_params(
            Jhat,
            record_file="loss_jax_cr.csv",
            bounds=param_bag.bounds,
            display_params=args.opt_params,
            method=args.optimizer,
            disp=args.opt_output,
            options=dict(gtol=args.gtol, ftol=args.ftol),
        )

        J_final = Jhat(trained_params_val)

        # Set params of CR to the new trained params.
        with stop_annotating():
            param_bag.set_trainable_param_vals(trained_params_val)
            cr_DphiDt.set_params(param_bag.get_vals())
            cr_tau.set_params(param_bag.get_vals())

        print()
        print("----------------------------")
        print("Trained params")
        print(param_bag.to_string("trainable"))
        print(
            "exp_losses:",
            [
                tuple(Control(e).tape_value() for e in exp_loss)
                for exp_loss in exp_losses
            ],
        )
        print("Final loss: %g" % J_final)
        print("----------------------------")

        # Run the experiments with the trained params.
        # TODO: Make this more efficient.
        #   - I should just be able to access the tape value, but I'd also like to plot the solutions.
        with push_tape():
            y_preds_train, w_preds_train = run_training(
                cr, title="Trained", ufl=False, disp=args.debug
            )
            y_preds_test, w_preds_test = run_testing(
                cr, title="Trained CR", ufl=False, disp=args.debug
            )

        if not args.batch:
            # Plot loss.
            plt.figure()
            plt.semilogy(iter_info[0])
            plt.xlabel("Iteration")
            plt.ylabel(r"L")
            plt.title("Loss during optimization")
            utils.saveplot()

            plt.figure()
            plt.semilogy(iter_info[1])
            plt.xlabel("Iteration")
            plt.ylabel(r"$|\nabla L|$")
            plt.title("Loss gradient during optimization")
            utils.saveplot()

        with stop_annotating():
            cr_plotter.plot_cr_all(
                scalar_invt_map,
                inner_map,
                cr_inputs_ufl,
                true_data=true_data,
                true_cb=true_cb,
                true_cb_xy=true_cb_xy,
                prefix="Trained CR: ",
            )

            cr_plotter_small.plot_cr_all(
                scalar_invt_map,
                inner_map,
                cr_inputs_ufl,
                true_data=true_data_small,
                true_cb=true_cb_small,
                true_cb_xy=true_cb_xy_small,
                prefix="Trained CR (small): ",
            )


def run_ufl_cr(
    args,
    cr_true,
    run_training,
    run_testing,
    y_trues_train,
    y_trues_test,
    w_trues,
    ex,
    loss,
):
    p = Constant(args.p_init, name="p")
    eps2 = Constant(args.eps2, name="eps2")
    gamma_f = Constant(args.gamma_f_init, name="gamma_f")
    esf = Constant(args.esf_init, name="esf")
    gamma_h = Constant(args.gamma_h_init, name="gamma_h")
    eh = Constant(args.eh_init, name="eh")

    pos_bounds = (0, onp.inf)
    all_params = (
        ConstantParam(p, Constant(0.0625, name="h_p"), False, (1.3, 2)),
        ConstantParam(eps2, Constant(1e-12, name="h_eps2"), False),
        ConstantParam(gamma_f, Constant(1e-4, name="h_gamma_f"), True, pos_bounds),
        ConstantParam(esf, Constant(0.0625, name="h_esf"), False, pos_bounds),
        ConstantParam(gamma_h, Constant(1e-3, name="h_gamma_h"), True, pos_bounds),
        ConstantParam(eh, Constant(0.0625, name="h_eh"), False, pos_bounds),
    )
    param_bag = ParamBag(all_params)

    print()
    print("----------------------------")
    print(param_bag)
    print("----------------------------")

    cr = make_ufl_cr(p, args.dim, eps2, gamma_f, esf, gamma_h, eh)

    y_preds_train, w_preds_train = run_training(
        cr, title="Untrained", ufl=True, disp=args.debug
    )
    y_preds_test, w_preds_test = run_testing(
        cr, title="Untrained", ufl=True, disp=args.debug
    )

    exp_losses = [
        loss(y_true, y_pred) for y_true, y_pred in zip(y_trues_train, y_preds_train)
    ]
    print("exp_losses:", exp_losses)
    J = sum(sum(losses) for losses in exp_losses)
    print()
    print("   Init loss: %g" % J)
    if onp.isnan(J):
        raise ValueError("Error: loss is NaN")

    Jhat = ReducedFunctional(J, param_bag.controls)
    if args.verify:
        m = param_bag.get_vals("trainable")
        h = tuple(param.h for param in param_bag.trainable_params)
        epsilons = [1.0 / 2 ** i for i in range(12)]
        d = taylor_to_dict(Jhat, m, h, Hm=0, epsilons=epsilons)
        pprint(d)
        if False:
            from pyadjoint_utils.verification import super_taylor_to_dict

            d = super_taylor_to_dict(Jhat, m, h, Hm=0, epsilons=epsilons)
            pprint(d)

    p_true = Constant(args.p_true, name="p_true_invariants")
    eps2_true = Constant(args.eps2, name="eps2_invariants")
    gamma_f_true = Constant(args.gamma_f_true, name="gamma_f_true_invariants")
    esf_true = Constant(args.esf_true, name="esf_true_invariants")
    gamma_h_true = Constant(args.gamma_h_true, name="gamma_h_true_invariants")
    eh_true = Constant(args.eh_true, name="eh_true_invariants")

    cr_true_inner_func, cr_true_scalar_invt = make_ufl_invariants_cr(
        p_true, args.dim, eps2_true, gamma_f_true, esf_true, gamma_h_true, eh_true
    )
    inner_map, scalar_invt_map = make_ufl_invariants_cr(
        p, args.dim, eps2, gamma_f, esf, gamma_h, eh
    )
    cr_inputs_ufl = Experiment.get_cr_inputs(w_trues[0], ())

    # Plot correct CR output.
    invariant_names = [r"tr $\epsilon$", r"tr $\epsilon^2$", r"$\phi$"]
    coeff_names = [r"$\frac{D\phi}{Dt}$"]

    with stop_annotating():
        # Plot untrained CR output in a large strain rate range.
        cr_plotter = InvariantCRPlotter(
            inner_map.source.shape()[-1],
            1,
            w_trues[0].function_space().mesh(),
            quad_params=ex.quad_params,
            invariant_cmap=INVARIANT_CMAP,
            xy_cmap=XY_CMAP,
            invariant_names=invariant_names,
            coeff_names=coeff_names,
            invariant_idxs=(1, 2),
            x_range=(0, 30),
            y_range=(0, 0.1),
            name="large",
        )

        true_cb, true_cb_xy, true_data = cr_plotter.plot_cr_all(
            cr_true_scalar_invt,
            cr_true_inner_func,
            cr_inputs_ufl,
            prefix="Correct CR: ",
            skip_plot=args.batch,
        )

        cr_plotter.plot_cr_all(
            scalar_invt_map,
            inner_map,
            cr_inputs_ufl,
            true_data=true_data,
            true_cb=true_cb,
            true_cb_xy=true_cb_xy,
            prefix="Untrained CR: ",
        )

        # Plot untrained CR output in the low strain rate range.
        cr_plotter_small = InvariantCRPlotter(
            inner_map.source.shape()[-1],
            1,
            w_trues[0].function_space().mesh(),
            quad_params=ex.quad_params,
            invariant_cmap=INVARIANT_CMAP,
            xy_cmap=XY_CMAP,
            invariant_names=invariant_names,
            coeff_names=coeff_names,
            invariant_idxs=(1, 2),
            x_range=(0, args.eh_true + 0.1),
            y_range=(0, 0.1),
            name="small",
        )

        true_cb_small, true_cb_xy_small, true_data_small = cr_plotter_small.plot_cr_all(
            cr_true_scalar_invt,
            cr_true_inner_func,
            cr_inputs_ufl,
            prefix="Correct CR (small): ",
            skip_plot=args.batch,
        )

        cr_plotter_small.plot_cr_all(
            scalar_invt_map,
            inner_map,
            cr_inputs_ufl,
            true_data=true_data_small,
            true_cb=true_cb_small,
            true_cb_xy=true_cb_xy_small,
            prefix="Untrained CR (small): ",
        )

    ############# Optimize ##################
    if args.opt:
        print()
        print("Optimizing...")
        trained_params_val, iter_info = optimize_params(
            Jhat,
            record_file="loss_ufl_cr.csv",
            bounds=param_bag.bounds,
            display_params=args.opt_params,
            method=args.optimizer,
            disp=args.opt_output,
            options=dict(gtol=args.gtol, ftol=args.ftol),
        )

        J_final = Jhat(trained_params_val)

        # Set params of CR to the new trained params.
        with stop_annotating():
            for param, trained_val in zip(
                param_bag.trainable_params, trained_params_val
            ):
                param.val.assign(trained_val)

        print()
        print("----------------------------")
        print("Trained params")
        print(param_bag.to_string("trainable"))
        print(
            "exp_losses:",
            [
                tuple(e.block_variable.saved_output for e in exp_loss)
                for exp_loss in exp_losses
            ],
        )
        print("Final loss: %g" % J_final)
        print("----------------------------")

        with stop_annotating():
            # Run the experiments with the new params.
            y_preds_train, w_preds_train = run_training(
                cr, title="Trained", ufl=True, disp=args.debug
            )
            y_preds_test, w_preds_test = run_testing(
                cr, title="Trained", ufl=True, disp=args.debug
            )

        if not args.batch:
            # Plot loss.
            plt.figure()
            plt.semilogy(iter_info[0])
            plt.xlabel("Iteration")
            plt.ylabel(r"L")
            plt.title("Loss during optimization")
            utils.saveplot()

            plt.figure()
            plt.semilogy(iter_info[1])
            plt.xlabel("Iteration")
            plt.ylabel(r"$|\nabla L|$")
            plt.title("Loss gradient during optimization")
            utils.saveplot()

        with stop_annotating():
            cr_plotter.plot_cr_all(
                scalar_invt_map,
                inner_map,
                cr_inputs_ufl,
                true_data=true_data,
                true_cb=true_cb,
                true_cb_xy=true_cb_xy,
                prefix="Trained CR: ",
            )

            cr_plotter_small.plot_cr_all(
                scalar_invt_map,
                inner_map,
                cr_inputs_ufl,
                true_data=true_data_small,
                true_cb=true_cb_small,
                true_cb_xy=true_cb_xy_small,
                prefix="Trained CR (small): ",
            )


def optimize_params(Jhat, display_params=False, method="L-BFGS-B", **kwargs):
    record_file = kwargs.pop("record_file", "data.csv")
    old_callback = kwargs.pop("callback", None)

    # Display minimize() info by default.
    options = kwargs.pop("options", {})
    options["disp"] = kwargs.pop("disp", options.get("disp", True))

    losses = []
    mag_grads = []
    if record_file:
        # Open up the data file and write the column headers.
        with open(record_file, "w") as f:
            f.write("loss,grad_loss\n")

    # Define a function that appends to this data file.
    def minimize_callback(xk, state=None):
        # Record the loss and loss gradient.
        loss = ReducedFunctionalNumPy(Jhat)(xk)
        grad_loss = Jhat.derivative()
        grad_loss = sum(onp.sqrt(coo(g)._ad_dot(g)) for g in grad_loss)

        losses.append(loss)
        mag_grads.append(grad_loss)
        if record_file:
            with open(record_file, "a") as f:
                f.write("%g,%g\n" % (loss, grad_loss))
        if old_callback is not None:
            return old_callback(xk, state=state)

    if display_params:
        orig_cb = Jhat.eval_cb_pre
        orig_der_cb = Jhat.derivative_cb_post

        def derivative_cb_post(output, derivatives, values):
            if orig_der_cb:
                orig_der_cb(output, derivatives, values)
            print("Derivatives are ")
            for i, param in enumerate(derivatives):
                values = param.values() if hasattr(param, "values") else param
                print(f"{i}: {values}")

        def eval_cb_pre(p):
            if orig_cb:
                orig_cb(p)
            print("Evaluating at ")
            for i, param in enumerate(p):
                values = param.values() if hasattr(param, "values") else param
                print(f"{i}: {values}")

        Jhat.eval_cb_pre = eval_cb_pre
        Jhat.derivative_cb_post = derivative_cb_post
    if method == "trust-constr":
        del options["ftol"]
        # options['initial_tr_radius'] = 1
        options["xtol"] = 1e-10
        options["verbose"] = 3
    elif method == "L-BFGS-B":
        options["maxcor"] = 200
    options["maxiter"] = 1000
    params = minimize(
        Jhat, method=method, options=options, callback=minimize_callback, **kwargs
    )

    if display_params:
        Jhat.eval_cb_pre = orig_cb
        Jhat.derivative_cb_post = orig_der_cb

    # Pyadjoint bug: minimize doesn't respect the enlistment of Jhat's controls.
    params = Jhat.controls.delist(Enlist(params))
    iter_info = [losses, mag_grads]
    return params, iter_info


def build_experiment_and_mesh(hyper_params):
    hp = hyper_params
    nx, ny = hp.grid, hp.grid
    mesh, top_boundary, right_boundary = build_mesh(hp.L, hp.H, nx, ny, h=hp.l)

    extras = (FiniteElement("CG", mesh.ufl_cell(), 1),)
    xi = Constant(hp.xi, name="xi")
    ex = Experiment(mesh, extras=extras, degree=2, xi=xi)
    ex.set_quad_params(hp.quad_params)
    return ex, mesh, top_boundary, right_boundary


def build_training_testing_functions(
    ex, observer, phi_init, top_boundary, right_boundary
):
    # Set up boundary conditions.
    if ex.dim != 2:
        raise ValueError("Must use 2D")

    def left_boundary(x, on_boundary):
        return on_boundary and near(x[0], 0, 1e-5)

    def bottom_boundary(x, on_boundary):
        return on_boundary and near(x[1], 0, 1e-5)

    u_noslip = Expression(("0",) * ex.dim, degree=0)
    u_nohorizontal = Expression("0", degree=0)
    phi_top0 = Expression("0", degree=0)

    ex.set_f(g=9.8, rho=1)
    noslip = DirichletBC(ex.W.sub(0), u_noslip, bottom_boundary)
    nohorizontal = DirichletBC(ex.W.sub(0).sub(0), u_nohorizontal, left_boundary)
    phi0 = DirichletBC(ex.W.sub(2), phi_top0, top_boundary)

    dir_bcs = [noslip, nohorizontal, phi0]

    class RightBoundaries(SubDomain):
        def inside(self, x, on_boundary):
            return right_boundary(x, on_boundary)

    markers = MeshFunction("size_t", ex.mesh, ex.mesh.topology().dim() - 1, 0)
    RightBoundaries().mark(markers, 1)
    dright = ds(1, domain=ex.mesh, subdomain_data=markers)

    # FIXME: the robin conditions are not part of the cache key, so if these values are modified, the cache must be cleared.
    u_outflow_robin = False
    if u_outflow_robin:
        a = Constant(1)
        b = Constant(1e-1)
        j = u_noslip
        u_rob_bcs = [(a, b, j, dright)]
    else:
        u_rob_bcs = []

    # FIXME: the robin conditions are not part of the cache key, so if these values are modified, the cache must be cleared.
    E_outflow_robin = False
    if E_outflow_robin:
        a = Constant(1)
        b = Constant(1e-1)
        j = Constant(0)
        E_rob_bcs = [(a, b, j, dright)]
    else:
        E_rob_bcs = []

    ex.set_bcs(dir_bcs, u_rob_bcs, E_rob_bcs=E_rob_bcs)

    initial_w = Function(ex.W)
    initials = initial_w.split()
    spaces = [f.function_space().collapse() for f in initials]

    initial_u = Function(spaces[0])
    initial_p = Function(spaces[1])
    initial_phi = project(Constant(phi_init), spaces[2])

    fa = FunctionAssigner(ex.W, spaces)
    fa.assign(initial_w, [initial_u, initial_p, initial_phi])

    def run_training_experiments(
        cr,
        title="",
        cache=None,
        cr_cache_key=None,
        refresh_cache=False,
        observer=observer,
        do_plots=True,
        **ex_kwargs,
    ):
        ys = []

        if cache is None or refresh_cache:
            w = None
        else:
            w = ex.get_w_from_cache(cache, cr_cache_key, **ex_kwargs)

        if do_plots:

            def adj_cb(adj_sol):
                if isinstance(adj_sol, (list, tuple)):
                    adj_sol = adj_sol[0]
                plot_solution(adj_sol, f"{title} (train) (adj)")

        else:
            adj_cb = None

        if w is None:
            print(f"Running experiment")
            w = ex.run(
                cr, initial_w=initial_w.copy(deepcopy=True), adj_cb=adj_cb, **ex_kwargs
            )

            if cache is not None:
                ex.write_w_to_cache(cache, cr_cache_key, w, **ex_kwargs)

        if do_plots:
            name = f"{title} (train)"
            plot_solution(w, title=name)

        ws = [w.copy(deepcopy=True)]
        if observer is None:
            return ws
        ys = [observer(w) if observer is not None else w for w in ws]
        return ys, ws

    def run_test_experiments(*args, observer=observer, **kwargs):
        if observer is None:
            return None
        return None, None

    return run_training_experiments, run_test_experiments


def plot_solution(w, title="", extra_names=(r"$\phi$",), combined=True):
    if BATCH:
        return

    u, p, *extras = w.split()

    plt.figure()
    f = plot(p)
    plt.colorbar(f)
    if not combined:
        plt.title(title)
        utils.saveplot(f"{title}_pressure")

        plt.figure()
    f = plot(u, cmap=EX_CMAP)
    plt.colorbar(f)
    plt.title(title)
    utils.saveplot()

    if len(extra_names) < len(extras):
        extra_names = list(extra_names) + [
            f"(Extra {i})" for i in range(len(extra_names), len(extras))
        ]

    for i, (e, name) in enumerate(zip(extras, extra_names)):
        plt.figure()
        f = plot(e)
        plt.colorbar(f)
        plt.title(f"{title} {name}")
        utils.saveplot(f"{title}_e{i}")


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    utils.add_bool_arg(parser, "debug", False, "Print extra output when solving PDE.")
    utils.add_bool_arg(
        parser, "viz_tape", False, "Call Tape.visualise() after everything is run."
    )
    utils.add_bool_arg(
        parser,
        "show",
        False,
        "Show interactive matplotlib plots. Be careful, there are a lot of them.",
    )

    parser.add_argument(
        "--save_dir",
        type=str,
        help="Directory for saving figures.",
        default="figs",
    )

    parser.add_argument(
        "--format",
        type=str,
        help="Image format to use for saving figures.",
        default="png",
    )

    parser.add_argument("--seed", type=int, help="Seed for RNG.", default=748)
    parser.add_argument(
        "-p",
        "--p_true",
        type=float,
        help="Value to use for ground-truth `p`.",
        default=1.5,
    )
    parser.add_argument(
        "--p_init",
        type=float,
        help="Value to use for initial `p` when using a non-network candidate CR.",
        default=1.5,
    )

    parser.add_argument(
        "--gamma_f_true",
        type=float,
        help="Value to use for ground-truth `gamma_f`.",
        default=0.05,
    )
    parser.add_argument(
        "--gamma_f_init",
        type=float,
        help="Value to use for initial `gamma_f` when using a non-network candidate CR.",
        default=0.05,
    )

    parser.add_argument(
        "--esf_true",
        type=float,
        help="Value to use for ground-truth `esf`.",
        default=2.0,
    )
    parser.add_argument(
        "--esf_init",
        type=float,
        help="Value to use for initial `esf` when using a non-network candidate CR.",
        default=2.0,
    )
    parser.add_argument(
        "--gamma_h_true",
        type=float,
        help="Value to use for ground-truth `gamma_h`.",
        default=0.01,
    )
    parser.add_argument(
        "--gamma_h_init",
        type=float,
        help="Value to use for initial `gamma_h` when using a non-network candidate CR.",
        default=0.01,
    )

    parser.add_argument(
        "--eh_true",
        type=float,
        help="Value to use for ground-truth `eh`.",
        default=1.0,
    )
    parser.add_argument(
        "--eh_init",
        type=float,
        help="Value to use for initial `eh` when using a non-network candidate CR.",
        default=1.0,
    )

    parser.add_argument(
        "--phi_init",
        type=float,
        help="Value to use for initial guess for `phi`.",
        default=0.0,
    )
    parser.add_argument(
        "-o",
        "--observer",
        type=str,
        choices=["full", "velocity", "pressure"],
        help="Observer function to use for loss. There are three options. - `full`: The velocity and pressure fields observed. - `velocity`: The velocity field is observed. - `pressure`: The pressure field is observed.",
        default="full",
    )
    parser.add_argument(
        "--noise",
        type=float,
        help="Standard deviation of noise to add to observations",
        default=None,
        nargs="+",
    )
    parser.add_argument("--noise_seed", type=int, help="Seed for noise.", default=None)

    utils.add_bool_arg(parser, "surface", False, "Observe only on surface.")
    utils.add_bool_arg(
        parser, "borehole", False, "Collect measurements along borehole."
    )

    parser.add_argument(
        "--borehole_observer",
        type=str,
        choices=["full", "velocity", "pressure"],
        help="Observer function to use for borehole loss. See --observer parameter.",
        default="full",
    )
    parser.add_argument(
        "--borehole_noise",
        type=float,
        help="Standard deviation of noise to add to borehole observations",
        default=None,
        nargs="+",
    )

    parser.add_argument(
        "--weights_file",
        type=str,
        help="This should be an npz file to load network weights from.",
        default=None,
    )

    parser.add_argument(
        "-d",
        "--dim",
        type=int,
        choices=[2],
        help="Number of spatial dimensions.",
        default=2,
    )
    parser.add_argument(
        "-g",
        "--grid",
        type=int,
        help="Grid size per dimension. In 2D, the grid is `g x g` and in 3D, `g x g x g`.",
        default=16,
    )
    parser.add_argument(
        "--fine_grid",
        type=int,
        help="Grid size per dimension for ground-truth data.",
        default=32,
    )
    parser.add_argument(
        "-H",
        "--H",
        type=float,
        help="Domain rectangular height.",
        default=1,
    )
    parser.add_argument(
        "-L",
        "--L",
        type=float,
        help="Domain rectangular length.",
        default=2,
    )
    parser.add_argument(
        "-l",
        "--l",
        type=float,
        help="Domain extra height for curved surface.",
        default=0.5,
    )
    parser.add_argument(
        "--epsilon",
        type=float,
        help="Regularization value used to avoid singularities in power law.",
        default=1e-5,
    )
    parser.add_argument(
        "--xi",
        type=float,
        help="Diffusivity factor for damage PDE.",
        default=5e-1,
    )
    parser.add_argument(
        "--quad",
        type=int,
        help="Quadrature degree used to assemble PDE weak form.",
        default=6,
    )

    parser.add_argument(
        "--gtol",
        type=float,
        help="Optimization stop condition for gradient.",
        default=1e-13,
    )
    parser.add_argument(
        "--ftol",
        type=float,
        help="Optimization stop condition for decrease in residual.",
        default=1e-30,
    )

    parser.add_argument(
        "--invariant_cmap",
        type=str,
        help="Matplotlib color map for plotting the CR output as a function of the invariants.",
        default="jet",
    )

    parser.add_argument(
        "--ex_cmap",
        type=str,
        help="Matplotlib color map for plotting the PDE solution in physical space (as a function of the mesh coordinates).",
        default="magma",
    )

    parser.add_argument(
        "--xy_cmap",
        type=str,
        help="Matplotlib color map for plotting the CR output in physical space.",
        default="spring",
    )

    utils.add_bool_arg(
        parser,
        "verify",
        True,
        "Skip Taylor test verification, which can save some time if you already know the gradient is correct.",
    )

    utils.add_bool_arg(
        parser,
        "verify_extra",
        False,
        "Run extra Taylor test verification.",
    )

    utils.add_bool_arg(
        parser,
        "test_jac",
        False,
        "Run Jacobian residual tests.",
    )

    utils.add_bool_arg(
        parser, "opt", False, "Run the optimization for the CR parameters."
    )
    utils.add_bool_arg(
        parser, "opt_output", True, "Hide optimization output from scipy.minimize"
    )
    utils.add_bool_arg(
        parser,
        "opt_params",
        False,
        "Print CR parameters at each optimization iteration.",
    )

    optimizer = ["L-BFGS-B", "trust-constr"]
    parser.add_argument(
        "--optimizer",
        choices=optimizer,
        help="Optimization algorithm.",
        default="trust-constr",
    )

    parser.add_argument(
        "--layer_sizes",
        type=int,
        help="Network hidden layer sizes, separated by spaces.",
        default=[5],
        nargs="+",
    )

    activations = ["softplus", "relu", "tanh"]
    parser.add_argument(
        "--activation",
        choices=activations,
        type=str,
        help="Network activation function.",
        default="softplus",
    )

    modes = ["ufl", "jax", "network"]
    true_cr_modes = ["ufl"]
    parser.add_argument(
        "mode",
        choices=modes,
        help="Which implementation to use for the candidate CR.",
        nargs="?",
        default=None,
    )
    parser.add_argument(
        "--true_cr",
        choices=true_cr_modes,
        default="ufl",
        help="Which implementation to use for the true CR.",
    )

    utils.add_bool_arg(
        parser,
        "refresh_cache",
        False,
        "Force a cache miss (i.e., make the ground-truth experiment run even if it is in the cache).",
    )

    utils.add_bool_arg(
        parser,
        "bootstrap",
        False,
        "For JAX network, do initial training on direct CR input-output pairs before doing training on observations.",
    )

    utils.add_bool_arg(
        parser,
        "batch",
        False,
        "Special settings for batch runs: no Taylor verification, no CR plots, no loss plots, data is written to a CSV file.",
    )

    args = parser.parse_args()

    args.eps2 = args.epsilon ** 2
    if args.batch:
        # Modify parameters for batch settings.
        args.show = False
        args.verify = False

        # I apologize for this global variable duct tape.
        global BATCH
        BATCH = True

    return args


def add_noise_numpy(rng, arr, mag, mult):
    if mult:
        return arr * (1 + mag * rng.normal(0, size=arr.shape))

    return arr + mag * rng.normal(0, size=arr.shape)


def add_noise_ufl_quadrature(rng, f, mag, quad_params, mult=False):
    Q = make_quadrature_space(f.ufl_shape, quad_params, expr=f)
    f_quad = project(f, Q)
    f_qvec = f_quad.vector()
    f_qvec.set_local(add_noise_numpy(rng, f_qvec.get_local(), mag, mult))
    return f_quad


def make_tensor_space(shape, family, degree, expr=None, domain=None):
    domain = domain if domain is not None else expr.ufl_domain()
    if domain is None:
        raise ValueError(
            "You must specify the domain (or an expression on the domain) to create a tensor space"
        )
    cell = domain.ufl_cell()
    mesh = mesh_from_ufl_domain(domain)

    if shape:
        TE = TensorElement(family, cell, degree=degree, shape=shape)
    else:
        TE = FiniteElement(family, cell, degree=degree)
    TV = FunctionSpace(mesh, TE)
    return TV


def add_noise_ufl(rng, f, mag, family="P", degree=1, mult=False):
    V = make_tensor_space(f.ufl_shape, family, degree, expr=f)
    noise = Function(V)
    vec = noise.vector()
    vec.set_local(add_noise_numpy(rng, vec.get_local(), mag, mult))
    return f + noise


def get_ground_truth_noise_function(hyper_params):
    hp = hyper_params

    def add_velocity_noise(u, mag):
        if mag is None or len(mag) == 0:
            return u
        if hp.surface:
            u_surf = u
            if u_surf.de is volume:
                u_full = add_noise_ufl_quadrature(
                    rng, u_surf.u, mag[0], hp.quad_params, mult=True
                )
                u_surf = SurfaceObserver.SurfaceObservation(u_full, u_surf.de)
                return u_surf
            u_full = add_noise_ufl(rng, u_surf.u, mag[0], mult=True)
            u_surf = SurfaceObserver.SurfaceObservation(u_full, u_surf.de)
            return u_surf
        u = add_noise_ufl_quadrature(rng, u, mag[0], hp.quad_params, mult=True)
        return u

    def add_pressure_noise(p, mag):
        if mag is None or len(mag) == 0:
            return p
        if hp.surface:
            p_surf = p
            if p_surf.de is volume:
                u_full = add_noise_ufl_quadrature(
                    rng, p_surf.u, mag[1], hp.quad_params, mult=False
                )
                p_surf = SurfaceObserver.SurfaceObservation(u_full, p_surf.de)
                return p_surf
            p = add_noise_ufl(rng, p_surf.u, mag[1], mult=False)
            p_surf = SurfaceObserver.SurfaceObservation(p, p_surf.de)
            return p_surf
        p = add_noise_ufl_quadrature(rng, p, mag[1], hp.quad_params, mult=False)
        return p

    def add_noise(y_trues, observer, noise):
        noisy_outputs = []
        if observer == "full":
            u, p, *rest_y_trues = y_trues
            u = add_velocity_noise(u, noise)
            p = add_pressure_noise(p, noise)
            noisy_outputs.append(u)
            noisy_outputs.append(p)
        elif observer == "velocity":
            u, *rest_y_trues = y_trues
            u = add_velocity_noise(u, noise)
            noisy_outputs.append(u)
        elif observer == "pressure":
            p, *rest_y_trues = y_trues
            p = add_pressure_noise(p, noise)
            noisy_outputs.append(p)
        else:
            print("Internal error: invalid observer  '%s'" % observer)
            sys.exit(1)
        return noisy_outputs, rest_y_trues

    def get_noisy_results(y_trues):
        rng = onp.random.default_rng(hp.noise_seed)
        noisy_outputs = []

        # Add noise to main measurements, which are velocity and/or pressure (over surface or full domain).
        noisy_outputs, rest_y_trues = add_noise(y_trues, hp.observer, hp.noise)

        # Add noise to borehole measurements.
        if hp.borehole:
            borehole_noisy_outputs, rest_y_trues = add_noise(
                rest_y_trues, hp.borehole_observer, hp.borehole_noise
            )
            noisy_outputs += borehole_noisy_outputs

        return tuple(noisy_outputs)

    return get_noisy_results


def get_ground_truth_data(
    hyper_params,
    ex,
    observer=None,
    debug=False,
    refresh_cache=False,
    cache=None,
    do_plots=False,
):
    with stop_annotating():
        # Set up CR.
        using_ufl_cr = True
        p_true = Constant(hyper_params.p_true, name="p_true")
        eps2 = Constant(hyper_params.eps2, name="eps2")
        gamma_f = Constant(hyper_params.gamma_f_true, name="gamma_f_true")
        esf = Constant(hyper_params.esf_true, name="esf_true")
        gamma_h = Constant(hyper_params.gamma_h_true, name="gamma_h_true")
        eh = Constant(hyper_params.eh_true, name="eh_true")
        cr_true = make_ufl_cr(p_true, hyper_params.dim, eps2, gamma_f, esf, gamma_h, eh)

        old_mesh = ex.mesh
        old_extras = ex.extras

        n = hyper_params.fine_grid
        mesh, top_boundary, right_boundary = build_mesh(
            hyper_params.L, hyper_params.H, n, n, h=hyper_params.l
        )
        if do_plots:
            plt.figure()
            plot(mesh)
            plt.title("Fine mesh")

        extras = [e.reconstruct(cell=mesh.ufl_cell()) for e in Enlist(old_extras)]
        ex.set_domain_parameters(
            mesh,
            degree=ex.degree,
            constrained_domain=ex.constrained_domain,
            extras=extras,
        )

        run_training, run_testing = build_training_testing_functions(
            ex, None, hyper_params.phi_init, top_boundary, right_boundary
        )

        # Run on finer mesh.
        cr_params = (p_true, eps2, gamma_f, esf, gamma_h, eh)
        cr_cache_key = tuple(tuple(p.values()) for p in cr_params)
        w_trues_train_fine = run_training(
            cr_true,
            title="Ground-truth",
            ufl=using_ufl_cr,
            cache=cache,
            cr_cache_key=cr_cache_key,
            refresh_cache=refresh_cache,
            disp=debug,
            do_plots=do_plots,
        )
        w_trues_test_fine = run_testing(
            cr_true,
            title="Ground-truth",
            ufl=using_ufl_cr,
            cache=cache,
            cr_cache_key=cr_cache_key,
            refresh_cache=refresh_cache,
            disp=debug,
            do_plots=do_plots,
        )

        if do_plots:
            # Plot DphiDt.
            for w in w_trues_train_fine:
                tau, DphiDt = cr_true(Experiment.get_cr_inputs(w, ()))
                plt.figure()
                DphiDt_projected = project(DphiDt, FunctionSpace(ex.mesh, ex.extras[0]))
                f = plot(DphiDt_projected)
                plt.colorbar(f)
                plt.title(r"Ground-truth $\frac{D\phi}{Dt}$ (train, fine)")

        # Project to coarser mesh.
        ex.set_domain_parameters(
            old_mesh,
            degree=ex.degree,
            constrained_domain=ex.constrained_domain,
            extras=old_extras,
        )
        for w in w_trues_train_fine:
            w.set_allow_extrapolation(True)
        w_trues_train = [project(w, ex.W) for w in w_trues_train_fine]

        noise_handler = get_ground_truth_noise_function(hyper_params)
        if observer is not None:
            y_trues_train = [observer(w) for w in w_trues_train]
            y_trues_train = [noise_handler(y) for y in y_trues_train]

        if w_trues_test_fine is not None and w_trues_test_fine != (None, None):
            for w in w_trues_test_fine:
                w.set_allow_extrapolation(True)
            w_trues_test = [project(w, ex.W) for w in w_trues_test_fine]
            if observer is not None:
                y_trues_test = [observer(w) for w in w_trues_test]
                y_trues_test = [noise_handler(y) for y in y_trues_test]
        else:
            y_trues_test = None

        if do_plots:
            # Plot coarse DphiDt.
            for w in w_trues_train:
                name = f"Ground-truth (train, coarse)"
                plot_solution(w, title=name)

                tau, DphiDt = cr_true(Experiment.get_cr_inputs(w, ()))
                plt.figure()
                DphiDt_projected = project(DphiDt, FunctionSpace(ex.mesh, ex.extras[0]))
                f = plot(DphiDt_projected)
                plt.colorbar(f)
                plt.title(r"Ground-truth $\frac{D\phi}{Dt}$ (train, coarse)")

    if observer is None:
        return cr_true, w_trues_train

    return cr_true, y_trues_train, y_trues_test, w_trues_train


def compare_jax_ufl(ex, w, cr_jax, cr_ufl):
    cr_inputs_ufl = Experiment.get_cr_inputs(w, ())

    data_file = "err_data.csv"
    with open(data_file, "w") as f:
        f.write(
            f' q, {"residual error":22s}, {"jacobian error":22s}, {"<outputs error>":22s}\n'
        )

    for q in range(0, 10):
        outputs_err, residual_err, mat_err = test_quadrature(
            ex, cr_ufl, cr_jax, cr_inputs_ufl, w, q
        )
        with open(data_file, "a") as f:
            f.write(f"{q:2d}, {residual_err: 22.15g}, {mat_err: 22.15g}")
            for err in outputs_err:
                f.write(f", {err: 22.15g}")
            f.write("\n")


def compare_funcs(funcs_ufl, funcs_np):
    funcs_ufl = Enlist(funcs_ufl)
    funcs_np = Enlist(funcs_np)
    all_err = []
    for i, (func_ufl, func_np) in enumerate(zip(funcs_ufl, funcs_np)):
        diff = func_ufl.vector()[:] - func_np.vector()[:]
        err = onp.sqrt((diff * diff).mean())
        print(f"     Func {i} vector diff:", err)
        all_err.append(err)
    return funcs_ufl.delist(all_err)


def compare_mats(mat_ufl, mat_np):
    diff = mat_ufl.array() - mat_np.array()
    err = onp.sqrt((diff * diff).mean())
    print("       Mat average diff:", err)
    return err


def test_quadrature(ex, cr_ufl, cr_jax, inputs, w, quad_degree):
    print("Quadrature degree:", quad_degree)
    quad_params = {"quadrature_degree": quad_degree}

    from ufl import replace

    with push_tape():
        domain = ex.mesh.ufl_domain()
        quad_spaces, _ = make_quadrature_spaces(
            [t for t in cr_ufl.target],
            domain=domain,
            quad_params=quad_params,
        )
        if len(quad_spaces) > 1:
            out_space = DirectSum([UFLFunctionSpace(s) for s in quad_spaces])
        else:
            out_space = UFLFunctionSpace(quad_spaces[0])

        cr_jax_composite = get_composite_cr(
            cr_ufl.source, cr_jax, out_space, quad_params=quad_params
        )
        cr_ufl_composite = get_composite_cr(cr_ufl, out_space, quad_params=quad_params)

        outputs_ufl = cr_ufl_composite(inputs)
        outputs_np = cr_jax_composite(inputs)

        print("Comparing sigma:")
        outputs_err = compare_funcs(outputs_ufl, outputs_np)

        # Now test assemble_with_cr()
        residual_np = Function(ex.W)
        residual_ufl = Function(ex.W)
        F, *cr_outputs = ex.get_form_cr(cr_jax, w)
        assemble_with_cr(
            F, cr_jax, inputs, cr_outputs, tensor=residual_np, quad_params=quad_params
        )
        assemble_with_cr(
            F, cr_ufl, inputs, cr_outputs, tensor=residual_ufl, quad_params=quad_params
        )

        print("Comparing residual:")
        residual_err = compare_funcs(residual_ufl, residual_np)

        # Test Jacobian matrix.
        rf = ReducedFunction(residual_np, Control(w))
        rf_np = ReducedFunctionNumPy(rf)
        mat_np = rf_np.jac_matrix()

        F_replaced = replace(
            F,
            {
                out_tmp: out_real
                for out_tmp, out_real in zip(Enlist(cr_outputs), Enlist(cr_ufl(inputs)))
            },
        )
        mat_ufl = assemble(derivative(F_replaced, w))

        mat_err = compare_mats(mat_np, mat_ufl)
        print()
    return outputs_err, residual_err, mat_err


def build_observer_and_loss(hyper_params, mesh, top_boundary, right_boundary):
    loss_weights = (AdjFloat(1), AdjFloat(1))

    def loss(y_true, y_pred):
        y_true = Enlist(y_true)
        y_pred = Enlist(y_pred)
        l = tuple(
            w * integral_loss(t, p) for w, t, p in zip(loss_weights, y_true, y_pred)
        )
        return l

    if hyper_params.observer == "full":
        # Remove damage from observations.
        function_observer = lambda w: split(w)[:-1]
    elif hyper_params.observer == "velocity":
        # Remove damage and pressure from observations.
        function_observer = lambda w: split(w)[:-2]
        loss_weights = loss_weights[:1]
    elif hyper_params.observer == "pressure":
        # Remove damage and velocity from observations.
        function_observer = lambda w: split(w)[1:-1]
        loss_weights = loss_weights[1:]
    else:
        print("Internal error: invalid observer  '%s'" % hyper_params.observer)
        sys.exit(1)

    if hyper_params.surface:

        class TopBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return top_boundary(x, on_boundary)

        markers = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
        TopBoundary().mark(markers, 1)
        de = ds(1, domain=mesh, subdomain_data=markers)

        # Our obs are taken only on the top boundary.
        surface_observer = SurfaceObserver(de)
    else:
        surface_observer = None

    if surface_observer is None:
        observer = function_observer
    else:

        def observer(w):
            return tuple(surface_observer(u) for u in function_observer(w))

    if hyper_params.borehole:
        bx = 0.6
        borehole_loss_weights = (AdjFloat(6e1), AdjFloat(3e2))

        if hyper_params.borehole_observer == "full":
            # Remove damage from observations.
            borehole_function_observer = lambda w: split(w)[:-1]
        elif hyper_params.borehole_observer == "velocity":
            # Remove damage and pressure from observations.
            borehole_function_observer = lambda w: split(w)[:-2]
            borehole_loss_weights = borehole_loss_weights[:1]
        elif hyper_params.borehole_observer == "pressure":
            # Remove damage and velocity from observations.
            borehole_function_observer = lambda w: split(w)[1:-1]
            borehole_loss_weights = borehole_loss_weights[1:]
        else:
            print(
                "Internal error: invalid borehole observer  '%s'"
                % hyper_params.borehole_observer
            )
            sys.exit(1)

        class BoreholeDomain(SubDomain):
            def inside(self, x, on_boundary):
                return near(x[0], bx, 0.05)

        loss_weights += borehole_loss_weights

        markers = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
        BoreholeDomain().mark(markers, 1)
        db = dx(1, domain=mesh, subdomain_data=markers)

        borehole_db_observer = SurfaceObserver(db)  # This should be called on u.

        def borehole_observer(w):
            borehole_funcs = borehole_function_observer(w)
            obs = tuple(borehole_db_observer(u) for u in borehole_funcs)
            return obs

        # Add new borehole observations to observer.
        old_observer = observer

        def observer(w):
            obs = old_observer(w)
            borehole_obs = borehole_observer(w)
            return tuple(obs) + tuple(borehole_obs)

    return observer, loss


BATCH = False


def main():
    args = parse_args()

    hyper_params = HyperParams.init_from_args(args)
    print(hyper_params)

    if args.batch:
        with open("hyper_params.txt", "w") as f:
            pprint(hyper_params.asdict(), stream=f)

    onp.random.seed(args.seed)
    if not args.debug:
        set_log_level(logging.CRITICAL)

    if args.save_dir != "":
        utils.FIGURE_SAVE_DIR = args.save_dir
        try:
            plt.rc("savefig", format=args.format)
        except Exception as e:
            print("Warning: couldn't set output image format:", e)
    utils.SHOW_FIGURES = args.show

    try:
        from matplotlib import cm

        global EX_CMAP
        EX_CMAP = cm.get_cmap(args.ex_cmap)

        global INVARIANT_CMAP
        INVARIANT_CMAP = cm.get_cmap(args.invariant_cmap)

        global XY_CMAP
        XY_CMAP = cm.get_cmap(args.xy_cmap)

    except Exception as e:
        print("Warning: couldn't get matplotlib colormap:", e)

    ex, mesh, top_boundary, right_boundary = build_experiment_and_mesh(hyper_params)
    if not args.batch:
        plt.figure()
        plot(mesh)
        plt.title("Coarse mesh")
    print("Using quadrature degree", ex.quad_degree)

    print("      True p: %g" % args.p_true)

    set_default_covering_params(domain=mesh.ufl_domain(), quad_params=ex.quad_params)
    V_quad = make_quadrature_spaces(
        [UFLFunctionSpace(ex.W.sub(0).collapse())],
        domain=mesh.ufl_domain(),
        quad_params=ex.quad_params,
    )[0][0]
    print("Quadrature DOF for velocity:", V_quad.dim())
    print()

    # Set up observer and loss.
    observer, loss = build_observer_and_loss(
        hyper_params, mesh, top_boundary, right_boundary
    )

    # Run experiment to get ground-truth observations.
    if args.batch:
        cache = None
    else:
        cache = PickleDictionaryCache("cache/db.pickle")

    cr_true, y_trues_train, y_trues_test, w_true = get_ground_truth_data(
        hyper_params,
        ex,
        observer,
        debug=args.debug,
        refresh_cache=args.refresh_cache,
        cache=cache,
        do_plots=not args.batch,
    )

    if args.mode is None:
        if args.show:
            plt.show()
        return

    run_training, run_testing = build_training_testing_functions(
        ex, observer, args.phi_init, top_boundary, right_boundary
    )

    if args.mode == "jax":
        run_jax_cr(
            args,
            cr_true,
            run_training,
            run_testing,
            y_trues_train,
            y_trues_test,
            w_true,
            ex,
            loss,
        )
    elif args.mode == "ufl":
        run_ufl_cr(
            args,
            cr_true,
            run_training,
            run_testing,
            y_trues_train,
            y_trues_test,
            w_true,
            ex,
            loss,
        )
    elif args.mode == "network":
        run_network_cr(
            args,
            hyper_params,
            cr_true,
            run_training,
            run_testing,
            y_trues_train,
            y_trues_test,
            w_true,
            ex,
            loss,
        )
    else:
        print("Internal error: invalid mode  '%s'" % args.mode)
        sys.exit(1)

    if args.viz_tape:
        get_working_tape().visualise(launch_tensorboard=True, open_in_browser=True)

    if args.show:
        plt.show()


if __name__ == "__main__":
    main()

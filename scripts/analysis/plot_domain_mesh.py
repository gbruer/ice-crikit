import tensorflow

import matplotlib.pyplot as plt
import numpy as np
from fenics import plot
from mesh import stretch_mesh, build_mesh
from plotting import plot_translucent_point_group

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)

MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"
GREY = "#CCCCCC"
GOLD = "gold"
AQUA = "aqua"
VIOLET = "violet"

point_kwargs_all = dict(facecolor=WHITE)
point_kwargs_left = dict(facecolor=MOSS_GREEN)
point_kwargs_right = dict(facecolor=VIOLET)
point_kwargs_right_large = dict(facecolor=AQUA)
point_kwargs_borehole = dict(facecolor=GOLD)
point_kwargs_surface = dict(facecolor=MINT_BLUE)


def label_corners(L, H, h, color="gray"):
    ax = plt.gca()
    # ax.annotate(
    #     r'$(0, 0)$',
    #     xy=(0, 0),
    #     xycoords='data',
    #     horizontalalignment='right',
    #     verticalalignment='top',
    #     color=color,
    # )

    ax.annotate(
        r"$(0, H + h)$",
        xy=(0, H + h),
        xycoords="data",
        horizontalalignment="left",
        verticalalignment="bottom",
        color=color,
    )

    ax.annotate(
        r"$(L, H)$",
        xy=(L, H),
        xycoords="data",
        horizontalalignment="left",
        verticalalignment="bottom",
        color=color,
    )

    ax.annotate(
        r"$(L, 0)$",
        xy=(L, 0),
        xycoords="data",
        horizontalalignment="left",
        verticalalignment="top",
        color=color,
    )


def main():
    filename_large = f"data_xy_large.npz"
    data_large = np.load(filename_large)
    xy_coords_large = data_large["xy_coords"]
    mask_xy_large = data_large["mask_xy"]

    filename_small = f"data_xy_small.npz"
    data_small = np.load(filename_small)
    xy_coords_small = data_small["xy_coords"]
    mask_xy_small = data_small["mask_xy"]

    # The mask removes the corner and the part that's not in the small/large range.
    xy_coords_masked_large = xy_coords_large[mask_xy_large]
    xy_coords_masked_small = xy_coords_small[mask_xy_small]

    L = 2
    H = 1
    h = 0.5
    nx, ny = 16, 16

    borehole_x = 0.6
    borehole_delta = 0.05

    x_vals = xy_coords_masked_small[:, 0]
    xy_coords_top_small = np.array(
        stretch_mesh(x_vals, np.ones_like(x_vals), L, H, h=h)
    ).T

    xy_coords_left = xy_coords_masked_small[x_vals < borehole_x - borehole_delta]
    xy_coords_right = xy_coords_masked_small[x_vals > borehole_x + borehole_delta]
    xy_coords_borehole = xy_coords_masked_small[
        np.abs(x_vals - borehole_x) <= borehole_delta
    ]
    xy_coords_surface = xy_coords_masked_small[
        np.abs(xy_coords_masked_small[:, 1] - xy_coords_top_small[:, 1]) < 0.01
    ]

    x_vals = xy_coords_masked_large[:, 0]
    xy_coords_right_large = xy_coords_masked_large[x_vals > borehole_x + borehole_delta]

    top_x = np.linspace(0, L, 100)
    top_y = np.ones_like(top_x)

    top = np.array(stretch_mesh(top_x, top_y, L, H, h=h)).T
    right = np.array([top[-1, :], (L, 0)])
    bottom = np.array([(L, 0), (0, 0)])
    left = np.array([(0, 0), top[0, :]])

    mesh, top_boundary, right_boundary = build_mesh(L, H, nx, ny, h=h)

    ct = np.array(stretch_mesh(L * 0.5, H, L, H, h=h))
    cr = np.array((L, H / 2))
    cl = np.array((0, (H + h) / 2))
    cb = np.array((L / 2, 0))

    ORANGE = "#fc8d62"
    BLUE = "#8da0cb"
    GREEN = "#66c2a5"
    MOSS_GREEN = "#a6d854"
    PINK = "#e78ac3"

    group_kwargs = dict(
        zoomable=True,
        lw=0,
        size=2,
        alpha=0.6,
    )

    plt.figure(figsize=(6.4 * 1.5, 4.8))
    bottom_color = BLUE
    left_color = GREEN
    top_color = ORANGE
    right_color = ORANGE

    label_corners(L, H, h)

    arrowprops = dict(arrowstyle="-|>")
    arrowprops = dict(arrowstyle="simple, tail_width=0.08, head_length=0.4")

    surface_kwargs = dict(group_kwargs, **point_kwargs_surface, size=5)
    plot_translucent_point_group(
        xy_coords_surface[:, 0],
        xy_coords_surface[:, 1],
        label=r"$\text{surface}$",
        **surface_kwargs,
    )

    plot_translucent_point_group(
        xy_coords_left[:, 0],
        xy_coords_left[:, 1],
        label=r"$\text{left}$",
        **group_kwargs,
        **point_kwargs_left,
    )

    plot_translucent_point_group(
        xy_coords_borehole[:, 0],
        xy_coords_borehole[:, 1],
        label=r"$\text{borehole}$",
        **group_kwargs,
        **point_kwargs_borehole,
    )

    plot_translucent_point_group(
        xy_coords_right[:, 0],
        xy_coords_right[:, 1],
        label=r"$\text{right}$",
        **group_kwargs,
        **point_kwargs_right,
    )

    plot_translucent_point_group(
        xy_coords_right_large[:, 0],
        xy_coords_right_large[:, 1],
        label=r"$\text{large strain}$",
        **group_kwargs,
        **point_kwargs_right_large,
    )

    plot(mesh, zorder=0, alpha=0.4, linewidth=2)

    ax = plt.gca()

    ax.set_aspect("equal")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)

    plt.box(False)
    ax.axis("off")

    plt.legend(bbox_to_anchor=(1.05, 1 / 3), loc="center left", borderaxespad=0.0)

    plt.savefig("domain_mesh_partitions.png")

    plt.figure()
    label_corners(L, H, h)

    ax = plt.gca()

    ax.set_aspect("equal")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)

    plt.box(False)
    ax.axis("off")

    plot(mesh, zorder=0, alpha=0.4, linewidth=2)
    plt.savefig("domain_mesh.png")

    plt.show()


if __name__ == "__main__":
    main()

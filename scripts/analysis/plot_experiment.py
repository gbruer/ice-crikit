import tensorflow

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import pathlib


from crikit import *
from fenics import plot
from job_analyzer import escape_latex

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)

MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"
GREY = "#CCCCCC"

full_data = {
    "interior 0%": dict(
        experimental_loss=0.03708022231,
        invariant_loss=2.530363862,
        directory="good_runs/jobs/3267117[9].sched-torque.pace.gatech.edu",
        label="tanh (2,)",
    ),
    "interior 1%": dict(
        experimental_loss=0.07217127183,
        invariant_loss=1.502464349,
        directory="good_runs/jobs/3647630[2].sched-torque.pace.gatech.edu",
        label="softplus (4,)",
    ),
    "interior 5%": dict(
        experimental_loss=1.233454987,
        invariant_loss=3.187901371,
        directory="good_runs/jobs/3647621[5].sched-torque.pace.gatech.edu",
        label="tanh (2, 2, 2)",
    ),
    "surface+borehole 0%": dict(
        experimental_loss=0.1539977734,
        invariant_loss=2.583373269,
        directory="good_runs/jobs/3389005[9].sched-torque.pace.gatech.edu",
        label="tanh (2, 2, 2)",
    ),
    "surface+borehole 1%": dict(
        experimental_loss=0.4553115731,
        invariant_loss=0.7629199255,
        directory="good_runs/jobs/3647620[8].sched-torque.pace.gatech.edu",
        label="tanh (2, 2, 2)",
    ),
    "surface+borehole 5%": dict(
        experimental_loss=9.455271243,
        invariant_loss=2.814424676,
        directory="good_runs/jobs/3647623[4].sched-torque.pace.gatech.edu",
        label="tanh (2, 2, 2)",
    ),
    "surface 0%": dict(
        experimental_loss=0.0115172133,
        invariant_loss=1.291224799,
        directory="good_runs/jobs/3389010[9].sched-torque.pace.gatech.edu",
        label="softplus (4,)",
    ),
    "surface 1%": dict(
        experimental_loss=0.04028779406,
        invariant_loss=4.211934957,
        directory="good_runs/jobs/3648316[6].sched-torque.pace.gatech.edu",
        label="tanh (4, 4)",
    ),
    "surface 5%": dict(
        experimental_loss=0.8563114519,
        invariant_loss=1.698686461,
        directory="good_runs/jobs/3645850[9].sched-torque.pace.gatech.edu",
        label="softplus (2, 2)",
    ),
}


def read_file(filename, get_trained=True, mesh=None):
    with XDMFFile(str(filename)) as f:
        if mesh is None:
            mesh = Mesh()
            f.read(mesh)

        V = VectorFunctionSpace(mesh, "P", 2)
        Q = FunctionSpace(mesh, "DG", 0)
        Psi = FunctionSpace(mesh, "P", 1)

        u_initial = Function(V)
        f.read_checkpoint(u_initial, "w_initial_0", 0)

        p_initial = Function(Q)
        f.read_checkpoint(p_initial, "w_initial_1", 0)

        phi_initial = Function(Psi)
        f.read_checkpoint(phi_initial, "w_initial_2", 0)

        w_initial = (u_initial, p_initial, phi_initial)
        if not get_trained:
            return w_initial

        u_trained = Function(V)
        f.read_checkpoint(u_trained, "w_trained_0", 0)

        p_trained = Function(Q)
        f.read_checkpoint(p_trained, "w_trained_1", 0)

        phi_trained = Function(Psi)
        f.read_checkpoint(phi_trained, "w_trained_2", 0)

    w_trained = (u_trained, p_trained, phi_trained)
    return w_initial, w_trained


def make_stem_plots(savedir, full_data):
    print("Making plots by stem")
    for k, data_dict in full_data.items():
        nrows = 2
        ncols = 3
        fig, axs = plt.subplots(nrows, ncols, figsize=(5.3 * ncols, 1 + 4.9 * nrows))

        f_dict = data_dict["f"]

        plt.sca(axs[0][0])
        cb = plot(f_dict["u_initial"])
        plt.colorbar(cb)
        plt.title("initial $u$")

        plt.sca(axs[0][1])
        cb = plot(f_dict["p_initial"])
        plt.colorbar(cb)
        plt.title("initial $p$")

        plt.sca(axs[0][2])
        cb = plot(f_dict["phi_initial"])
        plt.colorbar(cb)
        plt.title(r"initial $\phi$")

        plt.sca(axs[1][0])
        cb = plot(f_dict["u_trained"])
        plt.colorbar(cb)
        plt.title("trained $u$")

        plt.sca(axs[1][1])
        cb = plot(f_dict["p_trained"])
        plt.colorbar(cb)
        plt.title("trained $p$")

        plt.sca(axs[1][2])
        cb = plot(f_dict["phi_trained"])
        plt.colorbar(cb)
        plt.title(r"trained $\phi$")

        good_label = f"{k} ({data_dict['label']})"
        plt.suptitle(escape_latex(good_label))

        for ax in axs.flatten():
            ax.set_aspect("equal")
            ax.spines["top"].set_visible(False)
            ax.spines["right"].set_visible(False)
            ax.spines["bottom"].set_visible(False)
            ax.spines["left"].set_visible(False)

            ax.set_frame_on(False)
            ax.axis("off")

        plt.savefig(savedir / good_label)
        plt.close()


def make_delta_stem_plots(savedir, full_data):
    print("Making delta plots by stem")
    for k, data_dict in full_data.items():
        nrows = 2
        ncols = 3
        fig, axs = plt.subplots(nrows, ncols, figsize=(5.3 * ncols, 1 + 4.9 * nrows))

        cmap = "seismic"
        levels = 40
        divnorm = lambda: colors.TwoSlopeNorm(vcenter=0)

        df_dict = data_dict["df"]

        plt.sca(axs[0][0])
        cb = plot(df_dict["u_initial"])
        plt.colorbar(cb)
        plt.title(r"initial $\Delta u$")

        plt.sca(axs[0][1])
        cb = plot(df_dict["p_initial"], cmap=cmap, norm=divnorm(), levels=levels)
        plt.colorbar(cb)
        plt.title(r"initial $\Delta p$")

        plt.sca(axs[0][2])
        cb = plot(df_dict["phi_initial"], cmap=cmap, norm=divnorm(), levels=levels)
        plt.colorbar(cb)
        plt.title(r"initial $\Delta \phi$")

        plt.sca(axs[1][0])
        cb = plot(df_dict["u_trained"])
        plt.colorbar(cb)
        plt.title(r"trained $\Delta u$")

        plt.sca(axs[1][1])
        cb = plot(df_dict["p_trained"], cmap=cmap, norm=divnorm(), levels=levels)
        plt.colorbar(cb)
        plt.title(r"trained $\Delta p$")

        plt.sca(axs[1][2])
        cb = plot(df_dict["phi_trained"], cmap=cmap, norm=divnorm(), levels=levels)
        plt.colorbar(cb)
        plt.title(r"trained $\Delta \phi$")

        good_label = f"{k} ({data_dict['label']}) delta"
        plt.suptitle(escape_latex(good_label))

        for ax in axs.flatten():
            ax.set_aspect("equal")
            ax.spines["top"].set_visible(False)
            ax.spines["right"].set_visible(False)
            ax.spines["bottom"].set_visible(False)
            ax.spines["left"].set_visible(False)

            ax.set_frame_on(False)
            ax.axis("off")

        plt.savefig(savedir / good_label)
        plt.close()


def func_key_delta_plot(full_data, func_key, vmin=None, vmax=None, mapper=None):
    if mapper is None:
        mapper = lambda x: x
    nrows = 3
    ncols = 3
    fig, axs = plt.subplots(
        nrows,
        ncols,
        figsize=(6 * ncols, 1 + 4.9 * nrows),
        constrained_layout=True,
        tight_layout=False,
    )
    axs_flat = axs.flatten()
    colorbars = []

    if "u_" in func_key:
        clim_kwargs = dict(clim=(vmin, vmax))
        divnorm = lambda: None
        cmap = "viridis"
    else:
        cmap = "seismic"
        if "p_" in func_key:
            levels = 300
        else:
            levels = 40
        clim_kwargs = dict(vmin=vmin, vmax=vmax, levels=levels, cmap=cmap)
        divnorm = lambda: colors.TwoSlopeNorm(vcenter=0)

    plt.sca(axs[0][0])
    data_dict = full_data["interior 0%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.annotate(
        "interior",
        horizontalalignment="left",
        verticalalignment="bottom",
        xy=(0, 1),
        xycoords="axes fraction",
    )
    plt.title(r"0\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[0][1])
    data_dict = full_data["interior 1%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.title(r"1\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[0, 2])
    data_dict = full_data["interior 5%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.title(r"5\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[1, 0])
    data_dict = full_data["surface+borehole 0%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.annotate(
        "surface+borehole",
        horizontalalignment="left",
        verticalalignment="bottom",
        xy=(0, 1),
        xycoords="axes fraction",
    )
    # plt.title(r"0\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[1, 1])
    data_dict = full_data["surface+borehole 1%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"1\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[1, 2])
    data_dict = full_data["surface+borehole 5%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"5\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[2, 0])
    data_dict = full_data["surface 0%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    plt.annotate(
        "surface",
        horizontalalignment="left",
        verticalalignment="bottom",
        xy=(0, 1),
        xycoords="axes fraction",
    )
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"0\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[2, 1])
    data_dict = full_data["surface 1%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"1\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[2, 2])
    data_dict = full_data["surface 5%"]
    df_dict = data_dict["df"]
    cb = plot(mapper(df_dict[func_key]), **clim_kwargs, norm=divnorm())
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"5\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    # cb = cm.ScalarMappable(norm=norm, cmap='viridis')
    if vmin is not None and vmax is not None:
        scm = plt.cm.ScalarMappable(norm=divnorm(), cmap=cmap)
        scm.set_clim(vmin, vmax)

        cb = fig.colorbar(scm, ax=axs[:, 2], shrink=0.7)

    plt.suptitle(escape_latex(func_key))

    for ax in axs_flat:
        ax.set_aspect("equal")
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)

        ax.set_frame_on(False)
        ax.axis("off")

    if vmin is None or vmax is None:
        vmin = min(cb.vmin for cb in colorbars)
        vmax = max(cb.vmax for cb in colorbars)

    return vmin, vmax


def make_delta_byfunc_plots(savedir, full_data, mesh):
    func_keys = [
        # 'u_initial',
        # 'p_initial',
        # 'phi_initial',
        "u_trained",
        "p_trained",
        "phi_trained",
    ]

    mapper = None
    # corner = (1.85, 0.07)
    # x = SpatialCoordinate(mesh)
    # def mapper(f):
    #     s = conditional(
    #         And(x[0] > Constant(corner[0]), x[1] < Constant(corner[1])),
    #         Zero(f.ufl_shape),
    #         f,
    #     )
    #     return s

    print("Making delta plots by func")
    for func_key in func_keys:
        vmin, vmax = func_key_delta_plot(full_data, func_key, mapper=mapper)
        plt.savefig(savedir / f"cbs_{func_key}")
        plt.close()

        if "p_" in func_key:
            vmax = 0.2
        func_key_delta_plot(full_data, func_key, vmin=vmin, vmax=vmax, mapper=mapper)
        plt.savefig(savedir / f"{func_key}")
        plt.close()


def func_key_funcs_plot(full_data, func_key, vmin=None, vmax=None, mapper=None):
    if mapper is None:
        mapper = lambda x: x
    nrows = 3
    ncols = 3
    fig, axs = plt.subplots(
        nrows,
        ncols,
        figsize=(6 * ncols, 1 + 4.9 * nrows),
        constrained_layout=True,
        tight_layout=False,
    )
    axs_flat = axs.flatten()
    colorbars = []

    if "u_" in func_key:
        clim_kwargs = dict(clim=(vmin, vmax))
    else:
        clim_kwargs = dict(vmin=vmin, vmax=vmax)

    plt.sca(axs[0][0])
    data_dict = full_data["interior 0%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.annotate(
        "interior",
        horizontalalignment="left",
        verticalalignment="bottom",
        xy=(0, 1),
        xycoords="axes fraction",
    )
    plt.title(r"0\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[0][1])
    data_dict = full_data["interior 1%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.title(r"1\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[0, 2])
    data_dict = full_data["interior 5%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.title(r"5\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[1, 0])
    data_dict = full_data["surface+borehole 0%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    plt.annotate(
        "surface+borehole",
        horizontalalignment="left",
        verticalalignment="bottom",
        xy=(0, 1),
        xycoords="axes fraction",
    )
    # plt.title(r"0\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[1, 1])
    data_dict = full_data["surface+borehole 1%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"1\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[1, 2])
    data_dict = full_data["surface+borehole 5%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"5\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[2, 0])
    data_dict = full_data["surface 0%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    plt.annotate(
        "surface",
        horizontalalignment="left",
        verticalalignment="bottom",
        xy=(0, 1),
        xycoords="axes fraction",
    )
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"0\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[2, 1])
    data_dict = full_data["surface 1%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"1\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    plt.sca(axs[2, 2])
    data_dict = full_data["surface 5%"]
    f_dict = data_dict["f"]
    cb = plot(mapper(f_dict[func_key]), **clim_kwargs)
    if vmin is None or vmax is None:
        colorbars.append(plt.colorbar(cb))
    # plt.title(r"5\%")
    label = f"{data_dict['label']} exp:{data_dict['experimental_loss']:.3f}, invt:{data_dict['invariant_loss']:.3f}"
    plt.annotate(
        label,
        horizontalalignment="center",
        alpha=0.7,
        verticalalignment="top",
        xy=(0.5, -0.01),
        xycoords="axes fraction",
    )

    # cb = cm.ScalarMappable(norm=norm, cmap='viridis')
    if vmin is not None and vmax is not None:
        scm = plt.cm.ScalarMappable(cmap="viridis")
        scm.set_clim(vmin, vmax)

        cb = fig.colorbar(scm, ax=axs[:, 2], shrink=0.7)

    plt.suptitle(escape_latex(func_key))

    for ax in axs_flat:
        ax.set_aspect("equal")
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)

        ax.set_frame_on(False)
        ax.axis("off")

    if vmin is None or vmax is None:
        vmin = min(cb.vmin for cb in colorbars)
        vmax = max(cb.vmax for cb in colorbars)

    return vmin, vmax


def make_funcs_byfunc_plots(savedir, full_data, mesh):
    func_keys = [
        # 'u_initial',
        # 'p_initial',
        # 'phi_initial',
        "u_trained",
        "p_trained",
        "phi_trained",
    ]

    mapper = None
    # corner = (1.85, 0.07)
    # x = SpatialCoordinate(mesh)
    # def mapper(f):
    #     s = conditional(
    #         And(x[0] > Constant(corner[0]), x[1] < Constant(corner[1])),
    #         Zero(f.ufl_shape),
    #         f,
    #     )
    #     return s

    print("Making funcs plots by func")
    for func_key in func_keys:
        vmin, vmax = func_key_funcs_plot(full_data, func_key, mapper=mapper)
        plt.savefig(savedir / f"cbs_{func_key}")
        plt.close()

        # if "p_" in func_key:
        #     vmax = 0.2
        func_key_funcs_plot(full_data, func_key, vmin=vmin, vmax=vmax, mapper=mapper)
        plt.savefig(savedir / f"{func_key}")
        plt.close()


def true_plots(u, p, phi):
    savedir = pathlib.Path(".")
    nrows = 1
    ncols = 3
    fig, axs = plt.subplots(
        nrows, ncols, figsize=(5.3 * ncols, 1 + 4.9 * nrows), squeeze=False
    )

    plt.sca(axs[0, 0])
    cb = plot(u)
    plt.colorbar(cb, shrink=0.6)
    plt.title(r"$u_0$")

    plt.sca(axs[0, 1])
    cb = plot(p)
    plt.colorbar(cb, shrink=0.6)
    plt.title(r"$p_0$")

    plt.sca(axs[0, 2])
    cb = plot(phi)
    plt.colorbar(cb, shrink=0.6)
    plt.title(r"$\phi_0$")

    plt.savefig(savedir / "ground_truth_solution")
    plt.close()


def main():
    u_true, p_true, phi_true = read_file("experiment_output_true.xdmf", False)
    mesh = u_true.function_space().mesh()

    ground_truth_plot = False
    if ground_truth_plot:
        true_plots(u_true, p_true, phi_true)

    savedir = pathlib.Path("figs/best_networks")
    savedir.mkdir(parents=True, exist_ok=True)
    print("Reading data")
    for k, data_dict in full_data.items():
        directory = pathlib.Path(data_dict["directory"])
        filename_exp = directory / "experiment_output.xdmf"

        print(filename_exp)

        w_initial, w_trained = read_file(filename_exp, mesh=mesh)
        u_initial, p_initial, phi_initial = w_initial
        u_trained, p_trained, phi_trained = w_trained

        data_dict["f"] = {}
        data_dict["f"]["u_initial"] = u_initial
        data_dict["f"]["p_initial"] = p_initial
        data_dict["f"]["phi_initial"] = phi_initial
        data_dict["f"]["u_trained"] = u_trained
        data_dict["f"]["p_trained"] = p_trained
        data_dict["f"]["phi_trained"] = phi_trained

        data_dict["df"] = {}
        data_dict["df"]["u_initial"] = u_initial - u_true
        data_dict["df"]["p_initial"] = p_initial - p_true
        data_dict["df"]["phi_initial"] = phi_initial - phi_true
        data_dict["df"]["u_trained"] = u_trained - u_true
        data_dict["df"]["p_trained"] = p_trained - p_true
        data_dict["df"]["phi_trained"] = phi_trained - phi_true

    stem_plots = False
    if stem_plots:
        savedir_funcs = savedir / "funcs"
        savedir_funcs.mkdir(parents=True, exist_ok=True)
        make_stem_plots(savedir_funcs, full_data)

    delta_stem_plots = True
    if delta_stem_plots:
        savedir_delta = savedir / "delta"
        savedir_delta.mkdir(parents=True, exist_ok=True)
        make_delta_stem_plots(savedir_delta, full_data)

    delta_byfunc_plots = True
    if delta_byfunc_plots:
        savedir_delta_byfunc = savedir / "delta_byfunc"
        savedir_delta_byfunc.mkdir(parents=True, exist_ok=True)
        make_delta_byfunc_plots(savedir_delta_byfunc, full_data, mesh)

    funcs_byfunc_plots = False
    if funcs_byfunc_plots:
        savedir_funcs_byfunc = savedir / "funcs_byfunc"
        savedir_funcs_byfunc.mkdir(parents=True, exist_ok=True)
        make_funcs_byfunc_plots(savedir_funcs_byfunc, full_data, mesh)


if __name__ == "__main__":
    main()

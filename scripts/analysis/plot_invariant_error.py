import tensorflow

import ast
import matplotlib.pyplot as plt
import pathlib


from crikit import *
from fenics import plot

from hyper_param_utils import HyperParams
from plotting import replot_translucent_point_group

from job_analyzer import AggregateErrorInfo, escape_filename, escape_latex

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)

MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"
GREY = "#CCCCCC"


INVARIANT_SPACE_ERROR_CMAP = "magma_r"


def load_aggregate_error_infos(labels, filename, mesh=None):
    """TODO: implement this function"""

    infos = {}
    info_dict = {f: None for f in AggregateErrorInfo._fields}
    with XDMFFile(filename) as xdmf:
        if mesh is None:
            mesh = Mesh()
            xdmf.read(mesh)

        V = FunctionSpace(mesh, "P", 1)

        for label in labels:
            for k in info_dict:
                u = Function(V)
                function_name = f"{label}_{k}"
                print(f"reading {function_name}")
                import pdb

                pdb.set_trace()
                xdmf.read_checkpoint(u, function_name, 0)
                info_dict[k] = u
            info = AggregateErrorInfo(**info_dict)
            infos[label] = info

    return infos


class PlotInfo:
    pass


def do_cr_plotter_plots(
    plot_info,
    f,
    title="",
    borehole=False,
    surface=False,
    nrows=1,
    ncols=2,
    do_colorbar=True,
    boundaries=None,
    **plot_kwargs,
):
    m = plot(f, **plot_kwargs, vmin=0)
    if do_colorbar:
        if boundaries is not None:
            c_kwargs = dict(boundaries=boundaries)
        else:
            c_kwargs = {}
        plt.colorbar(m, **c_kwargs)
    plt.xlim(plot_info.x_range)
    plt.ylim(plot_info.y_range)
    plt.xlabel(plot_info.invariant_names[plot_info.invariant_idxs[0]])

    ax = plt.gca()
    ax.set_aspect(1.0 / ax.get_data_ratio(), adjustable="datalim")

    if True:
        group_kwargs = dict(
            zoomable=False,
            label="Experiment Invariants",
            lw=0,
            alpha=0.4,
        )
        if (
            hasattr(plot_info, "patches_invts_full")
            and plot_info.patches_invts_full is not None
        ):
            patches = replot_translucent_point_group(
                plot_info.patches_invts_full,
                **group_kwargs,
                **point_kwargs_all,
            )

    if borehole:
        if (
            hasattr(plot_info, "patches_invts_left")
            and plot_info.patches_invts_left is not None
        ):
            patches = replot_translucent_point_group(
                plot_info.patches_invts_left,
                **group_kwargs,
                **point_kwargs_left,
            )
        if (
            hasattr(plot_info, "patches_invts_right")
            and plot_info.patches_invts_right is not None
        ):
            patches = replot_translucent_point_group(
                plot_info.patches_invts_right,
                **group_kwargs,
                **point_kwargs_right,
            )
        if (
            hasattr(plot_info, "patches_invts_close")
            and plot_info.patches_invts_close is not None
        ):
            patches = replot_translucent_point_group(
                plot_info.patches_invts_close,
                **group_kwargs,
                **point_kwargs_close,
            )
    if surface:
        if (
            hasattr(plot_info, "patches_invts_surface")
            and plot_info.patches_invts_surface is not None
        ):
            patches = replot_translucent_point_group(
                plot_info.patches_invts_surface,
                **group_kwargs,
                **point_kwargs_surface,
            )

        # plt.legend()
    if title != "":
        plt.title(title)

    return m


def read_stem_params_file(filepath):
    with filepath.open("r") as f:
        READ_MAX = int(1e9)
        s = f.read(READ_MAX)
        if len(s) >= READ_MAX:
            raise ValueError(f"This file is too big: {filepath}")
        stem_params_dict = ast.literal_eval(s)
    stem_params = {
        stem: HyperParams(**param_dict) for stem, param_dict in stem_params_dict.items()
    }
    return stem_params


def main():
    file_stem_params = pathlib.Path("analyzer-stem-params.txt")
    stem_params = read_stem_params_file(file_stem_params)
    stem_infos = load_aggregate_error_infos(stem_params, "cr_data/small.xdmf")

    plot_kwargs = dict(
        cmap=INVARIANT_SPACE_ERROR_CMAP,
        extend="max",
    )

    savedir = pathlib.Path("automated_test")
    savedir.mkdir(parents=True, exist_ok=True)

    plot_info = PlotInfo()
    plot_info.name = "small"
    plot_info.invariant_names = [
        r"tr $\dot\epsilon$",
        r"$\text{tr }\dot\epsilon^2$",
        r"$\phi$",
    ]
    plot_info.coeff_names = [r"$\frac{D\phi}{Dt}$"]
    plot_info.invariant_idxs = (1, 2)

    plot_info.x_range = (0, 20)
    plot_info.y_range = (0, 0.15)

    for label in stem_params:
        hp = stem_params[label]
        info = stem_infos[label]

        title_label = escape_latex(label)
        file_label = escape_filename(label)

        fig = plt.figure(figsize=(6 * 2, 4.8))
        plt.subplot(1, 2, 1)
        do_cr_plotter_plots(
            plot_info,
            info.rmse,
            "Standard error (RMSE)",
            borehole=hp.borehole,
            surface=hp.surface,
            **plot_kwargs,
        )
        plt.ylabel(plot_info.invariant_names[plot_info.invariant_idxs[1]])

        plt.subplot(1, 2, 2)
        do_cr_plotter_plots(
            plot_info,
            info.stddev,
            "Standard deviation of CR",
            borehole=hp.borehole,
            surface=hp.surface,
            **plot_kwargs,
        )
        plt.suptitle(title_label)
        fig.savefig(savedir / f"{plot_info.name}_{file_label}")
        plt.close()


if __name__ == "__main__":
    main()

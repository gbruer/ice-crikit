import matplotlib.pyplot as plt
import numpy as np

from mesh import stretch_mesh

SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALL_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)


def label_corners(L, H, h, color="gray"):
    ax = plt.gca()
    # ax.annotate(
    #     r'$(0, 0)$',
    #     xy=(0, 0),
    #     xycoords='data',
    #     horizontalalignment='right',
    #     verticalalignment='top',
    #     color=color,
    # )

    ax.annotate(
        r"$(0, H + h)$",
        xy=(0, H + h),
        xycoords="data",
        horizontalalignment="left",
        verticalalignment="bottom",
        color=color,
    )

    ax.annotate(
        r"$(L, H)$",
        xy=(L, H),
        xycoords="data",
        horizontalalignment="left",
        verticalalignment="bottom",
        color=color,
    )

    ax.annotate(
        r"$(L, 0)$",
        xy=(L, 0),
        xycoords="data",
        horizontalalignment="left",
        verticalalignment="top",
        color=color,
    )


def main():
    L = 2
    H = 1
    h = 0.5

    top_x = np.linspace(0, L, 100)
    top_y = np.ones_like(top_x)

    top = np.array(stretch_mesh(top_x, top_y, L, H, h=h)).T

    right = np.array(
        [
            top[-1, :],
            (L, 0),
        ]
    )

    bottom = np.array(
        [
            (L, 0),
            (0, 0),
        ]
    )

    left = np.array(
        [
            (0, 0),
            top[0, :],
        ]
    )

    ct = np.array(stretch_mesh(L * 0.5, H, L, H, h=h))
    cr = np.array((L, H / 2))
    cl = np.array((0, (H + h) / 2))
    cb = np.array((L / 2, 0))

    ORANGE = "#fc8d62"
    BLUE = "#8da0cb"
    GREEN = "#66c2a5"

    plt.figure()
    bottom_color = BLUE
    left_color = GREEN
    top_color = ORANGE
    right_color = ORANGE
    plt.plot(top[:, 0], top[:, 1], color=top_color)
    plt.plot(bottom[:, 0], bottom[:, 1], color=bottom_color)
    plt.plot(right[:, 0], right[:, 1], color=right_color)
    plt.plot(left[:, 0], left[:, 1], color=left_color)

    arrowprops = dict(arrowstyle="-|>")
    arrowprops = dict(arrowstyle="simple, tail_width=0.08, head_length=0.4")

    ax = plt.gca()

    # Boundary labels
    ax.annotate(
        r"$\Gamma_D$",
        xy=(L / 2, 0),
        xycoords="data",
        xytext=(L / 2, H / 5),
        textcoords="data",
        arrowprops=dict(**arrowprops, color=bottom_color),
        horizontalalignment="center",
        verticalalignment="bottom",
    )
    ax.annotate(
        r"$\Gamma_S$",
        xy=(0, (H + h) / 2),
        xycoords="data",
        xytext=(L / 5, (H + h) / 2),
        textcoords="data",
        arrowprops=dict(**arrowprops, color=left_color),
        horizontalalignment="left",
        verticalalignment="top",
    )
    ct = np.array(stretch_mesh(L * 0.5, H, L, H, h=h))
    cr = np.array((L, H / 2))
    mid = (ct + cr) / 2
    p = 0.6
    text_place = p * mid + (1 - p) * np.array((ct[0], cr[1]))
    ax.annotate(
        "",
        xy=cr,
        xycoords="data",
        xytext=text_place,
        textcoords="data",
        arrowprops=dict(**arrowprops, color=right_color),
        horizontalalignment="right",
        verticalalignment="top",
    )
    ax.annotate(
        r"$\Gamma_N$",
        xy=ct,
        xycoords="data",
        xytext=text_place,
        textcoords="data",
        arrowprops=dict(**arrowprops, color=top_color),
        horizontalalignment="right",
        verticalalignment="bottom",
    )

    label_corners(L, H, h)

    # Boundary conditions.
    ax.text(
        ct[0],
        ct[1],
        r"$\sigma \cdot \mathbf{u} = 0$",
        horizontalalignment="left",
        verticalalignment="bottom",
    )
    ax.text(
        cb[0],
        -0.05,
        r"$\mathbf{u} = 0$",
        horizontalalignment="center",
        verticalalignment="top",
    )

    ax.text(
        0.05,
        cl[1] + (H + h - cl[1]) * 0.1,
        r"$\mathbf{u} \cdot \mathbf{e_1} = 0$",
        horizontalalignment="left",
        verticalalignment="bottom",
    )

    ax.set_aspect("equal")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)

    plt.box(False)
    ax.axis("off")

    plt.savefig("domain_velocity.png")

    plt.figure()

    MOSS_GREEN = "#a6d854"
    PINK = "#e78ac3"
    bottom_color = PINK
    left_color = PINK
    top_color = MOSS_GREEN
    right_color = PINK
    plt.plot(top[:, 0], top[:, 1], color=top_color)
    plt.plot(bottom[:, 0], bottom[:, 1], color=bottom_color)
    plt.plot(right[:, 0], right[:, 1], color=right_color)
    plt.plot(left[:, 0], left[:, 1], color=left_color)

    arrowprops = dict(arrowstyle="-|>")
    arrowprops = dict(arrowstyle="simple, tail_width=0.08, head_length=0.4")

    ax = plt.gca()

    # Boundary labels
    ax.annotate(
        r"$\Gamma_{D\phi}$",
        xy=ct,
        xycoords="data",
        xytext=(ct[0], ct[1] * 0.8),
        textcoords="data",
        arrowprops=dict(**arrowprops, color=top_color),
        horizontalalignment="center",
        verticalalignment="top",
    )
    text_place = (L / 2, H / 4)
    text = r"$\Gamma_{N\phi}$"
    ax.annotate(
        text,
        xy=cr,
        xycoords="data",
        xytext=text_place,
        textcoords="data",
        arrowprops=dict(**arrowprops, color=right_color),
        horizontalalignment="center",
        verticalalignment="center",
        alpha=0,
    )
    ax.annotate(
        text,
        xy=cl,
        xycoords="data",
        xytext=text_place,
        textcoords="data",
        arrowprops=dict(**arrowprops, color=left_color),
        horizontalalignment="center",
        verticalalignment="center",
        alpha=0,
    )
    ax.annotate(
        text,
        xy=cb,
        xycoords="data",
        xytext=text_place,
        textcoords="data",
        arrowprops=dict(**arrowprops, color=bottom_color),
        horizontalalignment="center",
        verticalalignment="center",
    )

    label_corners(L, H, h)

    # Boundary conditions.
    ax.text(
        ct[0],
        ct[1],
        r"$\phi = 0$",
        horizontalalignment="left",
        verticalalignment="bottom",
    )
    ax.text(
        cb[0],
        -0.05,
        r"$\nabla \phi \cdot \mathbf{n} = 0$",
        horizontalalignment="center",
        verticalalignment="top",
    )

    ax.set_aspect("equal")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)

    plt.box(False)
    ax.axis("off")

    plt.savefig("domain_damage.png")

    plt.show()


if __name__ == "__main__":
    main()

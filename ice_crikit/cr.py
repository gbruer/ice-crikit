import tensorflow
from crikit import *
from crikit.cr.map_builders import FunnelOut
from crikit.cr.ufl import point_map as ufl_point_map
from jax import numpy as jnp

import jax

from ice_crikit.network_builders import MLP, make_jax_network


def make_jax_cr_tau(p, eps2, dim, print_cr=True):
    input_types = (
        TensorType.make_symmetric(2, dim, "eps"),
        TensorType.make_scalar("phi"),
    )
    output_type = TensorType.make_symmetric(2, dim, "tau")

    def cr_func(scalar_invts, p, eps2):
        e2 = scalar_invts[1]
        phi = scalar_invts[2]
        phi = 0.999 * phi
        mu = (e2 + eps2) ** ((p - 2) / 2)
        E = 1 - phi
        isotropic_scale = 2 * E * mu

        return jnp.array([0, isotropic_scale])

    cr = CR(
        output_type,
        input_types,
        cr_func,
        params=(p, eps2),
    )

    if print_cr:
        print("-----cr_tau------------------------------------")
        print(cr.invariant_descriptions())
        print("-----------------------------------------------")
    return cr


def make_jax_network_cr(
    seed, dim, hidden_activation, layer_sizes, weights_file=None, do_print=True
):
    input_types = (
        TensorType.make_symmetric(2, dim, "eps"),
        TensorType.make_scalar("phi"),
    )
    output_type_DphiDt = TensorType.make_scalar("DphiDt")

    num_inputs, num_outputs = cr_function_shape(output_type_DphiDt, input_types)
    network = make_jax_network(
        seed,
        num_inputs,
        num_outputs,
        MLP,
        hidden_activation=hidden_activation,
        output_activation="linear",
        layer_sizes=layer_sizes,
    )

    if weights_file is not None:
        network.set_params_from_keras_file(weights_file)

    if do_print:
        # Print out network information
        print("Network info:", network)

    # Create CR.
    cr = CR(
        output_type_DphiDt,
        input_types,
        network,
        nojit=False,
        params=[p.val for p in network.get_flat_params()],
    )
    if do_print:
        print("-----cr_DphiDt---------------------------------")
        print(cr.invariant_descriptions())
        print("-----------------------------------------------")
    return cr, network


def make_jax_cr(p, dim, eps2, gamma_f, esf, gamma_h, eh):
    input_types = (
        TensorType.make_symmetric(2, dim, "eps"),
        TensorType.make_scalar("phi"),
    )
    output_type_DphiDt = TensorType.make_scalar("DphiDt")
    output_type_tau = TensorType.make_symmetric(2, dim, "tau")

    def cr_func_tau(scalar_invts, p, eps2, gamma_f, esf, gamma_h, eh):
        e2 = scalar_invts[1]
        phi = scalar_invts[2]
        mu = (e2 + eps2) ** ((p - 2) / 2)
        isotropic_scale = 2 * (1 - phi) * mu

        return jnp.array([0, isotropic_scale])

    def cr_func_DphiDt(scalar_invts, p, eps2, gamma_f, esf, gamma_h, eh):
        e2 = scalar_invts[1]
        phi = scalar_invts[2]

        e = jnp.sqrt(e2 + eps2)

        def fracturing_term(_):
            return gamma_f * e * (1 - phi)

        def healing_term(_):
            return gamma_h * (e - eh)

        def zero(_):
            return 0.0

        # Fracturing term.
        DphiDt = jax.lax.cond(
            e2 >= ((1 - phi) ** (2 / (1 - p))) * esf ** 2,
            fracturing_term,
            zero,
            operand=None,
        )

        # Healing term.
        DphiDt += jax.lax.cond(
            jnp.logical_and(phi >= 0, e <= eh), healing_term, zero, operand=None
        )

        return DphiDt

    cr_tau = CR(
        output_type_tau,
        input_types,
        cr_func_tau,
        params=(p, eps2, gamma_f, esf, gamma_h, eh),
    )

    cr_DphiDt = CR(
        output_type_DphiDt,
        input_types,
        cr_func_DphiDt,
        params=(p, eps2, gamma_f, esf, gamma_h, eh),
    )
    print("-----cr_tau------------------------------------")
    print(cr_tau.invariant_descriptions())
    print("-----cr_DphiDt---------------------------------")
    print(cr_DphiDt.invariant_descriptions())
    print("-----------------------------------------------")
    funnel = FunnelOut(cr_tau.source, 2)
    parallel = ParallelPointMap(cr_tau, cr_DphiDt)
    cr = get_composite_cr(funnel, parallel)
    return cr, cr_tau, cr_DphiDt


def make_ufl_invariants_cr(p, dim, eps2, gamma_f, esf, gamma_h, eh, linear_invt=False):
    from ufl import And

    @ufl_point_map((3,))
    def cr_true_inner_func(invariants):
        e1, e2, phi = invariants
        if linear_invt:
            e = e2
            e2 = e ** 2
        else:
            e = sqrt(e2 + eps2)

        E = 1 - phi
        # Fracturing term.
        DphiDt = conditional(e >= (E ** (1 / (1 - p))) * esf, gamma_f * e * E, 0)

        # Healing term.
        DphiDt += conditional(And(phi >= 0, e <= eh), gamma_h * (e - eh), 0)

        return DphiDt

    @ufl_point_map(((2, 2), ()), bare=True)
    def cr_true_scalar_invt(eps, phi):
        e1 = tr(eps)
        e2 = inner(eps, eps)
        if linear_invt:
            e2 = e2 ** 0.5
        return as_vector((e1, e2, phi))

    return cr_true_inner_func, cr_true_scalar_invt


def make_ufl_cr(p, dim, eps2, gamma_f, esf, gamma_h, eh):
    from ufl import And

    use_stress_threshold = False

    def ufl_exprs(point):
        epsilon, phi = point
        e2 = tr(dot(epsilon, epsilon))
        mu = (e2 + eps2) ** ((p - 2) / 2)
        phi = Constant(0.999) * phi
        E = 1 - phi
        tau = 2 * E * mu * epsilon

        e = sqrt(e2 + eps2)
        s2 = inner(tau, tau)

        # Fracturing term.
        if use_stress_threshold:
            DphiDt = conditional(s2 * 3 / 2 >= esf ** 2, gamma_f * e * E, 0)
        else:
            DphiDt = conditional(
                e2 >= (E ** (2 / (1 - p))) * esf ** 2, gamma_f * e * E, 0
            )

        # Healing term.
        DphiDt += conditional(And(phi >= 0, e <= eh), gamma_h * (e - eh), 0)

        return tau, DphiDt

    inputs = create_ufl_standins(((dim, dim), ()))
    source_space = DirectSum(list(UFLExprSpace(e) for e in inputs))
    cr = CR_UFL_Expr(
        source_space, ufl_exprs(inputs), {arg: i for i, arg in enumerate(inputs)}
    )
    return cr

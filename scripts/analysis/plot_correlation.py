import tensorflow

import matplotlib.pyplot as plt
import numpy as np

SMALLER_SIZE = 16
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 24
plt.rcParams.update(
    {
        "lines.linewidth": 3,
        "text.usetex": True,
        "figure.titlesize": BIGGER_SIZE,
        "figure.autolayout": True,
        "font.size": SMALL_SIZE,
        "axes.labelsize": MEDIUM_SIZE,
        "axes.titlesize": SMALL_SIZE,
        "legend.fontsize": SMALLER_SIZE,
        "xtick.labelsize": SMALL_SIZE,
        "ytick.labelsize": SMALL_SIZE,
        "savefig.format": "png",
        "text.latex.preamble": r"""\usepackage{amsmath}""",
    }
)

MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"
GREY = "#CCCCCC"


def add_spread_markers(datasets, colors, exp=True):
    xlim = plt.xlim()
    ax = plt.gca()
    # ax.annotate(
    #     r"$\sigma_x$:",
    #     xy=(0.01, 0),
    #     xycoords="axes fraction",
    #     horizontalalignment="left",
    #     verticalalignment="bottom",
    #     color="black",
    # )

    sigma_y_height = 0.0
    sigma_y_va = "bottom"
    ax.annotate(
        r"$\sigma_y$:",
        xy=(0.01, sigma_y_height),
        xycoords="axes fraction",
        horizontalalignment="left",
        verticalalignment=sigma_y_va,
        color="black",
    )
    # dx = (xlim[1]/xlim[0]) ** (1 /  len(datasets))
    for i, (d, color) in enumerate(zip(datasets, colors)):
        x, y = d[:, 0], d[:, 1]
        if exp:
            x = np.log(x)
            y = np.log(y)

        y_mean = y.mean()
        y_stddev = y.std()

        x_mean = x.mean()
        x_stddev = x.std()

        # startx = xlim[0] * (dx ** i)

        # line_x = (startx, startx * dx)
        line_x_vert = (x_mean - x_stddev, x_mean + x_stddev)
        line_y_vert = (y_mean, y_mean)

        line_x_hori = (x_mean, x_mean)
        line_y_hori = (y_mean - y_stddev, y_mean + y_stddev)

        if exp:
            line_x_vert = np.exp(line_x_vert)
            line_y_vert = np.exp(line_y_vert)

            line_x_hori = np.exp(line_x_hori)
            line_y_hori = np.exp(line_y_hori)

            x_stddev = np.exp(x_stddev)
            y_stddev = np.exp(y_stddev)

        plt.plot(
            line_x_vert,
            line_y_vert,
            color="black",
            alpha=0.7,
            linewidth=2,
            linestyle="--",
        )
        plt.plot(
            line_x_hori,
            line_y_hori,
            color="black",
            alpha=0.7,
            linewidth=2,
            linestyle="--",
        )

        # ax.annotate(
        #     f"{x_stddev:.4f}",
        #     xy=((i + 0.9) / len(datasets), 0),
        #     xycoords="axes fraction",
        #     horizontalalignment="right",
        #     verticalalignment="bottom",
        #     color="black",
        # )

        ax.annotate(
            f"{y_stddev:.2f}",
            xy=((i + 0.9) / len(datasets), sigma_y_height),
            xycoords="axes fraction",
            horizontalalignment="right",
            verticalalignment=sigma_y_va,
            color="black",
        )


def main():
    filename_interior = f"correlation_data_interior.npz"
    data_interior = np.load(filename_interior)
    interior_0 = data_interior["0_"]
    interior_1 = data_interior["1_"]
    interior_5 = data_interior["5_"]

    filename_surface_borehole = f"correlation_data_surface+borehole.npz"
    data_surface_borehole = np.load(filename_surface_borehole)
    surface_borehole_0 = data_surface_borehole["0_"]
    surface_borehole_1 = data_surface_borehole["1_"]
    surface_borehole_5 = data_surface_borehole["5_"]

    filename_surface = f"correlation_data_surface.npz"
    data_surface = np.load(filename_surface)
    surface_0 = data_surface["0_"]
    surface_1 = data_surface["1_"]
    surface_5 = data_surface["5_"]

    # import pdb; pdb.set_trace()

    fig = plt.figure(figsize=(5 * 3, 4.8))

    plt.subplot(1, 3, 1)
    datasets = [interior_0, interior_1, interior_5]
    colors = []
    for d in datasets:
        x, y = d[:, 0], d[:, 1]
        label = f"({np.corrcoef(x, y)[1, 0]:.2f})"
        (line,) = plt.loglog(x, y, ".", label=label)
        colors.append(line.get_color())
    add_spread_markers(datasets, colors)
    plt.xlabel("Experimental loss")
    plt.ylabel("Invariant loss")
    plt.title("interior")

    group_ylim = list(plt.ylim())

    plt.legend()

    plt.subplot(1, 3, 2)
    datasets = [surface_borehole_0, surface_borehole_1, surface_borehole_5]
    colors = []
    for d in datasets:
        x, y = d[:, 0], d[:, 1]
        label = f"({np.corrcoef(x, y)[1, 0]: .4f})"
        (line,) = plt.loglog(x, y, ".", label=label)
        colors.append(line.get_color())
    add_spread_markers(datasets, colors)
    plt.xlabel("Experimental loss")
    plt.title("surface + borehole")

    cur_ylim = plt.ylim()
    group_ylim[0] = min(group_ylim[0], cur_ylim[0])
    group_ylim[1] = max(group_ylim[1], cur_ylim[1])
    plt.legend()

    plt.subplot(1, 3, 3)
    datasets = [surface_0, surface_1, surface_5]
    colors = []
    for d in datasets:
        x, y = d[:, 0], d[:, 1]
        label = f"({np.corrcoef(x, y)[1, 0]: .4f})"
        (line,) = plt.loglog(x, y, ".", label=label)
        colors.append(line.get_color())
    add_spread_markers(datasets, colors)
    plt.xlabel("Experimental loss")
    plt.title("surface")

    cur_ylim = plt.ylim()
    group_ylim[0] = min(group_ylim[0], cur_ylim[0])
    group_ylim[1] = max(group_ylim[1], cur_ylim[1])
    plt.legend()

    for i in range(3):
        plt.subplot(1, 3, i + 1)
        plt.ylim(group_ylim)

    plt.savefig("correlation")
    # plot_label = f"{plot_label} ({np.corrcoef(*data_list)[1, 0]:.4f})"
    # plt.title(plot_label)

    plt.show()


if __name__ == "__main__":
    main()

from crikit import *
from crikit.cr.space_builders import DirectSum
from crikit.cr.quadrature import make_quadrature_spaces

import crikit.utils as utils
import numpy as onp

try:
    import matplotlib.pyplot as plt
except:
    pass

from ice_crikit.plotting import plot_translucent_point_group


class InvariantCRPlotData:
    def __init__(self, invts_map, coeff_map):
        self.invts_map = invts_map
        self.coeff_map = coeff_map

    def compute(
        self,
        cr_inputs_ufl_xy,
        invariants_space_xy_quad,
        coeff_space_xy_quad,
        input_invariants_ufl,
        coeff_space_quad,
        invariants_space_quad,
        cr_mesh,
        quad_params,
    ):

        do_xy = cr_inputs_ufl_xy is not None
        if do_xy:
            # Compute scalar invariants on the experiment mesh.
            cr_inputs_ufl_xy = Enlist(cr_inputs_ufl_xy)
            cr_input_space_ufl = DirectSum(
                [UFLExprSpace(s, ufl_domains=()) for s in cr_inputs_ufl_xy]
            )
            scalar_invt_ufl_map = get_composite_cr(cr_input_space_ufl, self.invts_map)
            scalar_invts_xy = scalar_invt_ufl_map(cr_inputs_ufl_xy)

            # Compute CR output on the experiment mesh.
            coeff_np_space = Ndarrays((-1,) + coeff_space_xy_quad.shape())
            inner_map_ufl = get_composite_cr(
                self.invts_map.target, self.coeff_map, coeff_space_xy_quad
            )
            get_coeff_np = get_composite_cr(coeff_space_xy_quad, coeff_np_space)
            self.coeff_func_xy_quad = inner_map_ufl(scalar_invts_xy)
            self.coeff_func_xy_quad_np = get_coeff_np(self.coeff_func_xy_quad)

            # Convert the scalar invariants to a NumPy array.
            invariants_np_space = Ndarrays((-1,) + invariants_space_xy_quad.shape())
            get_quad_invts_xy = get_composite_cr(
                self.invts_map.target, invariants_space_xy_quad
            )
            get_quad_invts_xy_np = get_composite_cr(
                invariants_space_xy_quad, invariants_np_space
            )
            self.scalar_invts_quad_xy = get_quad_invts_xy(scalar_invts_xy)
            self.scalar_invts_quad_xy_np = get_quad_invts_xy_np(
                self.scalar_invts_quad_xy
            )

        # Build scalar invariants on generalization mesh.
        invariants_expr_space = UFLExprSpace(input_invariants_ufl, ufl_domains=())
        invariants_np_space = Ndarrays((-1,) + invariants_expr_space.shape())
        coeff_np_space = Ndarrays((-1,) + coeff_space_quad.shape())

        # Convert the scalar invariants on generalization mesh to a NumPy array.
        get_quad_invts = get_composite_cr(
            invariants_expr_space,
            invariants_space_quad,
            domain=cr_mesh.ufl_domain(),
            quad_params=quad_params,
        )
        get_quad_invts_np = get_composite_cr(
            invariants_space_quad,
            invariants_np_space,
            domain=cr_mesh.ufl_domain(),
            quad_params=quad_params,
        )
        self.scalar_invts_quad = get_quad_invts(input_invariants_ufl)
        self.scalar_invts_quad_np = get_quad_invts_np(self.scalar_invts_quad)
        # FIXME: Actually, just set the scalar invts to be the coordinates.
        # self.scalar_invts_quad_np = self.scalar_invts_quad.function_space().tabulate_dof_coordinates()[::3, ]
        # self.scalar_invts_quad_np = onp.concatenate((onp.zeros_like(self.scalar_invts_quad_np[:, :1]), self.scalar_invts_quad_np), axis=1)
        # self.scalar_invts_quad.vector().set_local(self.scalar_invts_quad_np.flatten())

        # Compute CR output on generalization mesh.
        ufl_cr = get_composite_cr(
            invariants_expr_space,
            self.coeff_map,
            coeff_space_quad,
            domain=cr_mesh.ufl_domain(),
            quad_params=quad_params,
        )
        get_coeff_np = get_composite_cr(
            coeff_space_quad,
            coeff_np_space,
            domain=cr_mesh.ufl_domain(),
            quad_params=quad_params,
        )
        # import pdb; pdb.set_trace()
        self.coeff_func_quad = ufl_cr(input_invariants_ufl)
        # FIXME: Actually, just use the hacked-in scalar invts.
        # self.coeff_func_quad = ufl_cr(self.scalar_invts_quad)

        # FIXME: Let's just hide nan values.
        # a = self.coeff_func_quad.vector()[:]
        # a[onp.isnan(a)] = 0
        # self.coeff_func_quad.vector().set_local(a.flatten())
        self.coeff_func_quad_np = get_coeff_np(self.coeff_func_quad)

    def record_to_file(self, filepath):
        do_xy = hasattr(self, "coeff_func_xy_quad_np")

        data = {}
        if do_xy:
            data["coeff_func_xy_quad_np"] = self.coeff_func_xy_quad_np
            data["scalar_invts_quad_xy_np"] = self.scalar_invts_quad_xy_np
        data["scalar_invts_quad_np"] = self.scalar_invts_quad_np
        data["coeff_func_quad_np"] = self.coeff_func_quad_np

        onp.savez(filepath, **data)

    def record_to_file_functions(self, filename):
        do_xy = hasattr(self, "coeff_func_xy_quad")
        enc = XDMFFile.Encoding.HDF5

        filename_xy = f"{filename}_xy.xdmf"
        filename = f"{filename}.xdmf"
        comm = self.coeff_func_xy_quad.function_space().mesh().mpi_comm()

        def handle_quadrature(w):
            if w.ufl_element().family() != "Quadrature":
                f.write_checkpoint(w, f"{name}", *args, **kwargs)
                return

            mesh = w.function_space().mesh()
            cell = mesh.ufl_cell()
            shape = w.ufl_shape
            if shape:
                TE = TensorElement("P", cell, degree=1, shape=shape)
            else:
                TE = FiniteElement("P", cell, degree=1)
            TV = FunctionSpace(mesh, TE)

            # Let's project to continuous space.
            w_c = project(w, TV)
            return w_c

        def write_mixed_element_function(f, w, name, *args, **kwargs):
            subspaces = [u.function_space().collapse() for u in w.split()]
            if len(subspaces) == 0:
                w_c = handle_quadrature(w)
                f.write_checkpoint(w_c, f"{name}", *args, **kwargs)
                return

            w_subs = [Function(V) for V in subspaces]
            fa = FunctionAssigner(subspaces, w.function_space())
            fa.assign(w_subs, w)
            for i, wi in enumerate(w_subs):
                w_c = handle_quadrature(wi)
                f.write_checkpoint(w_c, f"{name}_{i}", *args, **kwargs)

        # Write the functions to the file.
        if do_xy:
            with XDMFFile(comm, filename_xy) as f:
                f.write(self.coeff_func_xy_quad.function_space().mesh())

                write_mixed_element_function(
                    f,
                    self.coeff_func_xy_quad,
                    "coeff_func_xy_quad",
                    0,
                    enc,
                    append=True,
                )
                write_mixed_element_function(
                    f,
                    self.scalar_invts_quad_xy,
                    "scalar_invts_quad_xy",
                    0,
                    enc,
                    append=True,
                )
        with XDMFFile(comm, filename) as f:
            f.write(self.coeff_func_quad.function_space().mesh())
            write_mixed_element_function(
                f, self.scalar_invts_quad, "scalar_invts_quad", 0, enc, append=True
            )
            write_mixed_element_function(
                f, self.coeff_func_quad, "coeff_func_quad", 0, enc, append=True
            )

    def read_from_file(
        self,
        filepath,
        coeff_space_xy_quad,
        invariants_space_xy_quad,
        coeff_space_quad,
        invariants_space_quad,
    ):
        data = onp.load(filepath)

        do_xy = invariants_space_xy_quad is not None and "coeff_func_xy_quad_np" in data
        if do_xy:
            self.coeff_func_xy_quad_np = data["coeff_func_xy_quad_np"]
            self.scalar_invts_quad_xy_np = data["scalar_invts_quad_xy_np"]

            self.coeff_func_xy_quad = Function(coeff_space_xy_quad._functionspace)
            self.coeff_func_xy_quad.vector().set_local(self.coeff_func_xy_quad_np)

            self.scalar_invts_quad_xy = Function(
                invariants_space_xy_quad._functionspace
            )
            self.scalar_invts_quad_xy.vector().set_local(
                self.scalar_invts_quad_xy_np.flatten()
            )

        self.scalar_invts_quad_np = data["scalar_invts_quad_np"]
        self.coeff_func_quad_np = data["coeff_func_quad_np"]

        self.coeff_func_quad = Function(coeff_space_quad._functionspace)
        self.coeff_func_quad.vector().set_local(self.coeff_func_quad_np)


class InvariantCRPlotter:
    def __init__(
        self,
        num_scalar_invariants,
        num_form_coefficients,
        xy_mesh,
        quad_params=None,
        x_range=[0, 1],
        y_range=[0, 1],
        nx=40,
        ny=40,
        cr_mesh=None,
        invariant_cmap=None,
        invariant_idxs=(0, 1),
        coeff_names=None,
        invariant_names=None,
        coeff_idx=0,
        xy_cmap=None,
        name=None,
    ):

        self.coeff_names = coeff_names
        if self.coeff_names is None:
            self.coeff_names = [f"$coeff_{i}$" for i in range(num_form_coefficients)]

        self.invariant_names = invariant_names
        if self.invariant_names is None:
            self.invariant_names = [f"$invt_{i}$" for i in range(num_scalar_invariants)]
            self.full_invariant_names = "invariants"
        else:
            self.full_invariant_names = ", ".join(
                self.invariant_names[i] for i in invariant_idxs
            )

        if cr_mesh is None:
            x_range = onp.sqrt(x_range)
            p1 = Point(x_range[0], y_range[0])
            p2 = Point(x_range[1], y_range[1])
            cr_mesh = RectangleMesh(p1, p2, nx, ny)

            # def scale_mesh(mesh):
            #     C = mesh.coordinates()
            #     C[:, 0] = C[:, 0] ** 2
            #     mesh.coordinates()[:] = C

            # scale_mesh(cr_mesh)

        self.invariant_cmap = invariant_cmap
        self.xy_cmap = xy_cmap
        # self.dim = xy_mesh.geometric_dimension()
        self.xy_mesh = xy_mesh
        self.invariant_idxs = tuple(invariant_idxs)
        self.coeff_idx = coeff_idx
        self.x_range = x_range
        self.y_range = y_range
        self.cr_mesh = cr_mesh

        self.quad_params = (
            quad_params
            if quad_params is not None
            else get_default_covering_params().get("quad_params", None)
        )
        degree = 1

        self.invariants_space = VectorFunctionSpace(
            self.cr_mesh, "P", degree, dim=num_scalar_invariants
        )
        self.invariants_space_quad, _ = make_quadrature_spaces(
            UFLFunctionSpace(self.invariants_space),
            self.quad_params,
            self.cr_mesh.ufl_domain(),
        )

        self.coeff_space = FunctionSpace(self.cr_mesh, "P", degree)
        self.coeff_space_quad, _ = make_quadrature_spaces(
            UFLFunctionSpace(self.coeff_space),
            self.quad_params,
            self.cr_mesh.ufl_domain(),
        )

        if self.xy_mesh is not None:
            self.invariants_space_xy = VectorFunctionSpace(
                self.xy_mesh, "P", degree, dim=num_scalar_invariants
            )
            self.invariants_space_xy_quad, _ = make_quadrature_spaces(
                UFLFunctionSpace(self.invariants_space_xy),
                self.quad_params,
                self.xy_mesh.ufl_domain(),
            )

            self.coeff_space_xy = FunctionSpace(self.xy_mesh, "P", degree)
            self.coeff_space_xy_quad, _ = make_quadrature_spaces(
                UFLFunctionSpace(self.coeff_space_xy),
                self.quad_params,
                self.xy_mesh.ufl_domain(),
            )

        self.num_scalar_invariants = num_scalar_invariants
        self.num_form_coefficients = num_form_coefficients

        self.name = name
        if name is None:
            raise ValueError("Must specify a name!!!")

    def compute_from_file(self, scalar_invt_map, inner_map, filepath):
        data = InvariantCRPlotData(scalar_invt_map, inner_map)
        if self.xy_mesh is None:
            data.read_from_file(
                filepath,
                None,
                None,
                UFLFunctionSpace(self.coeff_space_quad),
                UFLFunctionSpace(self.invariants_space_quad),
            )
        else:
            data.read_from_file(
                filepath,
                UFLFunctionSpace(self.coeff_space_xy_quad),
                UFLFunctionSpace(self.invariants_space_xy_quad),
                UFLFunctionSpace(self.coeff_space_quad),
                UFLFunctionSpace(self.invariants_space_quad),
            )
        return data

    def compute(self, scalar_invt_map, inner_map, cr_inputs_ufl_xy):
        data = InvariantCRPlotData(scalar_invt_map, inner_map)
        input_invariants_ufl = self.build_input_invariants()
        if self.xy_mesh is None:
            data.compute(
                cr_inputs_ufl_xy,
                None,
                None,
                input_invariants_ufl,
                UFLFunctionSpace(self.coeff_space_quad),
                UFLFunctionSpace(self.invariants_space_quad),
                self.cr_mesh,
                quad_params=self.quad_params,
            )
        else:
            data.compute(
                cr_inputs_ufl_xy,
                UFLFunctionSpace(self.invariants_space_xy_quad),
                UFLFunctionSpace(self.coeff_space_xy_quad),
                input_invariants_ufl,
                UFLFunctionSpace(self.coeff_space_quad),
                UFLFunctionSpace(self.invariants_space_quad),
                self.cr_mesh,
                quad_params=self.quad_params,
            )
        return data

    def plot_cr_all(
        self,
        scalar_invt_map=None,
        inner_map=None,
        cr_inputs_ufl_xy=None,
        data=None,
        true_data=None,
        true_cb=None,
        true_cb_xy=None,
        prefix="",
        skip_plot=False,
    ):
        if data is None:
            if scalar_invt_map is None or inner_map is None or cr_inputs_ufl_xy is None:
                raise ValueError(
                    "If data=None, must pass in scalar_invt_map, inner_map, cr_inputs_ufl_xy"
                )
            data = self.compute(scalar_invt_map, inner_map, cr_inputs_ufl_xy)

        # Extract and plot the desired coefficient as a function of the spatial coordinates.
        coeff_func_xy_quad = data.coeff_func_xy_quad
        if coeff_func_xy_quad.ufl_shape != ():
            coeff_func_xy_quad = coeff_func_xy_quad[self.coeff_idx]

        coeff_name = self.coeff_names[self.coeff_idx]
        title = f"{prefix}{coeff_name} vs (x, y) (standard color)"
        filename = f"{prefix}c{self.coeff_idx} vs (x, y) (standard color)"
        if true_cb_xy is not None and not skip_plot:
            plt.figure()
            self.plot_cr_output_xy(
                coeff_func_xy_quad,
                title=title,
                vmin=true_cb_xy.zmin,
                vmax=true_cb_xy.zmax,
            )
            utils.saveplot(filename)
            title = f"{prefix}{coeff_name} vs (x, y)"
            filename = f"{prefix}c{self.coeff_idx} vs (x, y)"

        plt.figure()
        cb_xy = self.plot_cr_output_xy(coeff_func_xy_quad, title=title)
        if skip_plot:
            plt.close()
        else:
            utils.saveplot(filename)

        # Extract and plot the desired coefficient as a function of the invariants.
        coeff_func_quad = data.coeff_func_quad
        if coeff_func_quad.ufl_shape != ():
            coeff_func_quad = coeff_func_quad[self.coeff_idx]
        coeff_func_quad = project(coeff_func_quad, self.coeff_space_quad)

        scalar_invts_quad_xy_np = data.scalar_invts_quad_xy_np[:, self.invariant_idxs]

        title = (
            f"{prefix}{coeff_name} vs ({self.full_invariant_names}) (standard color)"
        )
        filename = f"{prefix}c{self.coeff_idx} vs (invariants) (standard color)"
        if true_cb is not None and not skip_plot:
            plt.figure()
            self.plot_cr_output(
                coeff_func_quad,
                scalar_invts_quad_xy_np,
                title=title,
                vmin=true_cb.zmin,
                vmax=true_cb.zmax,
            )
            utils.saveplot(filename)
            title = f"{prefix}{coeff_name} vs ({self.full_invariant_names})"
            filename = f"{prefix}c{self.coeff_idx} vs (invariants)"

        plt.figure()
        cb = self.plot_cr_output(
            coeff_func_quad,
            scalar_invts_quad_xy_np,
            title=title,
        )
        if skip_plot:
            plt.close()
            return cb, cb_xy, data
        utils.saveplot(filename)

        self.plot_collapsed_1d(
            data, self.invariant_idxs[0], true_data=true_data, prefix=prefix
        )
        self.plot_collapsed_1d(
            data, self.invariant_idxs[1], true_data=true_data, prefix=prefix
        )

        return cb, cb_xy, data

    def plot_collapsed_1d(self, pred_data, invariant_idx, true_data=None, prefix=""):
        coeff_np = pred_data.coeff_func_quad_np
        coeff_xy_np = pred_data.coeff_func_xy_quad_np
        if true_data is not None:
            true_coeff_np = true_data.coeff_func_quad_np
            true_coeff_xy_np = true_data.coeff_func_xy_quad_np
        else:
            true_coeff_np = None
            true_coeff_xy_np = None

        if pred_data.coeff_func_quad.ufl_shape != ():
            coeff_np = coeff_np[:, self.coeff_idx]
            coeff_xy_np = coeff_xy_np[:, self.coeff_idx]
            if true_data is not None:
                true_coeff_np = true_coeff_np[:, self.coeff_idx]
                true_coeff_xy_np = true_coeff_xy_np[:, self.coeff_idx]

        scalar_invts_np = pred_data.scalar_invts_quad_np[:, invariant_idx]
        scalar_invts_xy_np = pred_data.scalar_invts_quad_xy_np[:, invariant_idx]

        # Only plot the points within the fixed range.
        xlim = self.x_range if invariant_idx == self.invariant_idxs[0] else self.y_range
        mask = (scalar_invts_np >= xlim[0]) & (scalar_invts_np <= xlim[1])
        mask_xy = (scalar_invts_xy_np >= xlim[0]) & (scalar_invts_xy_np <= xlim[1])

        coeff_name = self.coeff_names[self.coeff_idx]
        title = f"{prefix}{coeff_name} vs {self.invariant_names[invariant_idx]}"
        filename = f"{prefix}c{self.coeff_idx} vs invariants[{invariant_idx}]"

        plt.figure()
        self.plot_1d(
            scalar_invts_np[mask],
            coeff_np[mask],
            true_coeff_np[mask] if true_coeff_np is not None else None,
            title=f"{title} (generalization)",
        )
        plt.ylabel(self.coeff_names[self.coeff_idx])
        plt.xlabel(self.invariant_names[invariant_idx])
        utils.saveplot(f"{filename} generalization")

        plt.figure()
        self.plot_1d(
            scalar_invts_xy_np[mask_xy],
            coeff_xy_np[mask_xy],
            true_coeff_xy_np[mask_xy] if true_coeff_xy_np is not None else None,
            title=f"{title} (training)",
        )
        plt.ylabel(self.coeff_names[self.coeff_idx])
        plt.xlabel(self.invariant_names[invariant_idx])
        utils.saveplot(f"{filename} training")

    def build_input_invariants(self):
        input_invariants = [0] * self.num_scalar_invariants
        x = SpatialCoordinate(self.cr_mesh)
        for i, xi in zip(self.invariant_idxs, x):
            input_invariants[i] = xi
        return as_vector(input_invariants)

    def plot_cr_output(
        self, coeff_func_quad, scalar_invts_quad_xy_np=None, **plot_kwargs
    ):
        """Plots the coefficient on the CR mesh and plots X's marking the xy invariants."""
        cmap = plot_kwargs.pop("cmap", self.invariant_cmap)
        s = plot_kwargs.pop("s", 50)

        if coeff_func_quad is not None:
            coeff_func = project(coeff_func_quad, self.coeff_space)
            cb = plot(coeff_func, cmap=cmap, **plot_kwargs)
            plt.colorbar(cb)
        else:
            cb = None
        if scalar_invts_quad_xy_np is not None:
            plt.scatter(
                scalar_invts_quad_xy_np[:, 0],
                scalar_invts_quad_xy_np[:, 1],
                marker="x",
                c="r",
                alpha=1,
                s=s,
                label="Experiment Invariants",
            )
            plt.legend()
        plt.xlim(self.x_range)
        plt.ylim(self.y_range)
        plt.xlabel(self.invariant_names[self.invariant_idxs[0]])
        plt.ylabel(self.invariant_names[self.invariant_idxs[1]])
        ax = plt.gca()
        ax.set_aspect(1.0 / ax.get_data_ratio(), adjustable="datalim")
        return cb

    def plot_cr_output_xy(self, coeff_func_xy_quad, **plot_kwargs):
        """This plots a scalar UFL expression."""
        cmap = plot_kwargs.pop("cmap", self.xy_cmap)
        if coeff_func_xy_quad.ufl_shape == ():
            coeff_func_xy = project(coeff_func_xy_quad, self.coeff_space_xy)
            cb = plot(coeff_func_xy, cmap=cmap, **plot_kwargs)
            plt.colorbar(cb)
        else:
            raise ValueError("I can't handle this yet!")
            cb = plot(coeff_func_xy_quad, cmap=cmap, **plot_kwargs)
            plt.colorbar(cb)
        return cb

    def plot_1d(
        self,
        scalar_invts_np,
        coeff_np,
        true_coeff_np,
        title="",
        **plot_kwargs,
    ):
        if true_coeff_np is None:
            # Plot full extrapolated output.
            plt.plot(
                scalar_invts_np,
                coeff_np,
                ".",
                alpha=0.3,
                **plot_kwargs,
                label="Candidate CR",
            )
        else:
            plt.xlim(scalar_invts_np.min(), scalar_invts_np.max())
            ymin = min(coeff_np.min(), true_coeff_np.min())
            ymax = max(coeff_np.max(), true_coeff_np.max())
            plt.ylim(ymin, ymax)
            point_kwargs = dict(
                alpha=0.5,
                lw=0,
            )
            plot_translucent_point_group(
                scalar_invts_np,
                coeff_np,
                facecolor="red",
                zoomable=False,
                label="pred",
                **point_kwargs,
            )
            plot_translucent_point_group(
                scalar_invts_np,
                true_coeff_np,
                facecolor="blue",
                zoomable=False,
                label="true",
                **point_kwargs,
            )

        plt.title(title)
        plt.legend()

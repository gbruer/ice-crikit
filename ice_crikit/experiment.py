from crikit import *
from crikit.cr.space_builders import enlist


class Experiment:
    def __init__(
        self,
        mesh,
        alpha=0,
        g=9.8,
        rho=1,
        degree=2,
        constrained_domain=None,
        extras=(),
        xi=Constant(1),
    ):
        self.set_domain_parameters(
            mesh, degree=degree, constrained_domain=constrained_domain, extras=extras
        )
        self.set_f(alpha=alpha, g=g, rho=rho)
        self.xi = xi

    def set_f(self, alpha=None, g=None, rho=None):
        self.alpha = self.alpha if alpha is None else alpha
        self.g = self.g if g is None else g
        self.rho = self.rho if rho is None else rho
        f = (
            -self.rho * self.g * sin(self.alpha),
            -self.rho * self.g * cos(self.alpha),
            0,
        )
        if self.dim == 2:
            f = f[:2]
        self.f = Constant(f, name="f")

    def set_domain_parameters(self, mesh, degree=2, constrained_domain=None, extras=()):
        self.mesh = mesh
        self.degree = degree
        self.constrained_domain = constrained_domain
        self.extras = extras
        V_e = VectorElement("CG", self.mesh.ufl_cell(), self.degree)
        Q_e = FiniteElement("DG", self.mesh.ufl_cell(), self.degree - 2)
        all_e = MixedElement([V_e, Q_e] + Enlist(extras))
        self.W = FunctionSpace(self.mesh, all_e, constrained_domain=constrained_domain)
        self.dim = V_e.value_shape()[0]

    def set_quad_params(self, quad_params):
        self.quad_params = quad_params
        self.quad_degree = self.quad_params.get("quadrature_degree", None)

    def set_bcs(self, dir_bcs, rob_bcs=None, E_rob_bcs=None):
        """
        Sets Dirichlet and Robin boundary conditions. Each Robin condition is a
        tuple (a, b, j, ds) corresponding to the equation :math:`a u + b mu sym(grad(u)) n = j`
        on the boundary ds. Note that the Robin conditions are only applied
        to the velocity, not the pressure.

        Args:
            dir_bcs (DirichletBC or list[DirichletBC]): The Dirichlet boundary conditions.
            rob_bcs (tuple or list[tuple]): See description above.
        """
        self.bcs = Enlist(dir_bcs)
        self.h_bcs = homogenize_bcs(self.bcs)
        self.rob_bcs = Enlist(rob_bcs) if rob_bcs is not None else None
        self.E_rob_bcs = Enlist(E_rob_bcs) if E_rob_bcs is not None else None

    def get_robin_terms(self, u, v):
        """Adds the Robin terms to the given Form"""
        if self.rob_bcs is None:
            return 0
        F = 0
        for a, b, j, ds in self.rob_bcs:
            F += inner(v, (a * u - j)) / b * ds
        return F

    def run(
        self,
        cr,
        extra_inputs=(),
        observer=None,
        initial_w=None,
        ufl=False,
        solver_parameters=None,
        **kwargs,
    ):
        if solver_parameters is None:
            solver_parameters = {}

        # Define u.
        if initial_w is None:
            w = Function(self.W, name="w")
        else:
            w = initial_w

        if ufl:
            w = self._ufl_solve(cr, w, extra_inputs, **kwargs)
        else:
            w = self._solve(cr, w, extra_inputs, **kwargs)

        if observer is None:
            return w
        return observer(w)

    @staticmethod
    def get_cr_inputs(w, extra_inputs):
        u, p, *extra_unknowns = split(w)
        extra_inputs = Enlist(extra_inputs)

        strain_rate = sym(grad(u))
        if len(extra_inputs) == 0 and len(extra_unknowns) == 0:
            return strain_rate
        return [strain_rate] + extra_unknowns + extra_inputs

    def _ufl_solve(self, cr, w, extra_inputs, cback=None, disp=True, adj_cb=None):
        # Don't print out Newton iterations.
        if not disp:
            orig_log_level = get_log_level()
            set_log_level(logging.CRITICAL)

        # Set up the weak form.
        cr_inputs = self.get_cr_inputs(w, extra_inputs)
        cr_outputs = cr(cr_inputs)

        F = self.get_form(w, *Enlist(cr_outputs))

        solver_parameters = {
            "newton_solver": {
                "error_on_nonconvergence": False,
                "maximum_iterations": 30,
            }
        }
        solve_kwargs = dict(solver_parameters=solver_parameters)
        if annotate_tape():
            solve_kwargs["adj_cb"] = adj_cb
        solve(F == 0, w, self.bcs, **solve_kwargs)

        if not disp:
            set_log_level(orig_log_level)

        if cback is not None:
            cback(w)
        return w

    def _solve(self, cr, w, extra_inputs, cback=None, disp=True, adj_cb=None):
        if not disp:
            orig_log_level = get_log_level()
            set_log_level(logging.CRITICAL)

        # Set up the weak form.
        cr_inputs = self.get_cr_inputs(w, extra_inputs)
        F, *cr_outputs = self.get_form_cr(cr, w)

        with push_tape():
            residual = Function(self.W)
            assemble_with_cr(
                F,
                cr,
                cr_inputs,
                cr_outputs,
                tensor=residual,
                quad_params=self.quad_params,
            )

            wcontrol = Control(w)
            residual_rf = ReducedFunction(residual, wcontrol)
            self.residual = residual
            self.residual_tape = residual_rf.tape
        reduced_equation = ReducedEquation(residual_rf, self.bcs, self.h_bcs)

        solve_kwargs = dict(disp=disp, cback=cback)
        if annotate_tape():
            solve_kwargs["adj_cb"] = adj_cb
        solver = SNESSolver(reduced_equation, {"jmat_type": "assembled"})
        w = solver.solve(wcontrol, **solve_kwargs)

        if not disp:
            set_log_level(orig_log_level)
        return w

    def get_form_cr(self, cr, w):
        output_spaces = enlist(cr.target)
        target_shapes = []
        for space in output_spaces:
            target_shape = tuple(i for i in space.shape() if i != -1)
            target_shapes.append(target_shape)

        cr_output_standins = create_ufl_standins(target_shapes)

        F = self.get_form(w, *cr_output_standins)
        return (F, *cr_output_standins)

    def get_form(self, w, tau, *extra_cr_outputs):
        w_test = TestFunction(self.W)
        u, p, *extras = split(w)
        v, q, *extras_test = split(w_test)

        lhs = 0
        rhs = 0

        mdx = dx(
            metadata=dict(representation="uflacs", quadrature_degree=self.quad_degree)
        )
        if len(extras) > 0:
            if len(extras) != 1:
                raise ValueError(
                    f"Unexpected number of extra solution variables: {len(extras)}"
                )
            if len(extra_cr_outputs) != 1:
                raise ValueError(
                    f"Unexpected number of extra cr outputs: {len(extra_cr_outputs)}"
                )

            # extras[0] is phi, so add the equation for phi into the form.
            phi = extras[0]
            phi_test = extras_test[0]
            DphiDt = extra_cr_outputs[0]

            r = inner(u, grad(phi)) - self.xi * div(grad(phi)) - DphiDt  # PDE residual.

            # Add SUPG stabilization terms
            h = 2 * Circumradius(self.mesh)
            h2 = h ** 2
            unorm2 = inner(u, u)
            delta = 1 / (4.0 * unorm2 / h2 + (4.0 * self.xi / h2) ** 2) ** 0.5

            lhs += delta * inner(u, grad(phi_test)) * r * mdx

            lhs += (
                inner(u, grad(phi)) * phi_test
                + self.xi * inner(grad(phi), grad(phi_test))
            ) * mdx
            rhs += DphiDt * phi_test * mdx

        lhs += (
            inner(tau, grad(v)) * mdx - inner(div(v), p) * mdx - inner(div(u), q) * mdx
        )
        rhs += inner(self.f, v) * mdx

        F = lhs - rhs
        F += self.get_robin_terms(u, v)
        if len(extras) > 0:
            F += self.get_extra_terms(w, w_test)
        return F

    def get_extra_terms(self, w, w_test):
        """Adds the Robin terms to the given Form"""
        if self.E_rob_bcs is None:
            return 0
        u, p, *extras = split(w)
        v, q, *extras_test = split(w_test)
        F = 0

        phi = extras[0]
        phi_test = extras_test[0]
        for a, b, j, ds in self.E_rob_bcs:
            F += self.xi * inner(phi_test, (a * phi - j)) / b * ds
        return F

    @property
    def key(self):
        return (
            tuple(self.f.values()),
            (
                tuple(self.W.mesh().coordinates().flatten()),
                tuple(self.W.mesh().cells().flatten()),
            ),
            self.W.element().signature(),
            tuple(self.xi.values()),
            frozenset(self.quad_params.items()),
            tuple(frozenset(bc.get_boundary_values().items()) for bc in self.bcs),
        )

    def get_w_from_cache(self, cache, cr_cache_key, **ex_kwargs):
        cache_key = cr_cache_key + self.key
        filepath = cache.val.get(cache_key, None)
        if filepath is None:
            return None

        w = Function(self.W)
        subspaces = [f.function_space().collapse() for f in w.split()]
        w_subs = [Function(V) for V in subspaces]

        with XDMFFile(MPI.comm_world, filepath) as f:
            f.read_checkpoint(w_subs[0], "u", 0)
            f.read_checkpoint(w_subs[1], "p", 0)
            f.read_checkpoint(w_subs[2], "phi", 0)
            assert (
                len(w_subs) == 3
            ), f"Expected 3 functions in the file, but found {len(w_subs)}"

        fa = FunctionAssigner(self.W, subspaces)
        fa.assign(w, w_subs)
        return w

    def write_w_to_cache(self, cache, cr_cache_key, w, test=False, **ex_kwargs):
        import uuid
        import os

        # Generate file path.
        filename = str(uuid.uuid4())
        filepath = os.path.join("cache", f"{filename}.xdmf")

        # XDMFFile.write_checkpoint() can't handle a MixedElement, so we split the function into its components.
        subspaces = [f.function_space().collapse() for f in w.split()]
        w_subs = [Function(V) for V in subspaces]
        fa = FunctionAssigner(subspaces, self.W)
        fa.assign(w_subs, w)

        # Write the functions to the file.
        with XDMFFile(self.mesh.mpi_comm(), filepath) as f:
            f.write_checkpoint(w_subs[0], "u", 0, XDMFFile.Encoding.HDF5, append=False)
            f.write_checkpoint(w_subs[1], "p", 0, XDMFFile.Encoding.HDF5, append=True)
            f.write_checkpoint(w_subs[2], "phi", 0, XDMFFile.Encoding.HDF5, append=True)
            assert (
                len(w_subs) == 3
            ), f"Expected 3 subfunctions in w, but found {len(w_subs)}"

        # Save the file path in the cache.
        cache_key = cr_cache_key + self.key
        old_xdmf_name = cache.val.get(cache_key, None)
        cache.val[cache_key] = filepath
        cache.write()

        # Remove the previous filepath for this key if it was already in the cache.
        if old_xdmf_name is not None:
            try:
                os.remove(old_xdmf_name)
            except FileNotFoundError:
                pass
            old_hdf5_name = os.path.splitext(old_xdmf_name)[0] + ".h5"
            try:
                os.remove(old_hdf5_name)
            except FileNotFoundError:
                pass

        # Make sure that we can retrieve the function from the file.
        if test:
            w2 = self.get_w_from_cache(cache, cr_cache_key, **ex_kwargs)

            from numpy.testing import assert_allclose

            assert_allclose(w.vector()[:], w2.vector()[:])

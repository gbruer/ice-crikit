import tensorflow

import argparse
import alphashape
import dataclasses
import os
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import pathlib
import shapely
import sys
import warnings

from collections import defaultdict, namedtuple
from crikit import (
    Function,
    FunctionSpace,
    Mesh,
    XDMFFile,
    logging,
    plot,
    project,
    set_log_level,
    no_annotations,
    stop_annotating,
    set_working_tape,
    Tape,
    set_default_covering_params,
)
import meshio
import pygmsh
from pprint import pprint

sys.path.append(os.path.join(os.path.dirname(__file__),'../'))

from ice_crikit.cr_plotter import InvariantCRPlotter
from ice_crikit.cache import PickleDictionaryCache
from damage import (
    build_experiment_and_mesh,
    build_observer_and_loss,
    build_training_testing_functions,
    get_ground_truth_data,
    get_true_invariant_data,
    run_network_cr_batch,
    save_experiment_output,
)
from ice_crikit.hyper_param_utils import HyperParams
from ice_crikit.param_utils import ParamBag
from ice_crikit.plotting import plot_translucent_point_group, replot_translucent_point_group
from ice_crikit.run import Run

set_log_level(logging.CRITICAL)

INVARIANT_SPACE_ERROR_CMAP = "magma_r"
INVARIANT_SPACE_CMAP = "cividis"


def get_unique_stems(hyper_params, ignored_hyper_params=()):
    N = len(hyper_params)

    # Count how many instances there are of each unique param key-value.
    hyper_params_count = defaultdict(lambda: defaultdict(lambda: 0))
    ignored_hyper_params = ["directory", "seed"]
    for hp in hyper_params:
        hp_dict = hp.asdict()
        for ignored in ignored_hyper_params:
            hp_dict.pop(ignored, None)
        for name, val in hp_dict.items():
            hyper_params_count[name][val] += 1

    # See which hyper parameters are being modified by these runs.
    names_of_modified_params = set()
    common_params_dict = {}
    for name, val_count_dict in hyper_params_count.items():
        if len(val_count_dict) == 1:
            # This parameter is shared by everyone.
            val, count = val_count_dict.popitem()
            if count != N:
                print(
                    f"Uh oh: there's only one value for {param} but the count < N ({count} < {N})"
                )
                print(
                    "    This could mean a run didn't have this set, or a run incorrectly had this set."
                )
            common_params_dict[name] = val
        else:
            # This parameter is changed over the different runs.
            names_of_modified_params.add(name)

    hp_dict = {k: "uncommon" for k in HyperParams.__dataclass_fields__}
    hp_dict.update(common_params_dict)
    common_params = HyperParams(**hp_dict)
    return names_of_modified_params, common_params, common_params_dict


def name_generator(hp_dict):
    names = []
    if "surface" in hp_dict and "borehole" in hp_dict:
        s = hp_dict["surface"]
        b = hp_dict["borehole"]
        if s and b:
            names.append("surface+borehole")
        elif s and not b:
            names.append("surface")
        elif not s and not b:
            names.append("interior")
        elif not s and b:
            names.append("borehole")
        del hp_dict["surface"]
        del hp_dict["borehole"]
    if "noise" in hp_dict:
        n = hp_dict["noise"]

        def get_percent(n):
            if n is None or len(n) == 0:
                p = 0
                return f"{p:.0f}%"
            elif isinstance(n[0], (int, float)):
                p = n[0] * 100
                return f"{p:.0f}%"
            else:
                return f'({", ".join(get_percent(ni) for ni in n)})'

        names.append(get_percent(n))
        if len(hp_dict) != 1:
            names[-1] = f"noise:{names[-1]}"
        del hp_dict["noise"]
    names += [f"{k}:{hp_dict[k]}" for k in sorted(hp_dict.keys())]
    return " ".join(names)


def log_update_ticks(x, pos):
    return f"$10^{{{x}}}$"


def boxplot_log(x, *args, **kwargs):
    if isinstance(x, np.ndarray):
        logx = np.log10(x)
    else:
        logx = [np.log10(xi) for xi in x]
    plt.boxplot(logx, *args, **kwargs)
    ax = plt.gca()
    if kwargs.get("vert", True):
        data_axis = ax.yaxis
    else:
        data_axis = ax.xaxis
    data_axis.set_major_formatter(mticker.FuncFormatter(log_update_ticks))


def escape_filename(s):
    return s.replace(":", "=").replace(".", "_")


def escape_latex(s):
    return s.replace("_", r"\_").replace("%", r"\%")


AggregateErrorInfo = namedtuple("AggregateErrorInfo", ["mean_diff", "rmse", "stddev"])


def save_aggregate_error_infos(name, labels, infos):
    """Saves a bunch of AggregateErrorInfo functions assuming they all use the same mesh"""
    filename = f"{name}.xdmf"
    mesh = infos[0].rmse.function_space().mesh()
    comm = mesh.mpi_comm()

    # Write the functions to the file.
    with XDMFFile(comm, filename) as f:
        f.write(mesh)
        for label, info in zip(labels, infos):
            f.write_checkpoint(
                info.rmse, f"{label}_rmse", 0, XDMFFile.Encoding.HDF5, append=True
            )
            f.write_checkpoint(
                info.mean_diff,
                f"{label}_mean_diff",
                0,
                XDMFFile.Encoding.HDF5,
                append=True,
            )
            f.write_checkpoint(
                info.stddev, f"{label}_stddev", 0, XDMFFile.Encoding.HDF5, append=True
            )


def get_aggregate_error_info(data_np, true_np, V):
    diff_np = data_np - true_np
    err_np = diff_np ** 2

    mean_diff = Function(V)
    mean_diff_np = diff_np.mean(axis=0)
    mean_diff.vector().set_local(mean_diff_np)

    rmse = Function(V)
    rmse_np = np.sqrt(err_np.mean(axis=0))
    rmse.vector().set_local(rmse_np)

    stddev = Function(V)
    stddev_np = data_np.std(axis=0)
    stddev.vector().set_local(stddev_np)

    info = AggregateErrorInfo(mean_diff=mean_diff, rmse=rmse, stddev=stddev)

    V_smooth = FunctionSpace(V.mesh(), "P", 1)
    smooth_dict = {
        f: project(getattr(info, f), V_smooth) for f in AggregateErrorInfo._fields
    }
    smooth_info = AggregateErrorInfo(**smooth_dict)
    return info, smooth_info


def get_invariant_space_hull(cr_plotter, true_data, hull_alpha, corner=None):
    filename = pathlib.Path(f"{cr_plotter.name}_hull.xdmf")
    if True or not filename.exists():
        # Mask out the invts that aren't in the cr_plotter range.
        invts = true_data.scalar_invts_quad_xy_np[:, cr_plotter.invariant_idxs]
        mask_xy = invts[:, 0] >= cr_plotter.x_range[0]
        mask_xy &= invts[:, 0] <= cr_plotter.x_range[1]
        mask_xy &= invts[:, 1] >= cr_plotter.y_range[0]
        mask_xy &= invts[:, 1] <= cr_plotter.y_range[1]

        # These are the physical coordinates of the invariants in invts_masked.
        # Let's assume the quadrature points are the same for each dof.
        xy_coords = cr_plotter.coeff_space_xy_quad.tabulate_dof_coordinates()
        if corner is not None:
            mask_corner = (xy_coords[:, 0] > corner[0]) & (xy_coords[:, 1] < corner[1])
            mask_xy &= ~mask_corner

        invts_masked = invts[mask_xy]

        scales = np.max(invts_masked, axis=0)
        invts_masked_scaled = invts_masked / scales
        if callable(hull_alpha):
            orig_hull_alpha = hull_alpha
            hull_alpha = lambda ind, r: orig_hull_alpha(invts_masked_scaled, ind, r)
        scaled_hull = alphashape.alphashape(invts_masked_scaled, alpha=hull_alpha)
        buffered_scaled_hull = scaled_hull.envelope.intersection(
            scaled_hull.buffer(0.05)
        )

        hull = shapely.affinity.scale(
            buffered_scaled_hull, scales[0], scales[1], origin=(0, 0)
        )

        # Build a mesh for this hull.
        with pygmsh.geo.Geometry() as geom:
            geom.add_polygon(list(buffered_scaled_hull.exterior.coords), mesh_size=0.02)
            gmsh_mesh = geom.generate_mesh(dim=2)

        # Extract triangles.
        for cell in gmsh_mesh.cells:
            if cell.type == "triangle":
                gmsh_mesh.cells = [cell]
                break
        else:
            raise ValueError("Couldn't find triangle cells")

        # Remove bad triangles.
        def triangle_area(x, y):
            dy12 = y[1] - y[2]
            dy20 = y[2] - y[0]
            dy01 = y[0] - y[1]
            area = 0.5 * ((x[0] * dy12) + (x[1] * dy20) + (x[2] * dy01))
            return abs(area)

        triangles = gmsh_mesh.cells[0]
        idxs_all = triangles.data
        points_all = gmsh_mesh.points
        mask = []
        for i, idxs in enumerate(triangles.data):
            points = points_all[idxs]
            area = triangle_area(points[:, 0], points[:, 1])
            mask.append(area > 1e-10)

        # Remove bad triangles.
        new_triangles = triangles.data[mask]

        # Check for unused vertices.
        vertices_mask = np.full(len(points_all), True)
        vertices_mask[new_triangles.flatten()] = False
        if vertices_mask.sum() != 0:
            # Remove unused vertices and fix triangle vertices.
            points_all = points_all[~vertices_mask]
            removed_vertices_counter = np.cumsum(vertices_mask.astype(np.uint64))
            new_triangles -= removed_vertices_counter[new_triangles]

        # Truncate to 2D and scale back to original dimensions.
        points = points_all[:, :2] * scales

        cells = [("triangle", new_triangles)]
        gmsh_mesh_fixed = meshio.Mesh(points, cells)

        meshio.write(filename, gmsh_mesh_fixed)

        fenics_mesh = Mesh()
        with XDMFFile(str(filename)) as infile:
            infile.read(fenics_mesh)

        old_fig = plt.gcf()

        plt.figure()
        plot_filename = pathlib.Path(f"{cr_plotter.name}_hull.png")
        plot(fenics_mesh)
        ax = plt.gca()
        ax.set_aspect(1.0 / ax.get_data_ratio(), adjustable="datalim")
        plt.savefig(plot_filename)
        plt.close()

        plt.figure(old_fig)

    else:
        fenics_mesh = Mesh()
        with XDMFFile(str(filename)) as infile:
            infile.read(fenics_mesh)

    return fenics_mesh


def get_quadrature_dx(el, dx):
    if el.family() == "Quadrature":
        quad_params = {
            "quadrature_rule": el.quadrature_scheme(),
            "quadrature_degree": el.degree(),
            "representation": "quadrature",
        }
        dx = dx(metadata=quad_params)
    return dx


MINT_BLUE = "#1b9e77"
ORANGE = "#d95f02"
PURPLE = "#7570b3"
PINK = "#e7298a"
MOSS_GREEN = "#66a61e"
WHITE = "white"
GREY = "gainsboro"

point_kwargs_all = dict(facecolor=WHITE)
point_kwargs_left = dict(facecolor=MOSS_GREEN)
point_kwargs_right = dict(facecolor=PURPLE)
point_kwargs_close = dict(facecolor=GREY)
point_kwargs_surface = dict(facecolor=MINT_BLUE)


def compute_invt_patches(
    hyper_params, cr_plotter, true_data, right_color=None, corner=None
):
    group_kwargs = dict(
        zoomable=False,
        label="Experiment Invariants",
        lw=0,
        alpha=0.4,
    )
    if right_color is not None:
        right_kwargs = point_kwargs_right.copy()
        right_kwargs["facecolor"] = right_color
    else:
        right_kwargs = point_kwargs_right

    # Mask out the invts that aren't in the cr_plotter range.
    invts = true_data.scalar_invts_quad_xy_np[:, cr_plotter.invariant_idxs]
    mask_xy = invts[:, 0] >= cr_plotter.x_range[0]
    mask_xy &= invts[:, 0] <= cr_plotter.x_range[1]
    mask_xy &= invts[:, 1] >= cr_plotter.y_range[0]
    mask_xy &= invts[:, 1] <= cr_plotter.y_range[1]

    # These are the physical coordinates of the invariants in invts_masked.
    # Let's assume the quadrature points are the same for each dof.
    xy_coords = cr_plotter.coeff_space_xy_quad.tabulate_dof_coordinates()
    if corner is not None:
        mask_corner = (xy_coords[:, 0] > corner[0]) & (xy_coords[:, 1] < corner[1])
        mask_xy &= ~mask_corner

    invts_masked = invts[mask_xy]

    patches = plot_translucent_point_group(
        invts_masked[:, 0],
        invts_masked[:, 1],
        **group_kwargs,
        **point_kwargs_all,
    )
    cr_plotter.patches_invts_full = patches

    # I want to get the ones to the left of the borehole.
    bx = hyper_params.borehole_x
    true_data.xy_coords_masked = xy_coords[mask_xy]
    true_data.mask_left = true_data.xy_coords_masked[:, 0] < bx - 0.05
    true_data.mask_right = true_data.xy_coords_masked[:, 0] > bx + 0.05
    true_data.mask_close = np.abs(true_data.xy_coords_masked[:, 0] - bx) <= 0.05

    R = hyper_params.h / 2 + hyper_params.L ** 2 / (2 * hyper_params.h)
    cy = hyper_params.H - np.sqrt(R ** 2 - hyper_params.L ** 2)
    top_surface_y = cy + np.sqrt(R ** 2 - true_data.xy_coords_masked[:, 0] ** 2)
    true_data.mask_surface = (
        np.abs(true_data.xy_coords_masked[:, 1] - top_surface_y) < 0.01
    )

    invts_left = invts_masked[true_data.mask_left]
    invts_right = invts_masked[true_data.mask_right]
    invts_close = invts_masked[true_data.mask_close]
    invts_surface = invts_masked[true_data.mask_surface]

    if invts_left.size != 0:
        patches = plot_translucent_point_group(
            invts_left[:, 0],
            invts_left[:, 1],
            **group_kwargs,
            **point_kwargs_left,
        )
    else:
        patches = None
    cr_plotter.patches_invts_left = patches

    if invts_right.size != 0:
        patches = plot_translucent_point_group(
            invts_right[:, 0],
            invts_right[:, 1],
            **group_kwargs,
            **right_kwargs,
        )
    else:
        patches = None
    cr_plotter.patches_invts_right = patches

    if invts_close.size != 0:
        patches = plot_translucent_point_group(
            invts_close[:, 0],
            invts_close[:, 1],
            **group_kwargs,
            **point_kwargs_close,
        )
    else:
        patches = None
    cr_plotter.patches_invts_close = patches

    if invts_surface.size != 0:
        patches = plot_translucent_point_group(
            invts_surface[:, 0],
            invts_surface[:, 1],
            **group_kwargs,
            **point_kwargs_surface,
        )
    else:
        patches = None
    cr_plotter.patches_invts_surface = patches

    return xy_coords, invts, mask_xy


def plot_xy_space(cr_plotter, true_data, right_color, alpha=0.4, size=2):
    group_kwargs = dict(
        zoomable=False,
        lw=0,
        size=size,
        alpha=alpha,
    )

    if right_color is not None:
        right_kwargs = point_kwargs_right.copy()
        right_kwargs["facecolor"] = right_color
    else:
        right_kwargs = point_kwargs_right

    xy_coords_left = true_data.xy_coords_masked[true_data.mask_left]
    xy_coords_right = true_data.xy_coords_masked[true_data.mask_right]
    xy_coords_close = true_data.xy_coords_masked[true_data.mask_close]
    xy_coords_surface = true_data.xy_coords_masked[true_data.mask_surface]

    if xy_coords_left.size != 0:
        patches = plot_translucent_point_group(
            xy_coords_left[:, 0],
            xy_coords_left[:, 1],
            label="left of borehole",
            **group_kwargs,
            **point_kwargs_left,
        )
    else:
        patches = None
    cr_plotter.patches_xy_coords_left = patches

    if xy_coords_right.size != 0:
        patches = plot_translucent_point_group(
            xy_coords_right[:, 0],
            xy_coords_right[:, 1],
            label="right of borehole",
            **group_kwargs,
            **right_kwargs,
        )
    else:
        patches = None
    cr_plotter.patches_xy_coords_right = patches

    if xy_coords_close.size != 0:
        patches = plot_translucent_point_group(
            xy_coords_close[:, 0],
            xy_coords_close[:, 1],
            label="borehole",
            **group_kwargs,
            **point_kwargs_close,
        )
    else:
        patches = None
    cr_plotter.patches_xy_coords_close = patches

    if xy_coords_surface.size != 0:
        patches = plot_translucent_point_group(
            xy_coords_surface[:, 0],
            xy_coords_surface[:, 1],
            label="surface",
            **group_kwargs,
            **point_kwargs_surface,
        )
    else:
        patches = None
    cr_plotter.patches_xy_coords_surface = patches


def do_cr_plotter_plots(
    cr_plotter,
    f,
    title="",
    borehole=False,
    surface=False,
    nrows=1,
    ncols=2,
    do_colorbar=True,
    boundaries=None,
    **plot_kwargs,
):
    m = plot(f, **plot_kwargs, vmin=0)
    if do_colorbar:
        if boundaries is not None:
            c_kwargs = dict(boundaries=boundaries)
        else:
            c_kwargs = {}
        plt.colorbar(m, **c_kwargs)
    plt.xlim(cr_plotter.x_range)
    plt.ylim(cr_plotter.y_range)
    plt.xlabel(cr_plotter.invariant_names[cr_plotter.invariant_idxs[0]])

    ax = plt.gca()
    ax.set_aspect(1.0 / ax.get_data_ratio(), adjustable="datalim")

    if True:
        group_kwargs = dict(
            zoomable=False,
            label="Experiment Invariants",
            lw=0,
            alpha=0.4,
        )
        if cr_plotter.patches_invts_full is not None:
            patches = replot_translucent_point_group(
                cr_plotter.patches_invts_full,
                **group_kwargs,
                **point_kwargs_all,
            )

    if borehole:
        if cr_plotter.patches_invts_left is not None:
            patches = replot_translucent_point_group(
                cr_plotter.patches_invts_left,
                **group_kwargs,
                **point_kwargs_left,
            )
        if cr_plotter.patches_invts_right is not None:
            patches = replot_translucent_point_group(
                cr_plotter.patches_invts_right,
                **group_kwargs,
                **point_kwargs_right,
            )
        if cr_plotter.patches_invts_close is not None:
            patches = replot_translucent_point_group(
                cr_plotter.patches_invts_close,
                **group_kwargs,
                **point_kwargs_close,
            )
    if surface:
        if cr_plotter.patches_invts_surface is not None:
            patches = replot_translucent_point_group(
                cr_plotter.patches_invts_surface,
                **group_kwargs,
                **point_kwargs_surface,
            )

        # plt.legend()
    if title != "":
        plt.title(title)

    return m


class JobAnalyzer:
    def __init__(self, stem):
        self.runs = []
        self.completed_runs = []
        self.stem = stem
        self.figs_root = pathlib.Path("figs")

    def add_directory(self, dir_string):
        run = Run.init_from_directory(dir_string)
        if run.hyper_params is None:
            warnings.warn(
                f"Ignoring since run doesn't even have hyper parameters:{dir_string}.",
                stacklevel=2,
            )
            return
        self.runs.append(run)
        if run.is_complete:
            self.completed_runs.append(run)

    def get_unique_stems(self):
        # First, got to get all unique hyper params, ignoring seed.
        # Then, include the seed and remove duplicate seeds for unique sets of hyper params.
        file_common = pathlib.Path(f"{self.stem}-common.txt")
        file_data = pathlib.Path(f"{self.stem}-data.txt")
        file_stems = pathlib.Path(f"{self.stem}-stems.txt")
        file_stem_params = pathlib.Path(f"{self.stem}-stem-params.txt")
        file_incomplete = pathlib.Path(f"{self.stem}-incomplete.txt")
        file_extra = pathlib.Path(f"{self.stem}-extra.txt")
        folder_missing = pathlib.Path(f"{self.stem}-missing")

        def complete_filter(run):
            # return run.is_complete and run.hyper_params_data.finally_solvable
            return run.is_complete

        def progress_filter(hpd):
            if isinstance(hpd, Run):
                hpd = hpd.hyper_params_data
            return (
                np.abs(1 - hpd.loss_final / hpd.loss_initial)
                >= 1e-14
            )

        complete_runs = [run for run in self.runs if complete_filter(run)]

        def print_split_info(
            runs, categories, category_splitter_func, prop_splitter_func
        ):
            for category in categories:
                category_runs = [
                    run for run in runs if category_splitter_func(category, run)
                ]
                n_cat = len(category_runs)
                if n_cat == 0:
                    n_cat_prop = 0
                    p = 0
                else:
                    prop_runs = [
                        run for run in category_runs if prop_splitter_func(run)
                    ]
                    n_cat_prop = len(prop_runs)
                    p = n_cat_prop / n_cat

                print(f"  {category:20s}: {n_cat_prop:3d} / {n_cat:3d} == {p:.3f}")
            print()

        opt_categories = ["L-BFGS-B", "trust-constr"]

        def opt_splitter_func(category, run):
            return run.hyper_params.optimizer == category

        act_categories = ["relu", "tanh", "softplus"]

        def act_splitter_func(category, run):
            return run.hyper_params.activation == category

        def line_search_filter(run):
            return run.stdout_info.network_ls_tries > 0

        # Print how many of each different optimizer and activation made progress.
        print("Computing which runs made any sort of progress")

        print_split_info(
            complete_runs, opt_categories, opt_splitter_func, progress_filter
        )
        print_split_info(
            complete_runs, act_categories, act_splitter_func, progress_filter
        )

        # Print how many of each different optimizer and activation had to do a line search.
        print(
            "Of the runs that made progress, how many had to do a line search at the beginning?"
        )
        progress_runs = [run for run in complete_runs if progress_filter(run)]
        print_split_info(
            progress_runs, opt_categories, opt_splitter_func, line_search_filter
        )
        print_split_info(
            progress_runs, act_categories, act_splitter_func, line_search_filter
        )

        print()
        print(
            "Of the runs that made *no* progress, how many had to do a line search at the beginning?"
        )
        not_progress_runs = [run for run in complete_runs if not progress_filter(run)]
        print_split_info(
            not_progress_runs, opt_categories, opt_splitter_func, line_search_filter
        )
        print_split_info(
            not_progress_runs, act_categories, act_splitter_func, line_search_filter
        )

        print()
        print("Of the runs that had to do a line search, how many made progress?")
        line_search_runs = [run for run in complete_runs if line_search_filter(run)]
        print_split_info(
            line_search_runs, opt_categories, opt_splitter_func, progress_filter
        )
        print_split_info(
            line_search_runs, act_categories, act_splitter_func, progress_filter
        )

        print()
        print(
            "Of the runs that didn't have to do a line search, how many made progress?"
        )
        not_line_search_runs = [
            run for run in complete_runs if not line_search_filter(run)
        ]
        print_split_info(
            not_line_search_runs, opt_categories, opt_splitter_func, progress_filter
        )
        print_split_info(
            not_line_search_runs, act_categories, act_splitter_func, progress_filter
        )

        print("Computing unique stems")

        # Record which runs are worth analyzing.
        def good_filter(run):
            return (
                complete_filter(run)
                # and run.hyper_params.optimizer != "trust-constr"
                # and run.hyper_params.activation != "relu"
                and progress_filter(run)
            )

        self.good_runs = [run for run in self.runs if good_filter(run)]

        self.completed_runs = list(self.good_runs)

        N = len(self.good_runs)

        (
            self.names_of_modified_params,
            self.common_params,
            common_params_dict,
        ) = get_unique_stems(
            [run.hyper_params for run in self.good_runs],
            ignored_hyper_params=("directory", "seed"),
        )

        with file_common.open("w") as f:
            pprint(common_params_dict, stream=f)

        # Record the important hyper parameters for each run as a label.
        for run in self.good_runs:
            hyper_params_dict = run.hyper_params.asdict()
            run_stems = []
            for name in self.names_of_modified_params:
                run_stems.append((name, getattr(run.hyper_params, name)))
            if len(run_stems) == 0:
                run.label = "X"
            else:
                run_stems = sorted(run_stems)
                name_val_strings = [f"{name}:{val}" for name, val in run_stems]
                run.label = " ".join(name_val_strings)

        # Record which runs completed and which didn't.
        self.stems_completed_runs = defaultdict(list)
        self.stems_incompleted_runs = defaultdict(list)
        incomplete_runs = False
        for run in self.good_runs:
            if run.is_complete:
                self.stems_completed_runs[run.label].append(run)
                len(self.stems_incompleted_runs[run.label])
            else:
                len(self.stems_completed_runs[run.label])
                self.stems_incompleted_runs[run.label].append(run)

        self.stems_good_runs = defaultdict(list)
        for run in self.good_runs:
            self.stems_good_runs[run.label].append(run)

        # Build the HyperParams object for each label.
        self.stem_params = {}
        for stem in self.stems_completed_runs:
            if len(self.stems_completed_runs[stem]) > 0:
                run = self.stems_completed_runs[stem][0]
            else:
                run = self.stems_incompleted_runs[stem][0]
            # hp_dict = {k: "uncommon" for k in HyperParams.__dataclass_fields__}
            modified_key_vals = {
                k: getattr(run.hyper_params, k) for k in self.names_of_modified_params
            }
            self.stem_params[stem] = dataclasses.replace(
                self.common_params, **modified_key_vals
            )

        # Write stem params to a file.
        stem_params_dict = {k: v.asdict() for k, v in self.stem_params.items()}
        with file_stem_params.open("w") as f:
            pprint(stem_params_dict, stream=f)

        # Sort the runs for each label by the seed.
        self.stems_good_runs_seeds = {}
        for label, runs in self.stems_good_runs.items():
            runs = sorted(runs, key=lambda r: r.hyper_params.seed)
            self.stems_good_runs[label] = runs
            seeds = set(r.hyper_params.seed for r in runs)
            self.stems_good_runs_seeds[label] = seeds

        all_seeds = {
            909468,
            909469,
            909470,
            909471,
            909472,
            909473,
            909474,
            909475,
            909476,
            909477,
        }
        # all_seeds = None
        if all_seeds is None:
            # Figure out what all seeds were used.
            all_seeds = set()
            for label, seeds in self.stems_good_runs_seeds.items():
                all_seeds |= seeds
        all_seeds_list = sorted(all_seeds)

        print(f"all_seeds ({len(all_seeds)}):")
        for i, s in enumerate(all_seeds_list):
            print(f"{i:2d} {s:14d}")

        # Save more information about specific seeds.
        self.stems_good_runs_seed_lookup = {}
        for label, seeds in self.stems_good_runs_seeds.items():
            runs = self.stems_good_runs[label]
            seed_lookup = defaultdict(list)
            for run in runs:
                seed_lookup[run.hyper_params.seed].append(run)
            self.stems_good_runs_seed_lookup[label] = seed_lookup

        # For each stem, print out which seeds are missing and which is extra.
        missing_hps = []
        extra_runs = []
        for label, seeds in self.stems_good_runs_seeds.items():
            print(f"{label:150s} -- ", end="")
            missing = all_seeds - seeds
            extra = seeds - all_seeds
            if len(missing) == 0 and len(extra) == 0:
                print("perfect", end="")
            else:
                if len(missing) != 0:
                    print(
                        " missing", " ".join([str(m) for m in sorted(missing)]), end=""
                    )
                    for m in missing:
                        hp = dataclasses.replace(
                            self.stem_params[label], seed=m, directory=""
                        )
                        missing_hps.append(hp)
                if len(extra) != 0:
                    print(" extra", " ".join([str(m) for m in sorted(extra)]), end="")
                    seed_lookup = self.stems_good_runs_seed_lookup[label]
                    for m in extra:
                        runs = seed_lookup[m]
                        extra_runs += list(runs)

            if len(seeds) != len(self.stems_good_runs[label]):
                seeds_list = [r.hyper_params.seed for r in self.stems_good_runs[label]]
                print(f"has duplicates: {seeds_list}", end="")
            print()

        if len(missing_hps) > 0:
            folder_missing.mkdir(parents=True, exist_ok=True)
            for i, hp in enumerate(missing_hps):
                file_missing = folder_missing / f"{i}.txt"
                with file_missing.open("w") as f:
                    pprint(hp.asdict(), stream=f)
        print(
            f"Wrote {len(missing_hps)} missing hyper parameter sets to {folder_missing}"
        )

        if len(extra_runs) > 0:
            with file_extra.open("w") as f:
                for i, run in enumerate(extra_runs):
                    f.write(f"{run.directory.name}\n")
        else:
            if file_extra.exists():
                file_extra.unlink()
        print(f"Wrote {len(extra_runs)} extra directory names to {file_extra}")

        def compare_groups(attr_name, attr_vals, better):
            def group1(hp):
                return getattr(hp, attr_name) == attr_vals[0]

            def group2(hp):
                return getattr(hp, attr_name) == attr_vals[1]

            # Get all the stems that are in the first group.
            labels_1 = sorted(
                [label for label, hp in self.stem_params.items() if group1(hp)]
            )
            labels_2 = sorted(
                [label for label, hp in self.stem_params.items() if group2(hp)]
            )

            # Error check the groups.
            for label_1, label_2 in zip(labels_1, labels_2):
                hp_1 = self.stem_params[label_1]
                hp_2 = self.stem_params[label_2]

                # Make sure these only differ in attr_name.
                if hp_1 != dataclasses.replace(hp_2, **{attr_name: attr_vals[0]}):
                    raise ValueError("Groups don't correspond. See output.")

            # Compare the desired value seed by seed.
            count_1_better = 0
            count_total = 0
            for label_1, label_2 in zip(labels_1, labels_2):
                hp_1 = self.stem_params[label_1]
                hp_2 = self.stem_params[label_2]

                for s in all_seeds:
                    run_1 = self.stems_good_runs_seed_lookup[label_1][s][0]
                    run_2 = self.stems_good_runs_seed_lookup[label_2][s][0]

                    hpd_1 = run_1.hyper_params_data
                    hpd_2 = run_2.hyper_params_data

                    count_1_better += int(better(hpd_1, hpd_2))
                    count_total += 1
            return count_1_better, count_total

        def better(hpd_1, hpd_2):
            return progress_filter(hpd_1) and (
                not progress_filter(hpd_2)
                or getattr(hpd_1, comp_attr) < getattr(hpd_2, comp_attr)
            )

        do_comparison = False
        if do_comparison:
            comp_attrs = [
                "loss_initial",
                "loss_alg_initial",
                "loss_final",
                "loss_alg_final",
                "loss_final_gradient",
                "loss_alg_final_gradient",
            ]

            split_attr = "activation"
            split_vals = ["relu", "softplus"]
            print(f"{split_attr} -- {split_vals[0]} better than {split_vals[1]}:")
            for comp_attr in comp_attrs:
                count_better, count_total = compare_groups(
                    split_attr, split_vals, better
                )
                print(f"  {comp_attr:23s}: {count_better} / {count_total}")
            print()

            split_attr = "optimizer"
            split_vals = ["L-BFGS-B", "trust-constr"]
            print(f"{split_attr} -- {split_vals[0]} better than {split_vals[1]}:")
            for comp_attr in comp_attrs:
                count_better, count_total = compare_groups(
                    split_attr, split_vals, better
                )
                print(f"  {comp_attr:23s}: {count_better} / {count_total}")
            print()

        # Print out more information about specific seeds.
        print()
        for label, seeds in self.stems_good_runs_seeds.items():
            runs = self.stems_good_runs[label]
            seed_lookup = defaultdict(list)
            for run in runs:
                seed_lookup[run.hyper_params.seed].append(run)

            print(f"{label:100s}")
            for i, s in enumerate(all_seeds_list):
                print(f"  {i:2d} {s:14d}  ", end="")
                if s in seeds:
                    for run in seed_lookup[s]:
                        print(run.directory, " ", end="")
                else:
                    print("missing", end="")
                print()
            print()

        # Sort the stems in some desired order.
        def stem_sort_key(stem):
            hp = self.stem_params[stem]
            return (
                hp.noise,
                hp.borehole,
                hp.surface,
                hp.layer_sizes,
                hp.activation,
                hp.optimizer,
                hp,
            )

        self.idx_to_stem_good = sorted(self.stems_good_runs.keys(), key=stem_sort_key)
        self.idx_to_stem = sorted(self.stems_completed_runs.keys(), key=stem_sort_key)

        # Write stems to a file.
        with file_stems.open("w") as f:
            for i, label in enumerate(self.idx_to_stem):
                f.write(f"{i} {label}\n")

        self.stem_to_idx = {}
        for i, label in enumerate(self.idx_to_stem):
            self.stem_to_idx[label] = i

        def get_run_data_str(run):
            idx = self.stem_to_idx[run.label]
            d = run.hyper_params_data
            hp = run.hyper_params
            data = (
                run.directory.name,
                None if hp is None else hp.seed,
                -1 if run.stdout_info is None else run.stdout_info.duration,
                idx,
                None if d is None else d.loss_final,
                None if d is None else d.loss_final_gradient,
                None if d is None else d.loss_initial,
                None if d is None else d.loss_initial_gradient,
                None if d is None else d.optimizer_iterations,
                None if d is None else d.num_trainable_params,
            )
            data_str = " ".join([str(val) for val in data])
            return data_str

        # Record data.
        with file_data.open("w") as f:
            for run in self.completed_runs:
                data_str = get_run_data_str(run)
                f.write(data_str + "\n")

        if len(self.completed_runs) != len(self.good_runs):
            # Record incomplete runs.
            with file_incomplete.open("w") as f:
                for run in self.good_runs:
                    if run.is_complete:
                        continue
                    data_str = get_run_data_str(run)
                    f.write(data_str + "\n")
        else:
            if file_incomplete.exists():
                file_incomplete.unlink()

        # Print number of complete and incomplete runs for each label.
        print(f"{'Complete':10s} {'Incomplete':10s} {'label':10s}")
        c_count = len(self.completed_runs)
        inc_count = len(self.good_runs) - len(self.completed_runs)
        label = "total"
        print(f"{c_count:10d} {inc_count:10d} {label}")
        for label in self.stems_completed_runs:
            c_count = len(self.stems_completed_runs[label])
            inc_count = len(self.stems_incompleted_runs[label])
            print(f"{c_count:10d} {inc_count:10d} {label}")

        aggregate_stem_data_spec = {
            "Solvable": lambda runs: sum(
                1 for r in runs if r.hyper_params_data.initially_solvable
            ),
            "Unsolvable": lambda runs: sum(
                1 for r in runs if not r.hyper_params_data.initially_solvable
            ),
        }

        def print_aggregate_stem_data(data_spec):
            header = " ".join(f"{k:10s}" for k in data_spec.keys())
            header += f" {'label':10s}"
            print(header)

            def print_data(label, runs):
                data = [f(runs) for k, f in data_spec.items()]
                data_str = " ".join(f"{d:10d}" for d in data)
                print(data_str, label)

            print_data("total", self.completed_runs)
            for label in self.stems_completed_runs:
                print_data(label, self.stems_completed_runs[label])

        print_aggregate_stem_data(aggregate_stem_data_spec)

        def get_run_data_str(run):
            hp = run.hyper_params
            data = (
                run.directory.name,
                hp.seed,
                run.label,
            )
            data_str = " ".join([str(val) for val in data])
            return data_str

        # Record incomplete runs.
        if len(self.binit):
            file_init_unsolvable = pathlib.Path(f"{self.stem}-init-unsolvable.txt")
            with file_init_unsolvable.open("w") as f:
                for run in self.binit:
                    data_str = get_run_data_str(run)
                    f.write(data_str + "\n")

    def tukey_plots_exp_losses(self, stems, rel_dir="."):
        # We're doing box plots, so we shouldn't plot stems with very few runs.
        stems = [stem for stem in stems if len(self.stems_good_runs[stem]) > 1]
        if len(stems) == 0:
            warnings.warn("No stems to plot", stacklevel=2)
            return

        def boxplot_getter(getter, plotter=plt.boxplot, **plot_kwargs):
            all_data = []
            all_labels = []
            all_n_data = []

            for stem in stems:
                runs_list = self.stems_good_runs[stem]
                n_runs = len(runs_list)
                # Should have shape (n_runs,) or (n_runs, n_exps, n_losses_per_experiment)
                # I'm gonna assume n_exps is one here, so I'll go ahead and index into it.
                stem_data = np.array([getter(r)[0] for r in runs_list])
                escaped_stem = escape_latex(stem)
                first_plot_label = f"{escaped_stem}  (N={n_runs})"
                all_labels.append(first_plot_label)
                all_data.append(stem_data[:, 0])
                for i in range(1, stem_data.shape[1]):
                    plot_label = f"{i}"
                    all_labels.append(plot_label)
                    all_data.append(stem_data[:, i])
                all_n_data.append(stem_data.shape[1])

            plotter(all_data, labels=all_labels, vert=False, **plot_kwargs)

            # Draw a line after every stem's data.
            x = plt.xlim()
            n_total = 0
            for i, n in enumerate(all_n_data[:-1]):
                n_total = n_total + n
                yi = n_total + 0.5
                plt.plot(x, [yi, yi], "y-")

            ax = plt.gca()
            ax.invert_yaxis()
            ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

            return len(all_data)

        savedir = self.figs_root / "tukey_exp" / rel_dir
        savedir.mkdir(parents=True, exist_ok=True)

        common_kwargs = dict()

        # Plot initial losses.
        fig = plt.figure()
        getter = lambda r: r.hyper_params_data.loss_initial_exp
        n = boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        fig.set_size_inches(16, 1.5 + 0.5 * n)
        plt.title("Initial losses")
        plt.savefig(savedir / "loss_initial_exp")
        plt.close()

        # Plot initial loss gradients.
        fig = plt.figure()
        getter = lambda r: r.hyper_params_data.loss_initial_exp_gradient
        n = boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        fig.set_size_inches(16, 1.5 + 0.5 * n)
        plt.title("Initial loss gradients")
        plt.savefig(savedir / "loss_initial_exp_gradient")
        plt.close()

        # Plot final losses.
        fig = plt.figure()
        getter = lambda r: r.hyper_params_data.loss_final_exp
        n = boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        fig.set_size_inches(16, 1.5 + 0.5 * n)
        plt.title("Final losses")
        plt.savefig(savedir / "loss_final_exp")
        plt.close()

        # Plot final loss gradients.
        fig = plt.figure()
        getter = lambda r: r.hyper_params_data.loss_final_exp_gradient
        n = boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        fig.set_size_inches(16, 1.5 + 0.5 * n)
        plt.title("Final loss gradients")
        plt.savefig(savedir / "loss_final_exp_gradient")
        plt.close()

        # Plot alg vs recomputed losses.
        fig = plt.figure()

        def getter(r):
            hpd = r.hyper_params_data
            return (
                (
                    hpd.loss_alg_initial,
                    hpd.loss_initial,
                    hpd.loss_alg_final,
                    hpd.loss_final,
                ),
            )

        n = boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        fig.set_size_inches(16, 1.5 + 0.5 * n)
        plt.title("Orig loss vs recomputed loss")
        plt.savefig(savedir / "loss_alg")
        plt.close()

        # Plot alg vs recomputed loss gradients.
        fig = plt.figure()

        def getter(r):
            hpd = r.hyper_params_data
            return (
                (
                    hpd.loss_alg_initial_gradient,
                    hpd.loss_initial_gradient,
                    hpd.loss_alg_final_gradient,
                    hpd.loss_final_gradient,
                ),
            )

        n = boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        fig.set_size_inches(16, 1.5 + 0.5 * n)
        plt.title("Orig loss gradient vs recomputed loss gradient")
        plt.savefig(savedir / "loss_alg_gradient")
        plt.close()

    def tukey_plots(self, stems, rel_dir=".", line_check=None):
        # We're doing box plots, so we shouldn't plot stems with very few runs.
        stems = [stem for stem in stems if len(self.stems_good_runs[stem]) > 1]
        if len(stems) == 0:
            warnings.warn("No stems to plot", stacklevel=2)
            return

        # Figure out which parameters are different between these stems.
        hps = [self.stem_params[stem] for stem in stems]
        names_of_modified_params, common_params, common_params_dict = get_unique_stems(
            hps
        )

        def boxplot_getter(getter, plotter=plt.boxplot, **plot_kwargs):
            all_data = []
            all_labels = []

            for stem in stems:
                runs_list = self.stems_good_runs[stem]
                stem_data = np.array([getter(r) for r in runs_list])
                all_data.append(stem_data)
                hp_dict = {
                    k: getattr(self.stem_params[stem], k)
                    for k in names_of_modified_params
                }
                label = name_generator(hp_dict)
                plot_label = escape_latex(label)
                all_labels.append(plot_label)
            plotter(all_data, labels=all_labels, vert=False, **plot_kwargs)

            if line_check is not None:
                # Draw a line every time line_check says to.
                x = plt.xlim()
                hp_prev = self.stem_params[stems[0]]
                for i, stem in enumerate(stems[1:]):
                    hp_cur = self.stem_params[stem]
                    if line_check(hp_prev, hp_cur):
                        yi = i + 1.5
                        plt.plot(x, [yi, yi], "y-")
                    hp_prev = hp_cur

            ax = plt.gca()
            ax.invert_yaxis()
            ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

            return all_data

        # The label for the whole plot should only include the names that aren't different in the group.
        # run_stems = {name : val for name, val in common_params_dict if name not in names_of_modified_params}
        # hp_dict = {name: common_params_dict[name] for name in self.names_of_modified_params if not in names_of_modified_params}
        # label = name_generator(common_params_dict)
        # file_label = escape_filename(label)

        savedir = self.figs_root / "tukey" / rel_dir
        savedir.mkdir(parents=True, exist_ok=True)

        common_kwargs = dict()

        figsize = (16, 1.5 + 0.5 * len(stems))
        # Plot number of optimizer iterations.
        plt.figure(figsize=figsize)
        getter = lambda r: r.hyper_params_data.optimizer_iterations
        boxplot_getter(getter, **common_kwargs)
        plt.title("Optimizer iterations")
        plt.savefig(savedir / "optimizer_iterations")
        plt.close()

        # Plot loss.
        plt.figure(figsize=figsize)
        getter = lambda r: r.hyper_params_data.loss_initial
        boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        plt.title("Initial loss")
        plt.savefig(savedir / "loss_initial")
        plt.close()

        plt.figure(figsize=figsize)
        getter = lambda r: r.hyper_params_data.loss_final
        boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        plt.title("Final loss")
        plt.savefig(savedir / "loss_final")
        plt.close()

        # Plot loss gradient.
        plt.figure(figsize=figsize)
        getter = lambda r: r.hyper_params_data.loss_initial_gradient
        boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        plt.title("Initial loss gradient")
        plt.savefig(savedir / "loss_gradient_initial")
        plt.close()

        plt.figure(figsize=figsize)
        getter = lambda r: r.hyper_params_data.loss_final_gradient
        boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        plt.title("Final loss gradient")
        plt.savefig(savedir / "loss_gradient_final")
        plt.close()

        # Plot walltime.
        plt.figure(figsize=figsize)
        getter = lambda r: r.stdout_info.duration.total_seconds()
        boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
        plt.title("Walltime")
        plt.savefig(savedir / "walltime")
        plt.close()

        # Plot linesearch retries.
        plt.figure(figsize=figsize)
        getter = lambda r: r.stdout_info.network_ls_tries
        stems_data = boxplot_getter(getter, **common_kwargs)
        for i, data in enumerate(stems_data):
            n0 = data.size
            n = n0 - np.count_nonzero(data)
            label = f"{n}/{n0} = {round(n/n0 * 100)}\\%"
            plt.text(-0.95, i + 1, label, ha="left", va="center")
        plt.xlim(left=-1)
        plt.title("Linesearch Retries")
        plt.savefig(savedir / "linesearch_retries")
        plt.close()

        if hasattr(self, "cr_plotters"):
            # CR plotter error.
            for i, cr_plotter in enumerate(self.cr_plotters):
                plt.figure(figsize=figsize)
                getter = lambda r: r.hyper_params_data.invt_losses_final[i] ** 0.5
                boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
                safe_name = escape_latex(cr_plotter.name)
                plt.title(f"Invariant error: {safe_name}")
                plt.savefig(savedir / f"invariant_error_{cr_plotter.name}")
                plt.close()

            # CR plotter hull error.
            i = len(self.square_cr_plotters)
            plt.figure(figsize=figsize)
            getter = lambda r: sum(r.hyper_params_data.invt_losses_final[i:]) ** 0.5
            boxplot_getter(getter, plotter=boxplot_log, **common_kwargs)
            plt.title(f"Invariant error: hull")
            plt.savefig(savedir / f"invariant_error_hull")
            plt.close()

        return stems

    def observation_correlation_plots(self, stems, getters=None, rel_dir="default"):
        # Filter out stems with very few runs.
        stems = [stem for stem in stems if len(self.stems_good_runs[stem]) > 1]
        if len(stems) == 0:
            warnings.warn("No stems to plot", stacklevel=2)
            return

        print(f"Working on observation correlation plots for {rel_dir}")
        # Sort stems so that consecutive ones show the different observation types.
        def stem_sort_key(stem):
            hp = self.stem_params[stem]
            return (
                hp.layer_sizes,
                hp.activation,
                hp.optimizer,
                hp.noise,
                hp.borehole,
                hp.surface,
                hp,
            )

        sorted_stems = sorted(stems, key=stem_sort_key)

        ignored_attrs = ["borehole_noise"]
        different_attr_names = ["surface", "borehole"]

        def in_same_group(hp1, hp2):
            # There should be in the same group if they have identical params
            # other than observer type.
            attrs = {
                name: getattr(hp1, name)
                for name in different_attr_names + ignored_attrs
            }
            hp2_but_with_hp1_attrs = dataclasses.replace(hp2, **attrs)
            return hp1 == hp2_but_with_hp1_attrs

        savedir = self.figs_root / "correlation_observation" / rel_dir
        savedir.mkdir(parents=True, exist_ok=True)

        if getters is None:
            err_getter = lambda r: sum(r.hyper_params_data.invt_losses_final[2:]) ** 0.5
            loss_getter = lambda r: r.hyper_params_data.loss_final
            getters = [("loss", loss_getter), ("invariant space error", err_getter)]
        if len(getters) != 2:
            raise ValueError(
                f"There should be 2 getters, but I got {len(getters)}: {getters}"
            )

        # Split stems into groups.
        def split_stems_into_groups(sorted_stems, in_same_group_func):
            stem_groups = []
            stem_group = [sorted_stems[0]]
            hp_prev = self.stem_params[stem_group[-1]]
            # print('first    :', sorted_stems[0])
            for stem in sorted_stems[1:]:
                hp_cur = self.stem_params[stem]
                if in_same_group_func(hp_prev, hp_cur):
                    stem_group.append(stem)
                else:
                    # print('new group:', stem)
                    stem_groups.append(tuple(stem_group))
                    stem_group = [stem]
                hp_prev = hp_cur
            stem_groups.append(tuple(stem_group))
            return stem_groups

        from itertools import groupby

        def all_equal(iterable):
            g = groupby(iterable)
            return next(g, True) and not next(g, False)

        def make_correlation_plot(
            getters, stem_group, combiner=None, color_splitter=None, save_data=False
        ):

            if combiner is None:
                # Need a function to make a name for values that may be different.
                def combiner(key, val_set):
                    if len(val_set) == 1:
                        return key, val_set.pop()
                    return key, sorted(tuple(val_set))

            num_groups = len(stem_group)
            fig = plt.figure(figsize=(6 * num_groups, 4.8))
            for i, stem in enumerate(stem_group):

                if isinstance(stem, str):
                    runs_list = self.stems_good_runs[stem]
                    hp = self.stem_params[stem]
                    # The label for this subplot should only include the names that are different in the group.
                    run_stems = [
                        (name, getattr(hp, name)) for name in different_attr_names
                    ]
                else:
                    # If stem is a list, let's concatenate all the data for those stems.
                    runs_list = [r for s in stem for r in self.stems_good_runs[s]]
                    hps = [self.stem_params[s] for s in stem]
                    run_stems = [
                        combiner(name, set(getattr(hp, name) for hp in hps))
                        for name in different_attr_names
                    ]
                    hp_dict = {}
                    for name in HyperParams.__dataclass_fields__:
                        vals = list(set(getattr(hp, name) for hp in hps))
                        if all_equal(vals):
                            hp_dict[name] = vals[0]
                        else:
                            hp_dict[name] = tuple(sorted(vals))
                    if color_splitter is not None and color_splitter in hp_dict:
                        color_vals = set(getattr(hp, color_splitter) for hp in hps)
                        color_vals = list(sorted(color_vals))
                        if len(color_vals) == 1:
                            color_splitter = None
                    hp = HyperParams(**hp_dict)
                # run_stems = sorted(run_stems)

                # Make a DataFrame of the relevant data.
                # data_dict = {
                #     k: np.array([getter(r) for r in runs_list]) for k, getter in getters
                # }
                # dataset = pd.DataFrame(data_dict)

                plt.subplot(1, num_groups, i + 1)
                # Extract the relevant data.
                big_label = name_generator({k: v for k, v in run_stems})
                data_list = [
                    np.array([getter(r) for r in runs_list]) for k, getter in getters
                ]
                if color_splitter is None:
                    plt.loglog(data_list[0], data_list[1], ".")
                else:
                    color_vals_save_data = {}
                    for val in color_vals:
                        # Extract the relevant data.
                        color_runs = [
                            r
                            for r in runs_list
                            if getattr(r.hyper_params, color_splitter) == val
                        ]
                        x, y = [
                            np.array([getter(r) for r in color_runs])
                            for k, getter in getters
                        ]

                        val_name = name_generator({color_splitter: val})
                        val_plot_label = escape_latex(val_name)
                        label = f"({np.corrcoef(x, y)[1, 0]: .4f})"
                        if i == 0:
                            label = f"{val_plot_label} {label}"
                        plt.loglog(x, y, ".", label=label)
                        color_vals_save_data[val_name.replace("%", "_")] = np.array(
                            [x, y]
                        ).T

                        n_best = 10
                        ind = np.argpartition(x, -n_best)[-n_best:]
                        ind = ind[np.argsort(x[ind])]

                        print(f"Best {n_best} for {big_label} {val_name}:")
                        print(f"{getters[0][0]:15s}, {getters[1][0]:15s}")
                        for best_idx in ind:
                            run = color_runs[best_idx]
                            print(
                                f"{x[best_idx]: 15.10g}, {y[best_idx]: 15.10g}: {run.directory} {run.label}"
                            )
                        print()

                    if save_data:
                        np.savez(
                            f"correlation_data_{big_label}.npz", **color_vals_save_data
                        )
                plt.xlabel(getters[0][0])
                cur_ylim = plt.ylim()
                if i == 0:
                    plt.ylabel(getters[1][0])
                    ylim = list(cur_ylim)
                    if color_splitter is not None:
                        plt.legend()
                else:
                    ylim[0] = min(ylim[0], cur_ylim[0])
                    ylim[1] = max(ylim[1], cur_ylim[1])
                if color_splitter is not None:
                    plt.legend()

                # name_val_strings = [f"{name}:{val}" for name, val in run_stems]
                # label = " ".join(name_val_strings)
                plot_label = escape_latex(big_label)
                plot_label = f"{plot_label} ({np.corrcoef(*data_list)[1, 0]:.4f})"
                plt.title(plot_label)

            for i, stem in enumerate(stem_group):
                plt.subplot(1, num_groups, i + 1)
                plt.ylim(ylim)

            # The label for the whole plot should only include the names that aren't different in the group.
            run_stems = []
            for name in self.names_of_modified_params:
                if name in (different_attr_names + ignored_attrs):
                    continue
                run_stems.append((name, getattr(hp, name)))
            label = name_generator({k: v for k, v in run_stems})

            plot_label = escape_latex(label)
            fig.suptitle(plot_label)

            file_label = escape_filename(label)
            plt.savefig(savedir / file_label)
            plt.close("all")

        # Let's start out with all of them combined.
        def stem_sort_key(stem):
            hp = self.stem_params[stem]
            return (
                hp.surface,
                not hp.borehole,
                hp.activation,
                hp.layer_sizes,
                hp.optimizer,
                hp.noise,
                hp,
            )

        good_stems = [
            stem for stem in stems if not "relu" in stem and not "trust" in stem
        ]
        sorted_stems = sorted(good_stems, key=stem_sort_key)

        same_attr_names = ["borehole", "surface"]

        def in_same_group(hp1, hp2):
            # There should be in the same group if they have same observer type.
            return all(
                getattr(hp1, name) == getattr(hp2, name) for name in same_attr_names
            )

        observer_stem_groups = split_stems_into_groups(sorted_stems, in_same_group)
        make_correlation_plot(
            getters, observer_stem_groups, color_splitter="noise", save_data=True
        )

        # So now we've got our different stems grouped by observer type (probably 3 groups).

        def make_sort_key(attr_order):
            def stem_sort_key(stem):
                hp = self.stem_params[stem]
                return tuple(getattr(hp, attr) for attr in attr_order) + (hp,)

            return stem_sort_key

        # Now let's break up each of those groups by activation type.
        #   Note: this is changing the variable used by in_same_group().
        same_attr_names = ["activation"]
        key = make_sort_key(same_attr_names)
        sorted_groups = [sorted(g, key=key) for g in observer_stem_groups]
        sub_stem_groups = [
            split_stems_into_groups(g, in_same_group) for g in sorted_groups
        ]
        for groups in zip(*sub_stem_groups):
            make_correlation_plot(getters, groups, color_splitter="noise")

        # Now let's go back and break up each of the groups by layer size type.
        def combiner(name, val_set):
            if len(val_set) == 1:
                return name, val_set.pop()
            if name == "layer_sizes":
                l1 = val_set.pop()
                if all(l1[0] == l[0] for l in val_set):
                    return "units_per_layer", l1[0]
            return name, sorted(tuple(val_set))

        def in_same_group_layer_sizes(hp1, hp2):
            return hp1.layer_sizes[0] == hp2.layer_sizes[0]

        key = make_sort_key(same_attr_names)
        sorted_groups = [sorted(g, key=key) for g in observer_stem_groups]
        sub_stem_groups = [
            split_stems_into_groups(g, in_same_group_layer_sizes) for g in sorted_groups
        ]
        for groups in zip(*sub_stem_groups):
            make_correlation_plot(getters, groups, combiner, color_splitter="noise")

        # Now let's go back and break up each of the groups by layer size/depth type.
        same_attr_names = ["layer_sizes"]
        key = make_sort_key(same_attr_names)
        sorted_groups = [sorted(g, key=key) for g in observer_stem_groups]
        sub_stem_groups = [
            split_stems_into_groups(g, in_same_group) for g in sorted_groups
        ]
        for groups in zip(*sub_stem_groups):
            make_correlation_plot(getters, groups, color_splitter="noise")

    def analyze_solvability(self, runs=None):
        if runs is None:
            runs = self.completed_runs

        runs = set(runs)
        self.runs_initially_solvable = set(
            r for r in runs if r.hyper_params_data.initially_solvable
        )
        self.runs_finally_solvable = set(
            r for r in runs if r.hyper_params_data.finally_solvable
        )

        self.ginit = self.runs_initially_solvable
        self.gfin = self.runs_finally_solvable
        self.binit = runs - self.ginit
        self.bfin = runs - self.gfin
        ginit_gfin = self.ginit & self.gfin
        ginit_bfin = self.ginit & self.bfin
        binit_bfin = self.binit & self.bfin
        binit_gfin = self.binit & self.gfin

        width = 36
        indent = 4
        print(f"{'Total completed':{width}} : {len(runs)}")
        print(f"{'':{indent}}{'Initially solvable':{width-indent}} : {len(self.ginit)}")
        print(
            f"{'':{2*indent}}{'Finally unsolvable':{width-2*indent}} : {len(ginit_bfin)}"
        )
        print(
            f"{'':{2*indent}}{'Finally solvable':{width-2*indent}} : {len(ginit_gfin)}"
        )
        print(
            f"{'':{indent}}{'Initially unsolvable':{width-indent}} : {len(self.binit)}"
        )
        print(
            f"{'':{2*indent}}{'Finally unsolvable':{width-2*indent}} : {len(binit_bfin)}"
        )
        print(
            f"{'':{2*indent}}{'Finally solvable':{width-2*indent}} : {len(binit_gfin)}"
        )

    def get_post_run_data(
        self, taylor=False, get_rid_of_corner=True, run_plots_check=False
    ):
        # for run in self.runs:
        #     - build the CR.
        #     - Run the experiment.
        #     - Get CR output in invariant domain space
        if not callable(run_plots_check):
            run_plots_val = run_plots_check
            run_plots_check = lambda run: run_plots_val

        if get_rid_of_corner:
            self.corner = (1.85, 0.07)
        else:
            self.corner = None

        # Build the experiment, assuming all of the runs use the same mesh params.
        ex, mesh, top_boundary, right_boundary = build_experiment_and_mesh(
            self.common_params
        )
        set_default_covering_params(
            domain=mesh.ufl_domain(), quad_params=ex.quad_params
        )

        # Run experiment to get ground-truth observations.
        cache = PickleDictionaryCache("cache/db.pickle")
        noiseless_params = dataclasses.replace(
            self.common_params, noise=None, borehole_noise=None
        )
        cr_true, w_trues_train = get_ground_truth_data(
            noiseless_params,
            ex,
            None,
            debug=False,
            refresh_cache=False,
            cache=cache,
            do_plots=False,  # TODO: maybe generate these plots.
        )

        save_experiment_output(w_trues_train, None, "experiment_output_true")

        run_training, run_testing = build_training_testing_functions(
            ex, None, 0, top_boundary, right_boundary
        )

        invariant_names = [
            r"tr $\dot\epsilon$",
            r"tr $\dot\epsilon^2$",
            r"$\phi$",
        ]
        coeff_names = [r"$\frac{D\phi}{Dt}$"]
        quad_params = self.common_params.quad_params

        common_kwargs = dict(
            num_scalar_invariants=3,
            num_form_coefficients=1,
            xy_mesh=w_trues_train[0].function_space().mesh(),
            quad_params=quad_params,
            invariant_cmap=INVARIANT_SPACE_CMAP,
            xy_cmap="spring",
            invariant_idxs=(1, 2),
            invariant_names=invariant_names,
            coeff_names=coeff_names,
        )
        cr_plotter_large = InvariantCRPlotter(
            **common_kwargs,
            x_range=(20, 500) if get_rid_of_corner else (20, 4900),
            y_range=(0.08, 0.16),
            name="large",
        )

        cr_plotter_small = InvariantCRPlotter(
            **common_kwargs,
            x_range=(0, 20),
            y_range=(0, 0.15),
            name="small",
        )

        def hull_alpha_large(points, ind, r):
            x = points[ind, 0].mean()
            if r < 0.2 or x > 0.7:
                alpha = 1e-15
            else:
                alpha = 100
            # alpha = x * (right_alpha - left_alpha) + left_alpha
            # print(ind, r, alpha)
            return alpha

        cr_plotter_large.hull_alpha = hull_alpha_large
        cr_plotter_small.hull_alpha = 0

        with stop_annotating():
            # Get xy invariant info to build alpha-shape for tighter invariant-space meshes.
            # TODO: should probably separate xy stuff into a separate CRPlotter class.
            self.square_cr_plotters = [cr_plotter_small, cr_plotter_large]
            cr_inputs_ufl = ex.get_cr_inputs(w_trues_train[0], ())
            self.square_cr_plotters_data = get_true_invariant_data(
                self.common_params, self.square_cr_plotters, cr_inputs_ufl
            )

            self.alpha_cr_plotters = []
            for cr_plotter, true_data_info in zip(
                self.square_cr_plotters, self.square_cr_plotters_data
            ):
                hull_mesh = get_invariant_space_hull(
                    cr_plotter,
                    true_data_info[0],
                    cr_plotter.hull_alpha,
                    corner=self.corner,
                )
                cr_plotter_hull = InvariantCRPlotter(
                    **common_kwargs,
                    cr_mesh=hull_mesh,
                    x_range=cr_plotter.x_range,
                    y_range=cr_plotter.y_range,
                    name=f"{cr_plotter.name}_hull",
                )
                self.alpha_cr_plotters.append(cr_plotter_hull)
            self.cr_plotters = self.square_cr_plotters + self.alpha_cr_plotters

            self.alpha_cr_plotters_data = get_true_invariant_data(
                self.common_params, self.alpha_cr_plotters, cr_inputs_ufl
            )
            self.invt_data_trues = (
                self.square_cr_plotters_data + self.alpha_cr_plotters_data
            )

        for i, run in enumerate(self.good_runs):
            print(i + 1, run.directory)
            invariant_data_preds = run.get_cr_output(
                cr_type="trained",
                cr_plotters=self.cr_plotters,
                cr_inputs_ufl=cr_inputs_ufl,
                refresh_cache=False,
            )
            run.post_data.invt_data_preds = invariant_data_preds
        return

        # from plot_experiment import full_data
        # tmp_runs = [
        #     Run.init_from_directory(d["directory"], True, True)
        #     for k, d in full_data.items()
        # ]

        # for i, run in enumerate(tmp_runs):
        for i, run in enumerate(self.good_runs):
            set_working_tape(Tape())
            print(i + 1, run.directory)
            observer, loss = build_observer_and_loss(
                run.hyper_params, mesh, top_boundary, right_boundary
            )
            y_trues_train = [observer(w) for w in w_trues_train]
            if run.network_trained is None:
                run.post_data.invt_data_preds = None
                continue
            trained_param_bag = ParamBag(run.network_trained.get_flat_params())
            trained_params_val = trained_param_bag.get_vals("trainable")
            run_kwargs = dict(
                network=run.network_initial,
                cr_DphiDt_trained=run.cr_trained,
                trained_params_val=trained_params_val,
                hyper_params_data=run.hyper_params_data,
                run_training=run_training
                if True or taylor and not run.hyper_params_data.complete
                else None,
                verify=taylor and not run.hyper_params_data.complete,
                run_testing=run_testing,
                y_trues_train=y_trues_train,
                y_trues_test=None,
                w_trues=w_trues_train,
                observer=observer,
                loss=loss,
                cr_plotters=self.cr_plotters,
                dirpath=run.directory,  # if run_plots_check(run) else None,
                xy_use_true=True,  # False,
                invariant_data_trues=self.invt_data_trues,
            )
            invariant_data_preds, *rvs = run_network_cr_batch(
                run.cr_initial,
                run.hyper_params,
                **run_kwargs,
            )
            plt.close("all")
            if not run.hyper_params_data.complete:
                run.hyper_params_data = rvs[0]

            run.post_data.invt_data_preds = invariant_data_preds


    @no_annotations
    def cr_plots(self, specials=None, **plot_kwargs):
        savedir = pathlib.Path(self.figs_root / "cr_plots")
        savedir_data = pathlib.Path(self.figs_root / "cr_data")
        savedir.mkdir(parents=True, exist_ok=True)
        savedir_data.mkdir(parents=True, exist_ok=True)

        # Plot invariants in invariant domain.
        for cr_plotter, (true_data, *true_rest) in zip(
            self.cr_plotters, self.invt_data_trues
        ):
            V_smooth = FunctionSpace(cr_plotter.cr_mesh, "P", 1)
            plt.figure()
            f = project(true_data.coeff_func_quad, V_smooth)
            plt.colorbar(plot(f, **plot_kwargs))

            xy_coords, invts, mask_xy = compute_invt_patches(
                self.common_params, cr_plotter, true_data, corner=self.corner
            )
            np.savez(
                f"data_xy_{cr_plotter.name}.npz",
                xy_coords=xy_coords,
                invts=invts,
                mask_xy=mask_xy,
            )

            ax = plt.gca()
            ax.set_aspect(1.0 / ax.get_data_ratio(), adjustable="datalim")
            plt.xlabel(cr_plotter.invariant_names[cr_plotter.invariant_idxs[0]])
            plt.ylabel(cr_plotter.invariant_names[cr_plotter.invariant_idxs[1]])
            plt.savefig(savedir / f"true_invariant_{cr_plotter.name}")
            plt.close()

        # Extract CR output data from runs and organize the data by stem.
        for cr_plotter in self.cr_plotters:
            cr_plotter.stems_cr_data = defaultdict(list)
            cr_plotter.all_cr_data = []

        for run in self.good_runs:
            pd = run.post_data
            for cr_plotter, invt_data in zip(self.cr_plotters, pd.invt_data_preds):
                cr_plotter.stems_cr_data[run.label].append(invt_data)
                cr_plotter.all_cr_data.append(invt_data)

        print("Total:", len(self.cr_plotters[0].all_cr_data))

        for cr_plotter, (true_data, *true_rest) in zip(
            self.cr_plotters, self.invt_data_trues
        ):
            all_data = cr_plotter.all_cr_data
            all_data_np = [d.coeff_func_quad.vector().get_local() for d in all_data]
            all_data_np = np.array(all_data_np)
            f = true_data.coeff_func_quad
            V = f.function_space()
            true_data_np = f.vector().get_local()
            info, smooth_info = get_aggregate_error_info(all_data_np, true_data_np, V)

            idx = 1
            fig = plt.figure(figsize=(6 * 2, 4.8))
            plt.subplot(1, 2, 1)
            do_cr_plotter_plots(
                cr_plotter,
                smooth_info.rmse,
                "Standard error (RMSE)",
                borehole=True,
                surface=True,
                **plot_kwargs,
            )
            plt.ylabel(cr_plotter.invariant_names[cr_plotter.invariant_idxs[1]])

            plt.subplot(1, 2, 2)
            do_cr_plotter_plots(
                cr_plotter,
                smooth_info.stddev,
                "Standard deviation of CR",
                borehole=True,
                surface=True,
                **plot_kwargs,
            )

            fig.suptitle("All")
            fig.savefig(savedir / f"{cr_plotter.name}_all")
            plt.close()

        V_smooth_xy = FunctionSpace(self.cr_plotters[0].xy_mesh, "P", 1)

        # Plot borehole info in physical domain.
        # These plots didn't come out well, so it's fine if they no longer work.
        # quad_dx = get_quadrature_dx(self.square_cr_plotters_data[0][0].scalar_invts_quad_xy.ufl_element(), dx)
        # invt_idx = 1
        # for cr_plotter, true_data in zip(self.square_cr_plotters, self.square_cr_plotters_data):
        #     plt.figure()
        #     f = true_data[0].scalar_invts_quad_xy[invt_idx]
        #     if self.corner is not None:
        #         x = SpatialCoordinate(V_smooth_xy.mesh())
        #         f = conditional(
        #             And(x[0] > Constant(self.corner[0]), x[1] > Constant(self.corner[1])),
        #             f,
        #             Constant(0),
        #         )
        #     plt.colorbar(plot(project(f, V_smooth_xy, dx=quad_dx), **plot_kwargs))
        #     plot_xy_space(cr_plotter, true_data[0])
        #     plt.xlabel(r"$x$")
        #     plt.ylabel(r"$y$")
        #     plt.title(f"{cr_plotter.invariant_names[invt_idx]} in physical domain")
        #     plt.savefig(savedir / f"true_xy_coords_{cr_plotter.name}")
        #     plt.close()

        plt.figure(figsize=(6.4 * 1.8, 4.8))
        plot(self.cr_plotters[0].xy_mesh, zorder=0)
        right_color = point_kwargs_right["facecolor"]
        for cr_plotter, true_data in zip(
            self.square_cr_plotters, self.square_cr_plotters_data
        ):
            plot_xy_space(
                cr_plotter, true_data[0], right_color=right_color, alpha=0.6, size=3
            )
            right_color = ORANGE
        plt.legend(bbox_to_anchor=(1.05, 1), loc="upper left", borderaxespad=0.0)

        plt.xlabel(r"$x$")
        plt.ylabel(r"$y$")
        plt.savefig(savedir / "true_xy_coords_mesh")
        plt.close()

        with plt.rc_context({"text.usetex": True, "figure.titlesize": 20}):
            for i, label in enumerate(self.stems_good_runs):
                if len(self.cr_plotters[0].stems_cr_data[label]) < 2:
                    print(f"{i+1}/{len(self.stems_good_runs)}: {label} skipped")
                    continue
                print(f"{i+1}/{len(self.stems_good_runs)}: {label}")

                title_label = escape_latex(label)
                file_label = escape_filename(label)
                hp = self.stem_params[label]

                for cr_plotter, (true_data, *true_rest) in zip(
                    self.cr_plotters, self.invt_data_trues
                ):
                    data_np = [
                        d.coeff_func_quad.vector().get_local()
                        for d in cr_plotter.stems_cr_data[label]
                    ]
                    data_np = np.array(data_np)

                    f = true_data.coeff_func_quad
                    V = f.function_space()
                    true_data_np = f.vector().get_local()
                    info, smooth_info = get_aggregate_error_info(
                        data_np, true_data_np, V
                    )

                    filename = savedir_data / f"{cr_plotter.name}"
                    # filename = savedir_data / f"{cr_plotter.name}_{file_label}"
                    save_aggregate_error_infos(filename, (label,), (smooth_info,))
                    # np.savez(
                    #     savedir_data / f"{cr_plotter.name}_{file_label}.npz",
                    #     rmse=smooth_info.rmse,
                    #     stddev=smooth_info.stddev,
                    # )

                    # m = plot(f, **plot_kwargs, vmin=0)
                    # if do_colorbar:
                    #     if boundaries is not None:
                    #         c_kwargs = dict(boundaries=boundaries)
                    #     else:
                    #         c_kwargs = {}
                    #     plt.colorbar(m, **c_kwargs)
                    # plt.xlim(cr_plotter.x_range)
                    # plt.ylim(cr_plotter.y_range)
                    # plt.xlabel(cr_plotter.invariant_names[cr_plotter.invariant_idxs[0]])

                    fig = plt.figure(figsize=(6 * 2, 4.8))
                    plt.subplot(1, 2, 1)
                    do_cr_plotter_plots(
                        cr_plotter,
                        smooth_info.rmse,
                        "Standard error (RMSE)",
                        borehole=hp.borehole,
                        surface=hp.surface,
                        **plot_kwargs,
                    )
                    plt.ylabel(cr_plotter.invariant_names[cr_plotter.invariant_idxs[1]])

                    plt.subplot(1, 2, 2)
                    do_cr_plotter_plots(
                        cr_plotter,
                        smooth_info.stddev,
                        "Standard deviation of CR",
                        borehole=hp.borehole,
                        surface=hp.surface,
                        **plot_kwargs,
                    )
                    plt.suptitle(f"{title_label} (N={len(data_np)})")
                    fig.savefig(savedir / f"{cr_plotter.name}_{file_label}")
                    plt.close()

            if specials is not None:
                for k, group in enumerate(specials):
                    print("specials", k)
                    group_idx_labels = []
                    group_idxs = []
                    plot_idx = 0
                    for i in group:
                        if isinstance(i, int):
                            label = self.idx_to_stem[i]
                        else:
                            label = i
                            i = self.stem_to_idx[label]
                        if len(self.cr_plotters[0].stems_cr_data[label]) < 2:
                            print(f"{i+1}/{len(self.idx_to_stem)}: {label} skipped")
                            continue
                        group_idx_labels.append((i, label, plot_idx))
                        print(f"{i+1}/{len(self.idx_to_stem)}: {label}")
                        plot_idx += 1

                    ncols = len(group_idx_labels)
                    for cr_plotter, (true_data, *true_rest) in zip(
                        self.cr_plotters, self.invt_data_trues
                    ):
                        smooth_infos = []
                        for i, label, plot_idx in group_idx_labels:
                            print(
                                f"{cr_plotter.name} working on {i+1}/{len(self.idx_to_stem)}: {label}"
                            )

                            data_np = [
                                d.coeff_func_quad.vector().get_local()
                                for d in cr_plotter.stems_cr_data[label]
                            ]
                            data_np = np.array(data_np)

                            f = true_data.coeff_func_quad
                            V = f.function_space()
                            true_data_np = f.vector().get_local()
                            info, smooth_info = get_aggregate_error_info(
                                data_np, true_data_np, V
                            )
                            smooth_infos.append(smooth_info)

                        maxs = [
                            info.rmse.vector().get_local().max()
                            for info in smooth_infos
                        ]
                        median_max_idx = np.argsort(maxs)[len(maxs) // 2]
                        median_max_val = maxs[median_max_idx]
                        levels = np.linspace(0, median_max_val, 40)

                        do_colorbar = False

                        b = np.zeros(len(levels) + 1)
                        b[1:-1] = 0.5 * (levels[:-1] + levels[1:])
                        b[0] = 2.0 * b[1] - b[2]
                        b[-1] = 2.0 * b[-2] - b[-3]
                        boundaries = b

                        fig = plt.figure(figsize=(1 + 5 * ncols, 4.8))
                        axs = []
                        for (i, label, plot_idx), smooth_info in zip(
                            group_idx_labels, smooth_infos
                        ):
                            hp = self.stem_params[label]

                            plot_kwargs["levels"] = levels
                            plot_kwargs["vmax"] = median_max_val
                            axs.append(plt.subplot(1, ncols, plot_idx + 1))
                            colorbar_plot = do_cr_plotter_plots(
                                cr_plotter,
                                smooth_info.rmse,
                                "",
                                borehole=hp.borehole,
                                surface=hp.surface,
                                do_colorbar=False,
                                # boundaries=boundaries,
                                **plot_kwargs,
                            )
                            if plot_idx == 0:
                                plt.ylabel(
                                    cr_plotter.invariant_names[
                                        cr_plotter.invariant_idxs[1]
                                    ]
                                )

                        title_label = ",".join(
                            [str(i) for i, label, plot_idx in group_idx_labels]
                        )

                        fig.set_tight_layout(False)
                        for ax in axs[1:]:
                            ax.tick_params(labelleft=False)

                        cax = make_square_axes_with_colorbar(
                            axs[-1], size=0.15, pad=0.1
                        )
                        plt.colorbar(colorbar_plot, ax=axs, cax=cax)

                        # plt.suptitle(f"Standard error (RMSE)  ({title_label})")
                        fig.savefig(
                            savedir / f"{cr_plotter.name}_{title_label}_rmse",
                            bbox_inches="tight",
                        )
                        plt.close()


from matplotlib.transforms import Bbox
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size


class RemainderFixed(axes_size.Scaled):
    def __init__(self, xsizes, ysizes, divider):
        self.xsizes = xsizes
        self.ysizes = ysizes
        self.div = divider

    def get_size(self, renderer):
        xrel, xabs = axes_size.AddList(self.xsizes).get_size(renderer)
        yrel, yabs = axes_size.AddList(self.ysizes).get_size(renderer)
        bb = Bbox.from_bounds(*self.div.get_position()).transformed(
            self.div._fig.transFigure
        )
        w = bb.width / self.div._fig.dpi - xabs
        h = bb.height / self.div._fig.dpi - yabs
        return 0, min([w, h])


def make_square_axes_with_colorbar(ax, size=0.1, pad=0.1):
    """Make an axes square, add a colorbar axes next to it,
    Parameters: size: Size of colorbar axes in inches
                pad : Padding between axes and cbar in inches
    Returns: colorbar axes
    """
    divider = make_axes_locatable(ax)
    margin_size = axes_size.Fixed(size)
    pad_size = axes_size.Fixed(pad)
    xsizes = [pad_size, margin_size]
    yhax = divider.append_axes("right", size=margin_size, pad=pad_size)
    divider.set_horizontal([RemainderFixed(xsizes, [], divider)] + xsizes)
    divider.set_vertical([RemainderFixed(xsizes, [], divider)])
    return yhax


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "--stem",
        type=str,
        help="stem for writing output files. {stem}-data.txt and {stem}-stems.txt",
        default=None,
    )

    parser.add_argument(
        "input",
        help="Which directory to use as input. Should contain jobs/ and stdout/.",
    )


    parser.add_argument(
        "--output",
        help="Which directory to use as output. Defaults to current directory.",
        default="."
    )



    parser.add_argument(
        "--n",
        type=int,
        help="Number of job directories to process. -1 indicates no limit. Defaults to -1.",
        default=-1
    )

    args = parser.parse_args()
    if args.output is None:
        args.output = args.input

    if args.stem is None:
        args.stem = f"{args.output}/analyzer"


    return args


if __name__ == "__main__":
    args = parse_args()

    job_analyzer = JobAnalyzer(args.stem)
    input_dir = pathlib.Path(args.input)
    if not input_dir.exists():
        raise ValueError(f"Couldn't find root directory: {input_dir}")

    job_dir = input_dir / "jobs"
    if not job_dir.exists():
        raise ValueError(f"Couldn't find job directory: {job_dir}")

    stdout_dir = input_dir / "stdout"
    if not stdout_dir.exists():
        raise ValueError(f"Couldn't find stdout directory: {stdout_dir}")

    i = 0
    for child in job_dir.iterdir():
        if child.is_dir():
            print(i, child)
            job_analyzer.add_directory(child)
            i += 1
            if args.n >= 0 and i > args.n:
                break

    print()
    job_analyzer.analyze_solvability()
    print()

    job_analyzer.get_unique_stems()


    job_analyzer.get_post_run_data()

    specials = [
        (
            "activation:softplus borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:False layer_sizes:(4, 4, 4) optimizer:trust-constr surface:True",
        ),
        (
            "activation:relu borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
            "activation:tanh borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
        ),
        (
            "activation:softplus borehole:False layer_sizes:(2,) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:False layer_sizes:(2, 2) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:False layer_sizes:(2, 2, 2) optimizer:L-BFGS-B surface:True",
        ),
        (
            "activation:softplus borehole:False layer_sizes:(4,) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:False layer_sizes:(4, 4) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
        ),
        (
            "activation:softplus borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:False",
            "activation:softplus borehole:False layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
            "activation:softplus borehole:True layer_sizes:(4, 4, 4) optimizer:L-BFGS-B surface:True",
        ),
    ]

    job_analyzer.cr_plots(
        cmap=INVARIANT_SPACE_ERROR_CMAP,
        extend="max",
        # specials=specials,
    )

    stems = job_analyzer.idx_to_stem_good

    # loss_getter = lambda r: r.hyper_params_data.loss_final

    # err_getter = lambda r: sum(r.hyper_params_data.invt_losses_final[2:]) ** 0.5
    # getters = [("loss", loss_getter), ("invariant space error", err_getter)]
    # job_analyzer.observation_correlation_plots(stems, getters=getters, rel_dir="hull")

    # err_getter = lambda r: sum(r.hyper_params_data.invt_losses_final[:2]) ** 0.5
    # getters = [("loss", loss_getter), ("invariant space error", err_getter)]
    # job_analyzer.observation_correlation_plots(stems, getters=getters, rel_dir="square")

    # err_getter = lambda r: r.hyper_params_data.invt_losses_final[0] ** 0.5
    # getters = [("loss", loss_getter), ("invariant space error", err_getter)]
    # job_analyzer.observation_correlation_plots(
    #     stems, getters=getters, rel_dir="square_small"
    # )

    # err_getter = lambda r: r.hyper_params_data.invt_losses_final[1] ** 0.5
    # getters = [("loss", loss_getter), ("invariant space error", err_getter)]
    # job_analyzer.observation_correlation_plots(
    #     stems, getters=getters, rel_dir="square_large"
    # )

    # err_getter = lambda r: r.hyper_params_data.invt_losses_final[2] ** 0.5
    # getters = [("loss", loss_getter), ("invariant space error", err_getter)]
    # job_analyzer.observation_correlation_plots(
    #     stems, getters=getters, rel_dir="hull_small"
    # )

    # err_getter = lambda r: r.hyper_params_data.invt_losses_final[3] ** 0.5
    # getters = [("loss", loss_getter), ("invariant space error", err_getter)]
    # job_analyzer.observation_correlation_plots(
    #     stems, getters=getters, rel_dir="hull_large"
    # )

    def bfgs_noiseless_stem_check(hp):
        return (
            hp.optimizer == "L-BFGS-B"
            and hp.noise == ()
            and not hp.surface
            and not hp.borehole
        )

    bfgs_noiseless_stems = [
        stem
        for stem in stems
        if bfgs_noiseless_stem_check(job_analyzer.stem_params[stem])
    ]

    rel_dir = "bfgs_noiseless"
    print(f"Building Tukey plots in {rel_dir}")

    def stem_sort_key(stem):
        hp = job_analyzer.stem_params[stem]
        return (
            hp.activation,
            hp.layer_sizes,
            hp,
        )

    bfgs_noiseless_stems = sorted(bfgs_noiseless_stems, key=stem_sort_key)
    job_analyzer.tukey_plots(stems=bfgs_noiseless_stems, rel_dir=rel_dir)

    def recursive_splitter(job_analyzer, splitter_list, line_checks, stems, rel_dir):
        # First, plot the current set of stems in the current directory.
        if line_checks is not None:
            cur_line_check = line_checks[0]
            line_checks = line_checks[1:]
        rel_dir = pathlib.Path(rel_dir)
        print(f"Building Tukey plots in {rel_dir}")
        job_analyzer.tukey_plots(
            stems=stems, rel_dir=rel_dir, line_check=cur_line_check
        )

        # Then handle splitting up the stems into separate directories.
        if len(splitter_list) == 0:
            return
        cur_splitter = splitter_list[0]
        splitter_list = splitter_list[1:]
        for name, stem_filter in cur_splitter.items():
            filtered_stems = [
                stem for stem in stems if stem_filter(job_analyzer.stem_params[stem])
            ]
            split_dir = rel_dir / name
            recursive_splitter(
                job_analyzer,
                splitter_list,
                line_checks,
                stems=filtered_stems,
                rel_dir=split_dir,
            )

    # Do box plots for recursive subcategories.
    splitter_optimizer = {
        "trust": lambda hp: hp.optimizer == "trust-constr",
        "bfgs": lambda hp: hp.optimizer == "L-BFGS-B",
    }
    splitter_activation = {
        # "relu": lambda hp: hp.activation == "relu",
        "softplus": lambda hp: hp.activation == "softplus",
        "tanh": lambda hp: hp.activation == "tanh",
    }
    splitter_layer_size = {
        "two": lambda hp: hp.layer_sizes[0] == 2,
        "four": lambda hp: hp.layer_sizes[0] == 4,
    }
    splitters = [
        splitter_optimizer,
        splitter_activation,
        splitter_layer_size,
    ]

    def line_check_optimizer(hp1, hp2):
        # There should be a line drawn if anything but optimizer is different.
        hp2_mod = dataclasses.replace(hp2, optimizer=hp1.optimizer)
        return hp1 != hp2_mod

    def line_check_activation(hp1, hp2):
        # There should be a line drawn if anything but activation is different.
        attr_names = ["activation"]
        attrs = {name: getattr(hp1, name) for name in attr_names}
        hp2_mod = dataclasses.replace(hp2, **attrs)
        return hp1 != hp2_mod

    def line_check_layer_sizes(hp1, hp2):
        # There should be a line drawn if anything but layer_sizes is different.
        hp2_mod = dataclasses.replace(hp2, layer_sizes=hp1.layer_sizes)
        return hp1 != hp2_mod

    line_checks = [
        line_check_optimizer,
        line_check_activation,
        line_check_layer_sizes,
        line_check_layer_sizes,
    ]

    recursive_splitter(job_analyzer, splitters, line_checks, stems, ".")


    def recursive_splitter_exp_losses(job_analyzer, splitter_list, stems, rel_dir):
        # First, plot the current set of stems in the current directory.
        rel_dir = pathlib.Path(rel_dir)
        print(f"Building exp Tukey plots in {rel_dir}")
        job_analyzer.tukey_plots_exp_losses(stems=stems, rel_dir=rel_dir)

        # Then handle splitting up the stems into separate directories.
        if len(splitter_list) == 0:
            return
        cur_splitter = splitter_list[0]
        splitter_list = splitter_list[1:]
        for name, stem_filter in cur_splitter.items():
            filtered_stems = [
                stem for stem in stems if stem_filter(job_analyzer.stem_params[stem])
            ]
            split_dir = rel_dir / name
            recursive_splitter_exp_losses(
                job_analyzer,
                splitter_list,
                stems=filtered_stems,
                rel_dir=split_dir,
            )

    recursive_splitter_exp_losses(job_analyzer, splitters, stems, ".")

    plt.show()

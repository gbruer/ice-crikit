import ast
import csv
import numpy as np
import os
import pathlib
import re
import shutil
import warnings
from dataclasses import dataclass, field, replace
from datetime import datetime, timedelta
from pprint import pprint

from crikit import CR, no_annotations
from .cr import make_jax_network_cr
from .network_builders import Network

from .hyper_param_utils import HyperParams, HyperParamsData
from .param_utils import ParamBag


@dataclass(frozen=True)
class StdoutInfo:
    duration: timedelta
    network_ls_tries: int = -1


class DataBox:
    pass


class RunPaths:
    def __init__(self, directory):
        self.directory = directory

        self.params_initial = directory / "params_initial.npz"
        self.params_initial_linesearch = directory / "params_initial_linesearch.npz"
        self.params_trained = directory / "params_trained.npz"
        self.hyper_params = directory / "hyper_params.txt"
        self.hyper_params_data = directory / "hyper_params_data.txt"
        self.batch_data = directory / "batch_data.csv"
        self.loss = directory / "loss_network_cr.csv"

        self.params_solvable = (
            self.params_initial_linesearch
            if self.params_initial_linesearch.exists()
            else self.params_initial
        )

    def get_cr_output(self, cr_type, cr_plotters):
        files = []
        for p in cr_plotters:
            files.append(self.directory / f"cr_output_{cr_type}_{p.name}.npz")
        return files


@dataclass(unsafe_hash=True)
class Run:
    READ_MAX = 20000

    directory: pathlib.Path
    hyper_params: HyperParams
    stdout_info: StdoutInfo
    hyper_params_data: HyperParamsData
    is_complete: bool
    paths: RunPaths

    cr_initial: CR = field(default=None, compare=False)
    network_initial: Network = field(default=None, compare=False)

    cr_trained: CR = field(default=None, compare=False)
    network_trained: Network = field(default=None, compare=False)

    label: str = field(default="", init=False, compare=False)

    post_data: DataBox = field(compare=False, init=False, default_factory=DataBox)

    @no_annotations
    def get_cr_output(
        self,
        cr_type,
        cr_plotters=(),
        cr_inputs_ufl=None,
        refresh_cache=False,
        cleanup=True,
    ):
        if cr_type not in ("initial", "trained"):
            raise ValueError(f"invalid cr_type: {cr_type}")

        paths_cr_output = self.paths.get_cr_output(cr_type, cr_plotters)
        if refresh_cache:
            is_cached = [False] * len(cr_plotters)
        else:
            # is_cached = [p.exists() and time.gmtime(os.getmtime()) for p in paths_cr_output]
            is_cached = []
            for p in paths_cr_output:
                if p.exists():
                    dt = datetime.now() - datetime.fromtimestamp(os.path.getmtime(p))
                    if dt / timedelta(days=1) < 4 or True:
                        is_cached.append(True)
                    else:
                        is_cached.append(False)
                else:
                    is_cached.append(False)

        if not all(is_cached):
            # Need to compute some cr output, so let's build the CR.
            cr = self.build_cr(cr_type)
            if cr is None:
                return
            (
                scalar_invt_map,
                form_invt_map,
                inner_map,
                coeff_form_map,
            ) = cr.get_point_maps()

        invariant_data_preds = []
        for cr_plotter, c, path in zip(cr_plotters, is_cached, paths_cr_output):
            if c:
                invariant_data_pred = cr_plotter.compute_from_file(
                    None,
                    None,
                    path,
                )
            else:
                invariant_data_pred = cr_plotter.compute(
                    scalar_invt_map,
                    inner_map,
                    cr_inputs_ufl,
                )
                invariant_data_pred.record_to_file(path)
            invariant_data_preds.append(invariant_data_pred)

        if cleanup:
            if cr_type == "initial":
                self.cr_initial = None
                self.network_initial = None
            else:
                self.cr_trained = None
                self.network_trained = None

        return invariant_data_preds

    def build_cr(self, cr_type):
        if cr_type not in ("initial", "trained"):
            raise ValueError(f"invalid cr_type: {cr_type}")
        if cr_type == "initial":
            cr = self.cr_initial
            path = self.paths.params_solvable
        else:
            cr = self.cr_trained
            path = self.paths.params_trained
        if cr is not None:
            return cr

        if not path.exists():
            warnings.warn(f"{self.directory}: {cr_type} params not found", stacklevel=2)
            return

        cr, network = Run.build_cr_network(self.hyper_params)
        network.load_params_from_file(path)
        param_bag = ParamBag(network.get_flat_params())
        cr.set_params(param_bag.get_vals())

        if cr_type == "initial":
            self.cr_initial = cr
            self.network_initial = network
        else:
            self.cr_trained = cr
            self.network_trained = network

        return cr

    @staticmethod
    def init_from_directory(directory):
        directory = pathlib.Path(directory)
        paths = RunPaths(directory)

        # Read hyper params from file.
        hyper_params = Run.read_hyper_params(paths.hyper_params)
        if hyper_params is None:
            warnings.warn(f"{directory}: hyperparams not found", stacklevel=2)

        if paths.hyper_params_data.exists():
            # Read hyper params data from file.
            hyper_params_data = Run.read_hyper_params_data(paths.hyper_params_data)
        else:
            warnings.warn(
                f"{directory}: hyper params data file not found", stacklevel=2
            )

            # Read loss from file.
            opt_info = Run.read_loss(paths.loss)
            if opt_info is None:
                warnings.warn(
                    f"{directory}: optimization information not found", stacklevel=2
                )

            if True or network_initial is None or opt_info is None:
                hyper_params_data = None
            else:
                # Rebuild hyper params data.
                param_bag = ParamBag(network_initial.get_flat_params())
                hyper_params_data = HyperParamsData.init_from_opt_info(
                    opt_info, param_bag
                )

        # Read stdout.
        stdout_info = Run.read_stdout(directory)

        is_complete = (
            paths.params_initial.exists()
            and paths.params_trained.exists()
            and paths.hyper_params.exists()
            and paths.hyper_params_data.exists()
            and paths.loss.exists()
            and hyper_params is not None
            and stdout_info is not None
            and hyper_params_data is not None
        )
        run = Run(
            directory=directory,
            paths=paths,
            hyper_params=hyper_params,
            stdout_info=stdout_info,
            hyper_params_data=hyper_params_data,
            is_complete=is_complete,
        )
        return run

    @staticmethod
    def get_stdout_stderr_paths(directory):
        job_id = directory.name
        i = job_id.find("[")
        if i == -1:
            suffix = ""
        else:
            j = job_id.find("]", i)
            if j == -1:
                raise ValueError('Found "[" without matching "]"')
            n = job_id[i + 1 : j]
            suffix = f"-{n}"

        stdout_name = f"{job_id}.out{suffix}"
        stdout_path = directory / ".." / ".." / "stdout" / stdout_name

        stderr_name = f"{job_id}.err{suffix}"
        stderr_path = directory / ".." / ".." / "stderr" / stderr_name

        # Some of the stdout files were accidentally named with .err extension.
        if not stdout_path.exists():
            stdout_name = f"{job_id}.err{suffix}"
            stdout_path = directory / ".." / ".." / "stdout" / stdout_name
            if not stdout_path.exists():
                warnings.warn(
                    f"{directory}: stdout file was not found (tried {stdout_path})",
                    stacklevel=2,
                )
                stdout_path = None

        # Some of the stderr files were accidentally named with .out extension.
        if not stderr_path.exists():
            stderr_name = f"{job_id}.out{suffix}"
            stderr_path = directory / ".." / ".." / "stderr" / stderr_name
            if not stderr_path.exists():
                warnings.warn(
                    f"{directory}: stderr file was not found (tried {stderr_path})",
                    stacklevel=2,
                )
                stderr_path = None

        return stdout_path, stderr_path

    @staticmethod
    def read_stdout(directory):
        stdout_path, stderr_path = Run.get_stdout_stderr_paths(directory)

        # Extract runtime from PBS info.
        start_prefix = "End PBS Prologue"
        end_prefix = "Begin PBS Epilogue"
        datetime_format = "%a %b %d %H:%M:%S %Z %Y"

        # And get number of line search retries.
        re_linesearch_retries = re.compile(
            r"(\d*) Retrying with smaller network weights"
        )
        network_ls_tries = -1
        network_worked = False
        network_worked_prefix = "The network worked"

        def parse_program_output(line):
            nonlocal network_ls_tries, network_worked

            m = re_linesearch_retries.match(line)
            if m is not None:
                network_ls_tries = int(m.group(1)) + 1
                # print("FOUND RETRYING LINE: SET TO", network_ls_tries)
                return

            if line.startswith(network_worked_prefix):
                network_worked = True
                # print("FOUND NETWORK WORKED LINE")

        with stdout_path.open("r") as f:
            # Search for start prefix.
            for line in f:
                if line.startswith(start_prefix):
                    start_datestr = line[len(start_prefix) :].strip()
                    break
            else:
                print(f"{directory}: Couldn't find '{start_prefix}'")
                return None

            # Parse program output until reaching end prefix.
            for line in f:
                if line.startswith(end_prefix):
                    end_datestr = line[len(end_prefix) :].strip()
                    break
                parse_program_output(line)
            else:
                print(f"{directory}: Couldn't find '{end_prefix}'")
                return None

        start_datetime = datetime.strptime(start_datestr, datetime_format)
        end_datetime = datetime.strptime(end_datestr, datetime_format)
        duration = end_datetime - start_datetime

        if not network_worked:
            # We need to check the other file for the program output.
            output_file = directory / "output.txt"
            if output_file.exists():
                with output_file.open("r") as f:
                    for line in f:
                        parse_program_output(line)
            else:
                warnings.warn(
                    f"{directory}: output file was not found (tried {output_file})",
                    stacklevel=2,
                )
                network_ls_tries = 0

        if network_worked and network_ls_tries == -1:
            network_ls_tries = 0

        # print("network_ls_tries:", network_ls_tries)
        return StdoutInfo(duration=duration, network_ls_tries=network_ls_tries)

    @staticmethod
    def read_loss(path_loss):
        if not path_loss.exists():
            return None

        losses = []
        mag_grads = []
        with path_loss.open("r", newline="") as f:
            reader = csv.reader(f)
            header = next(reader)
            for row in reader:
                losses.append(float(row[0]))
                mag_grads.append(float(row[1]))
        return [losses, mag_grads]

    @staticmethod
    def read_hyper_params(path_hyper_params):
        if not path_hyper_params.exists():
            return None
        with path_hyper_params.open("r") as f:
            s = f.read(Run.READ_MAX)
            if len(s) >= Run.READ_MAX:
                raise ValueError(f"This file is too big: {path_hyper_params}")
        if s.startswith("HyperParams"):
            # sed -e 's/HyperParams(\(.*\))//g' -e 's/\([,{]\) *\([_A-Za-z][_A-Za-z0-9]*\)=/\1\n    "\2": /g'
            s = re.sub(r"HyperParams\((.*)\)", r"{\1\n}", s)
            s = re.sub(r"([,{]) *([_A-Za-z][_A-Za-z0-9]*)=", r'\1\n    "\2": ', s)
            path_hyper_params.rename(f"{path_hyper_params}_orig")
            path_hyper_params.write_text(s)

        hyper_params_dict = ast.literal_eval(s)
        hyper_params = HyperParams(**hyper_params_dict)
        return hyper_params

    @staticmethod
    def read_hyper_params_data(path_hyper_params_data):
        if not path_hyper_params_data.exists():
            return None
        with path_hyper_params_data.open("r") as f:
            s = f.read(Run.READ_MAX)
            if len(s) >= Run.READ_MAX:
                raise ValueError(f"This file is too big: {path_hyper_params_data}")

        if s.startswith("HyperParamsData"):
            # Need to define things so that eval can handle them alright.
            ndarray = lambda x: x
            DeviceArray = lambda x, **kwargs: x
            float64 = ""

            # UNSAFE EVAL
            hyper_params_data = eval(s)

            # Some of the gradients are written out fully, but I only want the magnitude.
            d = hyper_params_data.asdict()
            fixed_params = {}
            for k, v in d.items():
                if isinstance(v, list):
                    if "gradient" in k:
                        if "exp" in k:
                            # List of tuples of numpy arrays => tuple
                            fixed = []
                            for exp_list in v:
                                fixed_exp_list = []
                                for loss_list in exp_list:
                                    if isinstance(loss_list, (list, tuple)):
                                        arrs = [np.array(l) for l in loss_list]
                                        mag = np.sqrt(
                                            sum(np.sum(arri ** 2) for arri in arrs)
                                        )
                                    else:
                                        mag = loss_list
                                    fixed_exp_list.append(mag)
                                fixed.append(tuple(fixed_exp_list))
                            fixed = tuple(fixed)
                        else:
                            # List of numpy arrays.
                            arrs = [np.array(vi) for vi in v]
                            fixed = np.sqrt(sum(np.sum(arri ** 2) for arri in arrs))
                    else:
                        fixed = tuple(v)
                    fixed_params[k] = fixed
            hyper_params_data = replace(hyper_params_data, **fixed_params)

            path_hyper_params_data.rename(f"{path_hyper_params_data}_orig")
            with path_hyper_params_data.open("w") as f:
                pprint(hyper_params_data.asdict(), stream=f)
        else:
            # Some of them have `array` and `ndarray` in them, so I need to remove those.
            old_s = s
            s = old_s.replace("ndarray", "")
            s = s.replace("array", "")

            # Read from file.
            hyper_params_dict = ast.literal_eval(s)
            if "taylor_data" in hyper_params_dict:
                hyper_params_dict["taylor_data"] = str(hyper_params_dict["taylor_data"])
            hyper_params_data = HyperParamsData(**hyper_params_dict)

            # Fix the ones gradients that incorrectly weren't magnitudes.
            hpd = hyper_params_data
            fixed = []
            for exp in hpd.loss_initial_exp_gradient:
                exp_fixed = []
                for loss_grad in exp:
                    if isinstance(loss_grad, (list, tuple)):
                        arr = np.concatenate([np.array(l).flatten() for l in loss_grad])
                        mag = np.sqrt((arr * arr).sum())
                    else:
                        mag = loss_grad
                    exp_fixed.append(mag)
                fixed.append(tuple(exp_fixed))
            hpd.loss_initial_exp_gradient = tuple(fixed)

            if isinstance(hpd.loss_initial_gradient, (list, tuple)):
                arr = np.concatenate(
                    [np.array(l).flatten() for l in hpd.loss_initial_gradient]
                )
                hpd.loss_initial_gradient = np.sqrt((arr * arr).sum())

            # Fix some stuff that should've been tuples but were actually lists.
            hpd.loss_final_exp = tuple(hpd.loss_final_exp)
            hpd.loss_final_exp_gradient = tuple(hpd.loss_final_exp_gradient)
            hpd.loss_initial_exp = tuple(hpd.loss_initial_exp)

        return hyper_params_data

    @staticmethod
    def build_cr_network(hyper_params):
        if hyper_params is None:
            return None, None

        cr, network = make_jax_network_cr(
            seed=hyper_params.seed,
            dim=2,
            hidden_activation=hyper_params.activation,
            layer_sizes=hyper_params.layer_sizes,
            do_print=False,
        )

        # num_inputs = 3
        # num_outputs = 1
        # network = make_jax_network(
        #     0,
        #     num_inputs,
        #     num_outputs,
        #     MLP,
        #     hidden_activation=hyper_params.activation,
        #     output_activation="linear",
        #     layer_sizes=hyper_params.layer_sizes,
        # )
        return cr, network

    def copy_data(self, target_directory):
        target_directory = pathlib.Path(target_directory)
        target_stdout_directory = target_directory / "stdout"
        target_stderr_directory = target_directory / "stderr"
        target_stdout_directory.mkdir(parents=True, exist_ok=True)
        target_stderr_directory.mkdir(parents=True, exist_ok=True)

        stdout_path, stderr_path = self.get_stdout_stderr_paths(self.directory)
        shutil.copytree(
            str(self.directory), str(target_directory / "jobs" / self.directory.name)
        )
        shutil.copy2(str(stdout_path), str(target_stdout_directory / stdout_path.name))
        shutil.copy2(str(stderr_path), str(target_stderr_directory / stdout_path.name))

from jax import random
from flax import linen as nn
from typing import List
import jax
import jax.numpy as jnp
import numpy as onp

from pyadjoint_utils.jax_adjoint import array

from ice_crikit.param_utils import Param


def make_jax_network(
    seed, num_inputs, num_outputs, constructor, *constructor_args, **constructor_kwargs
):
    network_jax = constructor(num_outputs, *constructor_args, **constructor_kwargs)

    # Initialize weights by giving dummy input.
    key1, key2 = jax.random.split(jax.random.PRNGKey(seed))
    x = jax.random.normal(key1, (num_inputs - 1,))  # TODO: get rid of this -1.
    params_jax = network_jax.init(key2, x)

    def make_param(p):
        h = 1e-1 * array(jnp.array(onp.random.randn(*p.shape) ** 2))
        bounds = [-onp.inf, onp.inf]
        param = Param(array(p), h, True, "TODO:get_param_name", bounds)
        return param

    params = jax.tree_util.tree_map(make_param, params_jax)

    # Check for nonnegative params and set their lower bounds accordingly.
    for key in params["params"]:
        if key.startswith("nonnegative_"):
            params["params"][key]["kernel"].bounds[0] = 0
    network = Network(network_jax, params)
    network.build_param_names()

    return network


class BatchStats:
    def __str__(self):
        return f"BatchStats(mean={self.mean}, stddev={self.stddev})"

    def set_stats(self, mean, stddev):
        self.mean = mean
        self.stddev = stddev

    def set_stats_from_data(self, data):
        mean = data.mean(axis=0)
        stddev = data.std(axis=0)
        self.set_stats(mean, stddev)


class Network:
    def __init__(
        self, network, params, input_batch_stats=None, output_batch_stats=None
    ):
        self.network = network
        self.params = params
        self.tree_structure = jax.tree_util.tree_structure(params)
        self.input_batch_stats = input_batch_stats
        self.output_batch_stats = output_batch_stats

    def build_param_names(self):
        params = self.params
        if len(params) == 1 and "params" in params:
            params = params["params"]
        all_paths = self.get_dict_path(params, [], [])
        param_names = ["_".join(p) for p in all_paths]
        for p, name in zip(self.get_flat_params(), param_names):
            p.name = name

    @staticmethod
    def get_dict_path(d, all_paths, path_list):
        if isinstance(d, Param):
            all_paths.append(path_list)
            return all_paths
        for c in d:
            path_copy = list(path_list)
            path_copy.append(c)
            all_paths = Network.get_dict_path(d[c], all_paths, path_copy)
        return all_paths

    def set_params_from_keras_file(self, filename):
        with onp.load(filename) as data:
            keys = list(data.keys())
            batch_stats = [data[k] for k in keys[:3]]
            weights_np = [data[k] for k in keys[3:]]
        if self.input_batch_stats is None:
            self.input_batch_stats = BatchStats()
        self.input_batch_stats.set_stats(batch_stats[0], onp.sqrt(batch_stats[1]))
        print("New stats are:", self.input_batch_stats)

        for i in range(0, len(weights_np), 2):
            weights_np[i], weights_np[i + 1] = weights_np[i + 1], weights_np[i]

    def set_weights(self, weights_np):
        flat_params = self.get_flat_params()
        for w, p in zip(weights_np, flat_params):
            if w.shape != p.val.shape:
                raise ValueError(
                    f"Warning: saved weight shape {w.shape} does not match original shape {p.val.shape}"
                )
            p.val = array(w)

    def save_params_to_file(self, filename):
        arrays = [self.input_batch_stats.mean, self.input_batch_stats.stddev]
        arrays += [p.val.arr for p in self.get_flat_params()]
        onp.savez(filename, *arrays)

    def load_params_from_file(self, filename):
        with onp.load(filename) as data:
            data_list = [data[k] for k in data]
            if self.input_batch_stats is None:
                self.input_batch_stats = BatchStats()
            self.input_batch_stats.set_stats(data_list[0], data_list[1])
            self.set_weights(data_list[2:])

    def get_flat_params(self):
        return jax.tree_util.tree_leaves(self.params)

    def input_transform(self, inputs):
        # Remove first invariant and take square root of second invariant.
        inputs = inputs[1:]
        e = (inputs[0] + 1e-10) ** 0.5
        inputs = jax.ops.index_update(inputs, 0, e)
        return inputs

    def __call__(self, inputs, *flat_params):
        inputs = self.input_transform(inputs)
        if self.input_batch_stats is not None:
            inputs = (
                inputs - self.input_batch_stats.mean
            ) / self.input_batch_stats.stddev
        params = jax.tree_util.tree_unflatten(self.tree_structure, flat_params)
        outputs = self.network.apply(params, inputs)
        if self.output_batch_stats is not None:
            outputs = (
                outputs * self.output_batch_stats.stddev + self.output_batch_stats.mean
            )
        return outputs

    def __str__(self):
        network_shapes = jax.tree_map(lambda p: p.val.shape, self.params)
        return str(network_shapes)


def uniform(wmin, wmax, dtype=jnp.float64):
    scale = wmax - wmin

    def init(key, shape, dtype=dtype):
        return random.uniform(key, shape, dtype) * scale + wmin

    return init


def zeros(key, shape, dtype=jnp.float64):
    return jnp.zeros(shape, dtype)


class FICNN(nn.Module):
    """A Fully Input Convex Neural Network model.

    Based on "Input Convex Neural Networks" by Amos et al: https://arxiv.org/pdf/1609.07152.pdf
    """

    num_outputs: int
    layer_sizes: List[int]

    def setup(self):
        self.nonneg_init = uniform(0, 1e-6)
        self.normal_init = uniform(-1e-6, 1e-6)

    @nn.compact
    def __call__(self, x):
        x = x.flatten()
        inputs = x
        x_hidden_layer_kwargs = {
            "dtype": jnp.float64,
            "bias_init": zeros,
            "kernel_init": self.nonneg_init,
        }
        y_hidden_layer_kwargs = {
            "dtype": jnp.float64,
            "kernel_init": self.normal_init,
            "use_bias": False,
        }

        nonneg_i = 0
        for num in self.layer_sizes:
            x = nn.Dense(num, **x_hidden_layer_kwargs, name=f"nonnegative_{nonneg_i}")(
                x
            )
            nonneg_i += 1
            y = nn.Dense(num, **y_hidden_layer_kwargs)(inputs)
            x = nn.softplus(x + y)

        x = nn.Dense(
            self.num_outputs, **x_hidden_layer_kwargs, name=f"nonnegative_{nonneg_i}"
        )(x)
        nonneg_i += 1
        y = nn.Dense(self.num_outputs, **y_hidden_layer_kwargs)(inputs)

        outputs = nn.softplus(x + y)
        if self.num_outputs == 1:
            outputs = outputs.reshape((-1,))
        return outputs


class MonotonicNN(nn.Module):
    """Based on Lang, Bernhard. "Monotonic multi-layer perceptron networks as universal
    approximators." International conference on artificial neural networks. Springer, Berlin, Heidelberg, 2005."""

    num_outputs: int
    layer_sizes: List[int]

    def setup(self):
        self.nonneg_init = uniform(0, 1e-6)
        self.nonneg_i = 0

    @nn.compact
    def __call__(self, x):
        hidden_layer_kwargs = {
            "dtype": jnp.float64,
            "bias_init": zeros,
            "kernel_init": self.nonneg_init,
        }
        q_hidden_layer_kwargs = {
            "dtype": jnp.float64,
            "kernel_init": self.nonneg_init,
            "use_bias": False,
        }

        self.nonneg_i = 0
        q = x
        first = True
        for num in self.layer_sizes:
            x = nn.Dense(
                num, **hidden_layer_kwargs, name=f"nonnegative_{self.nonneg_i}"
            )(x)
            self.nonneg_i += 1
            if not first:
                # Connect inputs directly to this hidden layer.
                y = nn.Dense(
                    num, **q_hidden_layer_kwargs, name=f"nonnegative_{self.nonneg_i}"
                )(q)
                self.nonneg_i += 1
                x += y
                first = False
            x = nn.sigmoid(x)
        x = nn.Dense(
            self.num_outputs, **hidden_layer_kwargs, name=f"nonnegative_{self.nonneg_i}"
        )(x)
        self.nonneg_i += 1

        if not first:
            y = nn.Dense(
                self.num_outputs,
                **q_hidden_layer_kwargs,
                name=f"nonnegative_{self.nonneg_i}",
            )(q)
            self.nonneg_i += 1
            x += y
            first = False
        x = nn.softplus(x)

        # Add a bias at the end to allow negative output.
        bias = self.param(
            "output_bias", lambda rng, shape: jnp.zeros(shape), x.shape[1:]
        )
        output = x + bias
        return output


class FIQCNN(nn.Module):
    """A quasi-convex version of a FICNN"""

    num_outputs: int
    convex_outputs: int
    convex_layer_sizes: List[int]
    monotonic_layer_sizes: List[int]

    @nn.compact
    def __call__(self, x):
        x = FICNN(self.convex_outputs, self.convex_layer_sizes)(x)
        x = MonotonicNN(self.num_outputs, self.monotonic_layer_sizes)(x)
        return x


class MLP(nn.Module):
    """Just a simple multi-layer network."""

    num_outputs: int
    hidden_activation: str
    output_activation: str
    layer_sizes: List[int]

    def setup(self):
        self.normal_init = uniform(-2, 2)

    @nn.compact
    def __call__(self, x, train=False):
        inputs = x
        hidden_layer_kwargs = {
            "kernel_init": self.normal_init,
            "bias_init": zeros,
            "dtype": jnp.float64,
        }

        if self.hidden_activation == "linear":
            activation = lambda x: x
        else:
            activation = getattr(nn, self.hidden_activation)

        for num in self.layer_sizes:
            x = nn.Dense(num, **hidden_layer_kwargs)(x)
            x = activation(x)

        output_layer_kwargs = {
            "kernel_init": self.normal_init,
            "bias_init": zeros,
            "dtype": jnp.float64,
        }
        x = nn.Dense(self.num_outputs, **output_layer_kwargs)(x)
        if self.output_activation != "linear":
            x = getattr(nn, self.output_activation)(x)

        if self.num_outputs == 1:
            x = x.reshape((-1,))
        return x

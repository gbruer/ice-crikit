import os


try:
    import cPickle as pickle
except:
    import pickle


class PickleDictionaryCache:
    """Pickles a dictionary to and from a file."""


    def __init__(self, filename):
        self.filename = filename
        try:
            with open(self.filename, "rb") as f:
                self.val = pickle.load(f)
        except FileNotFoundError:
            print(f'Warning: cache file "{self.filename}" not found')
            self.val = {}

    def write(self):
        backup = f"{self.filename}.backup"

        try:
            print("Making backup cache")
            os.rename(self.filename, backup)
            made_cache = True
        except FileNotFoundError:
            print("  Nevermind. There was no cache file to back up anyway.")
            made_cache = False

        try:
            with open(self.filename, "wb") as f:
                pickle.dump(self.val, f)
            if made_cache:
                print("Removing backup cache")
                try:
                    os.remove(backup)
                except FileNotFoundError:
                    print("  Failed to remove backup cache, but it's probably fine")

        except:
            print("Restoring backup cache")
            os.rename(backup, self.filename)
            raise

from matplotlib.collections import PatchCollection
from matplotlib.lines import Line2D
from matplotlib.transforms import IdentityTransform
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

import numpy as np

import shapely.affinity
from shapely.geometry import Point, MultiPolygon
from shapely.ops import unary_union


def plot_translucent_point_group(*args, zoomable=True, ax=None, size=2, **patch_kwargs):
    collection = ComboPointCollection(*args, ax=ax, size=size, **patch_kwargs)
    ax = collection.ax
    if not zoomable:
        patches = collection.get_polygon_patch(
            output_transform=False, input_transform=False
        )
        collection = PatchCollection(patches, match_original=True, **patch_kwargs)
    l = Line2D(
        [],
        [],
        marker="o",
        label=collection.get_label(),
        color="w",
        alpha=patch_kwargs.get("alpha", 1),
        markerfacecolor=patch_kwargs.get("facecolor", None),
        markersize=10,
    )
    ax.add_line(l)
    ax.add_collection(collection)
    if not zoomable:
        return patches


def replot_translucent_point_group(
    patches, zoomable=False, ax=None, size=2, **patch_kwargs
):
    if zoomable:
        raise ValueError("zoomable must be False for replot")
    ax = ax or plt.gca()

    collection = PatchCollection(patches, match_original=True, **patch_kwargs)
    l = Line2D(
        [],
        [],
        marker="o",
        label=collection.get_label(),
        color="w",
        alpha=patch_kwargs.get("alpha", 1),
        markerfacecolor=patch_kwargs.get("facecolor", None),
        markersize=10,
    )
    ax.add_line(l)
    ax.add_collection(collection)


class ComboPointCollection(PatchCollection):
    def __init__(self, x, y, ax=None, size=2, **patch_kwargs):
        self.ax = ax or plt.gca()
        self.__size = size
        self.__x = np.asarray(x)
        self.__y = np.asarray(y)
        self.__data_coords = np.array([self.__x.flatten(), self.__y.flatten()]).T
        self.patches = self.get_polygon_patch()
        super().__init__(
            self.patches,
            match_original=True,
            transform=IdentityTransform(),
            **patch_kwargs
        )
        mins = self.__data_coords.min(axis=0)
        maxs = self.__data_coords.max(axis=0)

        mids = (mins + maxs) / 2
        diff = (maxs - mins) / 2
        mins = mids - diff * 1.05
        maxs = mids + diff * 1.05

        xlim = self.ax.get_xlim()
        ylim = self.ax.get_ylim()

        xlim = (min(xlim[0], mins[0]), max(xlim[1], maxs[0]))
        ylim = (min(ylim[0], mins[1]), max(ylim[1], maxs[1]))

        self.ax.set_xlim(xlim[0], xlim[1])
        self.ax.set_ylim(ylim[0], ylim[1])

        self.__points = [Point(px, py).buffer(1) for px, py in zip(self.__x, self.__y)]

    def get_polygon_patch(self, input_transform=True, output_transform=False):
        xmin, xmax = self.ax.get_xlim()
        ymin, ymax = self.ax.get_ylim()

        if input_transform:
            coords = self.ax.transData.transform(self.__data_coords)
            x_scale = self.__size
            y_scale = self.__size
        else:
            s = self.__size
            inv = self.ax.transData.inverted()
            x_scale, y_scale = inv.transform((s, s)) - inv.transform((0, 0))
            coords = self.__data_coords

        xs = coords[:, 0]
        ys = coords[:, 1]

        polygons = [
            shapely.affinity.scale(Point(px, py).buffer(1), x_scale, y_scale)
            for px, py in zip(xs, ys)
        ]
        polygons_union = unary_union(polygons)
        if not isinstance(polygons_union, MultiPolygon):
            polygons_union = MultiPolygon((polygons_union,))

        ps = []
        if output_transform:
            transform = self.ax.transData
        else:
            transform = IdentityTransform()

        for polygon in polygons_union:
            arr = np.array(polygon.exterior)
            if arr.size != 0:
                polygon = mpatches.Polygon(arr, transform=transform)
                ps.append(polygon)
        return ps

    def update_patches(self):
        self.patches = self.get_polygon_patch()
        self.set_paths(self.patches)

    def draw(self, renderer):
        self.update_patches()
        super().draw(renderer)

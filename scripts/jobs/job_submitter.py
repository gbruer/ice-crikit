import itertools
import pathlib
import subprocess
import sys

from pprint import pprint
from ice_crikit.hyper_param_utils import HyperParams


class JobSubmitter:
    def __init__(
        self,
        job_dir="jobs",
        stdout_dir="stdout",
        stderr_dir="stderr",
        job_script="run_tests.pbs",
    ):
        self.job_dir = pathlib.Path(job_dir)
        self.stdout_dir = pathlib.Path(stdout_dir)
        self.stderr_dir = pathlib.Path(stderr_dir)
        self.job_script = pathlib.Path(job_script)

        self.job_dir.mkdir(parents=True, exist_ok=True)
        self.stdout_dir.mkdir(parents=True, exist_ok=True)
        self.stderr_dir.mkdir(parents=True, exist_ok=True)

    def submit_job(self, hyper_params, dryrun=None):
        v_arg = []
        for k, v in hyper_params.asdict().items():
            if isinstance(v, (str, bool, int, float)):
                stringified = f"{k}={v}"
            elif isinstance(v, (list, tuple)):
                tup = tuple(str(a) for a in v)
                stringified = f"{k}={' '.join(tup)}"
            elif v is None:
                continue
            else:
                raise ValueError(f"I can't handle this type yet: {type(v)}, {v}")
            v_arg.append(stringified)
        v_arg_str = ",".join(v_arg)

        # args = ["echo", "qsub", "-v", v_arg_str, self.job_script]
        args = ["qsub", "-v", v_arg_str, self.job_script]
        if dryrun or dryrun is None:
            print("Would run:", args)
        else:
            print(args)
            subprocess.run(args, shell=False)


def parameter_sweep():
    all_layer_sizes = ((2,), (2, 2), (2, 2, 2), (4,), (4, 4), (4, 4, 4))
    all_activations = ("softplus", "tanh")
    all_optimizers = ("L-BFGS-B", "trust-constr")
    all_noises = ((0.01, 0.13), (0.05, 0.65))
    all_observers = sum(
        (
            (
                dict(surface=False, borehole=False, noise=noise),
                dict(surface=True, borehole=False, noise=noise),
                dict(surface=True, borehole=True, noise=noise, borehole_noise=noise),
            )
            for noise in all_noises
        ),
        (),
    )
    seed = 909467

    all_params = (all_layer_sizes, all_activations, all_optimizers, all_observers)

    job_submitter = JobSubmitter()
    params_to_test = itertools.product(*all_params)
    N = len(list(params_to_test))
    params_to_test = itertools.product(*all_params)
    for i, (layer_sizes, activation, optimizer, observer) in enumerate(params_to_test):

        kwargs = dict(
            observer="full",
            activation=activation,
            optimizer=optimizer,
            layer_sizes=layer_sizes,
            seed=seed,
            noise_seed=874363511,
        )
        kwargs.update(observer)
        hyper_params = HyperParams(**kwargs)

        print()
        print(f"{i+1:2d} / {N}: ", end="")
        pprint(hyper_params.asdict())
        submit = input("submit?: ")
        if submit == "y":
            job_submitter.submit_job(hyper_params, dryrun=False)


def resubmit_jobs():
    job_submitter = JobSubmitter()

    input_file = pathlib.Path(sys.argv[1])
    if len(sys.argv) > 2:
        root_dir = sys.argv[2]
    else:
        root_dir = "."
    root_dir = pathlib.Path(root_dir)

    if input_file.is_file():
        with input_file.open("r") as f:
            for i, line in enumerate(f):
                job_dir = pathlib.Path(line.strip())
                param_path = root_dir / job_dir / "hyper_params.txt"
                hyper_params = HyperParams.read_from_file(param_path)

                print()
                print(f"{i+1:2d}: ", end="")
                pprint(hyper_params.asdict())
                submit = input("submit?: ")
                if submit == "y":
                    job_submitter.submit_job(hyper_params, dryrun=False)
    elif input_file.is_dir():
        for i, child in enumerate(input_file.iterdir()):
            if not child.is_file():
                print(f"Skipping non-file {child}")
                continue

            hyper_params = HyperParams.read_from_file(child)
            print()
            print(f"{i+1:2d}: ", end="")
            pprint(hyper_params.asdict())
            submit = input("submit?: ")
            if submit == "y":
                job_submitter.submit_job(hyper_params, dryrun=False)


if __name__ == "__main__":
    # parameter_sweep()
    resubmit_jobs()
